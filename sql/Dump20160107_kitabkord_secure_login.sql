-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: kitabkord_secure_login_db
-- ------------------------------------------------------
-- Server version	5.5.41-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `login_attempts_tb`
--

DROP TABLE IF EXISTS `login_attempts_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_attempts_tb` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_attempts_tb`
--

LOCK TABLES `login_attempts_tb` WRITE;
/*!40000 ALTER TABLE `login_attempts_tb` DISABLE KEYS */;
INSERT INTO `login_attempts_tb` VALUES (1,'1441647336'),(1,'1442181993'),(1,'1442385825'),(1,'1442558652'),(1,'1442558662'),(1,'1442728997'),(1,'1445730285'),(1,'1445731559'),(1,'1445736534'),(1,'1445737321'),(1,'1445737569'),(1,'1445812657'),(1,'1449444628'),(1,'1449447147'),(1,'1449451520'),(1,'1449451529'),(1,'1451194543');
/*!40000 ALTER TABLE `login_attempts_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `members_privilege_tb`
--

DROP TABLE IF EXISTS `members_privilege_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members_privilege_tb` (
  `user_username` varchar(30) NOT NULL,
  `user_privilege` varchar(100) NOT NULL,
  UNIQUE KEY `user_username` (`user_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members_privilege_tb`
--

LOCK TABLES `members_privilege_tb` WRITE;
/*!40000 ALTER TABLE `members_privilege_tb` DISABLE KEYS */;
INSERT INTO `members_privilege_tb` VALUES ('root','admin'),('test_user','admin');
/*!40000 ALTER TABLE `members_privilege_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `members_tb`
--

DROP TABLE IF EXISTS `members_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  `register_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `profile_picture` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`,`username`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members_tb`
--

LOCK TABLES `members_tb` WRITE;
/*!40000 ALTER TABLE `members_tb` DISABLE KEYS */;
INSERT INTO `members_tb` VALUES (1,'root','root@root.com','b6829a0ba3abe5f07b8b6eed79c095cdc1d201a312724b93e878f0c577236711d7737374708211e0431d2972abc96b0015198c9c5cf168bdfcb13dbd67655208','b58b24c70d0980c37bea830525b2e01cc73f6f4ea572bc252e8261ffabe4f3ae2b22c4c1db3f9349d7292ba13e7f61e8a6b6393f4c07a148b878b00bf8a8fddd','2015-09-07 10:22:32','1_root.png'),(2,'test_user','test@example.com','00807432eae173f652f2064bdca1b61b290b52d40e429a7d295d76a71084aa96c0233b82f1feac45529e0726559645acaed6f3ae58a286b9f075916ebf66cacc','f9aab579fc1b41ed0c44fe4ecdbfcdb4cb99b9023abb241a6db833288f4eea3c02f76e0d35204a8695077dcf81932aa59006423976224be0390395bae152d4ef','2015-09-07 10:22:34',NULL),(3,'nonroot','nonroot@nonroot.com','899b60e3ac7eead4aeee4f83e1ccc2ab77d10068be9f93a06da92e6913101a121b416a5d9505e6a128f1e3588cba5ab29905330a5b6e0bf90d000792d21a79ab','8458c6082d37b05931d12e5600d36d4e8c23898e6ccd971f5b153261f2f9c5511206676359f10be5b11c1dc6a9b4ac3ad187fabbead73ecc0d612c7a3b808950','2015-09-13 22:32:42',NULL),(4,'testing','testing@testing.com','363b9d8a8d1a934f8dee9dad2bdfc6fc613b96ab6d6a7e45ea43c12e6027bf15d0d6c0da495e08a15f3664a1a90bfb5d320490bd9d574f3a59e4b1a2a5b2ddb2','df3b366203d086e98722f7b72f1cade091d20d4df9698d57f9e9d30222719eeccce7c0a3eb5303ed365f3545137445491bc8c1216111ad0ddfd48100bd0f2fa6','2015-12-28 00:20:44','4_testing.jpg');
/*!40000 ALTER TABLE `members_tb` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-07  1:20:59
