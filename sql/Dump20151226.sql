-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: kitabkord_db
-- ------------------------------------------------------
-- Server version	5.5.41-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `note_tb`
--

DROP TABLE IF EXISTS `note_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `note_tb` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `note_value` varchar(100) NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `note_tb`
--

LOCK TABLES `note_tb` WRITE;
/*!40000 ALTER TABLE `note_tb` DISABLE KEYS */;
INSERT INTO `note_tb` VALUES (1,'C'),(2,'C#'),(3,'D'),(4,'D#'),(5,'E'),(6,'F'),(7,'F#'),(8,'G'),(9,'G#'),(10,'A'),(11,'A#'),(12,'B');
/*!40000 ALTER TABLE `note_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlists_songs_tb`
--

DROP TABLE IF EXISTS `playlists_songs_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlists_songs_tb` (
  `playlists_songs_id` int(11) NOT NULL AUTO_INCREMENT,
  `playlists_songs_playlist_id` int(11) NOT NULL,
  `playlists_songs_song_id` int(11) NOT NULL,
  `playlists_songs_song_idx` int(11) DEFAULT NULL,
  `playlists_songs_target_note_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`playlists_songs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlists_songs_tb`
--

LOCK TABLES `playlists_songs_tb` WRITE;
/*!40000 ALTER TABLE `playlists_songs_tb` DISABLE KEYS */;
INSERT INTO `playlists_songs_tb` VALUES (49,19,13,1,8),(50,19,12,2,8),(51,21,12,1,8),(52,19,14,3,8),(53,22,14,1,8),(54,21,14,2,8),(55,19,11,4,1),(57,21,11,3,1),(58,22,11,2,1),(59,20,11,1,5);
/*!40000 ALTER TABLE `playlists_songs_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlists_tb`
--

DROP TABLE IF EXISTS `playlists_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlists_tb` (
  `playlist_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `playlist_name` varchar(50) NOT NULL,
  PRIMARY KEY (`playlist_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlists_tb`
--

LOCK TABLES `playlists_tb` WRITE;
/*!40000 ALTER TABLE `playlists_tb` DISABLE KEYS */;
INSERT INTO `playlists_tb` VALUES (1,'0','root'),(2,'0','dummy_playlist'),(3,'0','test3'),(4,'0','test4'),(5,'0','test5'),(6,'0','test6'),(19,'root','KTM PW 12-09'),(20,'root','KTM PW 10-11'),(21,'root','KTM PW 11-20'),(22,'root','KTM PW 11-30');
/*!40000 ALTER TABLE `playlists_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `song_info_tb`
--

DROP TABLE IF EXISTS `song_info_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `song_info_tb` (
  `song_id` int(11) NOT NULL AUTO_INCREMENT,
  `song_title` varchar(200) NOT NULL,
  `song_writer` varchar(200) DEFAULT NULL,
  `song_singer` varchar(200) NOT NULL,
  `song_album` varchar(200) DEFAULT NULL,
  `song_tag_id` varchar(100) NOT NULL,
  `song_base_note_id` int(11) NOT NULL,
  `song_lyric` longtext NOT NULL,
  `song_thumbs` int(11) DEFAULT NULL,
  `song_youtube` varchar(100) DEFAULT NULL,
  `song_itunes` varchar(100) DEFAULT NULL,
  `song_spotify` varchar(100) DEFAULT NULL,
  `song_google_play` varchar(100) DEFAULT NULL,
  `song_approval_status` varchar(100) NOT NULL,
  `song_username` varchar(30) NOT NULL,
  `song_submit_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`song_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `song_info_tb`
--

LOCK TABLES `song_info_tb` WRITE;
/*!40000 ALTER TABLE `song_info_tb` DISABLE KEYS */;
INSERT INTO `song_info_tb` VALUES (11,'Sejauh Timur','','Nikita','','2',1,':Intro: C F G Dm G C 1x\r\n\r\n:Verse:\r\n  C         Am                Em           F\r\nSejauh Timur dari Barat Engkau membuang dosaku\r\n   Dm            G             C  G\r\nTiada kau ingat lagi pelanggaranku\r\n  C           Am               Em            F\r\nJauh ke dalam tubir laut Kau melemparkan dosaku\r\n   Dm            G              C  G\r\nTiada kau perhitungkan kesalahanku\r\n\r\n:Reff:\r\n          C     C        F\r\nBetapa besar kasih pengampunanMu Tuhan\r\n        Dm            G            C  G\r\nTak Kau pandang hina hati yang hancur\r\n          C     C      F\r\nKu berterimakasih kepadaMu ya Tuhan\r\n      Dm             G             C\r\nPengampunan yang kau beri pulihkanku',0,'nj-Cw3nepI0','','','','approved','root','2015-09-18 06:46:49'),(12,'Jangan Lelah                ','','Franky Sihombing','','1',8,':Intro: G Am D C G 1x\r\n\r\n:Verse: \r\n           G                        Em\r\nJangan lelah bekerja di ladangnya Tuhan\r\n                       C                Am            D\r\nRoh Kudus yang b\'ri kekuatan yang mengajar dan menopang\r\n          G                      Em\r\nTiada lelah bekerja bersamamu Tuhan\r\n                    C    D                      G\r\nYang selalu mencukupkan ...........akan segalanya\r\n\r\n:Reff:\r\n           G          D\r\nRatakan tanah bergelombang\r\n             Em         C   Am\r\nTimbunlah tanah yang berlubang\r\n           G       Am                 D\r\nMenjadi siap dibangun di atas dasar iman',0,'JnyW00ZEFjY','','','','approved','root','2015-09-18 06:47:40'),(13,'Flawless                ','','MercyMe','','1',8,':Intro: G 1x\r\n\r\n:Verse 1:\r\n                  G\r\nThere\'s got to be more, than going back and forth \r\n     D\r\nFrom doing right to doing wrong \'cause we were taught that\'s who we are \r\nC\r\nCome on get in line right behind me you along with everybody \r\nG\r\nThinking there\'s worth in what you do \r\n\r\nG\r\nThen Like a hero who takes the stage when we\'re\r\nD                                        C\r\non the edge of our seats saying it\'s too late \r\n                             G\r\nWell let me introduce you to amazing grace \r\n\r\n:Chorus:\r\nG\r\nNo matter the bumps, no matter the bruises\r\nD \r\nNo matter the scars, still the truth is \r\n    C                                      G\r\nThe cross has made, the cross has made you flawless (the cross has made you flawless)\r\nG\r\nNo matter the hurt or how deep the wound is \r\nD\r\nNo matter the pain, still the truth is \r\n    C                                      G\r\nThe cross has made, the cross has made you flawless\r\n\r\n:Verse 2:\r\n                 G\r\nCould it possibly be, that we simply can\'t believe \r\n          D\r\nThat this unconditional, kind of love would be enough to\r\nC\r\ntake a filthy wretch like this and wrap him up in righteousness \r\nG\r\nBut that\'s exactly what He did \r\n\r\n:(Chorus)\r\n\r\n:Bridge:\r\nF       C            G\r\nOh yeah oooh-ooo-ooo oooh-ooo-ooo\r\n F\r\nTake a breath smile and say \r\nF\r\nRight here right now I\'m ok \r\nC                     G\r\nBecause the cross was enough \r\n\r\nG\r\nThen Like a hero who takes the stage when we\'re\r\nD                                        C\r\non the edge of our seats saying it\'s too late \r\n                             G\r\nWell let me introduce you to grace grace \r\nG\r\nGod\'s grace \r\n\r\n:(Chorus)\r\n\r\nG                                              D\r\nNo matter what they say, or what you think you are\r\n                                         C\r\nThe day you called His name, He made you flawless\r\n            G\r\nHe made you flawless\r\n\r\nG\r\nNo matter the bumps, no matter the bruises \r\nD\r\nNo matter the scars, still the truth is \r\n    C                                      G\r\nThe cross has made, the cross has made you flawless',0,'wjLlLPZderk','','','','approved','root','2015-09-18 06:53:17'),(14,'Amazing Grace                                 ','','Chris Tomlin','','2, 3',8,':Intro: G C G G D7 G 1x\r\n\r\n:Verse 1:\r\n       G            C          G\r\nAmazing Grace, how sweet the sound,\r\n			 D7\r\nThat saved a wretch like me.\r\n     G                C       G\r\nI once was lost, but now im found,\r\n               D7     G\r\nWas blind, but now I see.\r\n\r\n:Verse 2:\r\n    G                        C       G\r\nTwas grace that thought my heart to fear,\r\n                      D7            G\r\nand grace my fears released. How precious\r\n      C          G                  D7     G\r\ndid that grace appear, the  hour I first believed.\r\n\r\n:Verse 3:\r\n G                           C       G\r\nWhen we\'ve been there ten thousand years,\r\n G                     D7\r\nbright shining as the sun,\r\n  G                     C           G\r\nwe\'ve no less days to sing god\'s praise\r\n  G                D7         G\r\nthan when did when we first begun.\r\n\r\n:Verse 4:\r\nG            C          G\r\nAmazing Grace, how sweet the sound,\r\n			 D7\r\nThat saved a wretch like me.\r\n     G                C       G\r\nI once was lost, but now im found,\r\n               D7     G\r\nWas blind, but now I see.',0,'Jbe7OruLk8I','','','','approved','root','2015-09-18 07:03:49');
/*!40000 ALTER TABLE `song_info_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `song_tag_tb`
--

DROP TABLE IF EXISTS `song_tag_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `song_tag_tb` (
  `song_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `song_tag_tb`
--

LOCK TABLES `song_tag_tb` WRITE;
/*!40000 ALTER TABLE `song_tag_tb` DISABLE KEYS */;
/*!40000 ALTER TABLE `song_tag_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag_tb`
--

DROP TABLE IF EXISTS `tag_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag_tb` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_value` varchar(100) NOT NULL,
  `tag_picture` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag_tb`
--

LOCK TABLES `tag_tb` WRITE;
/*!40000 ALTER TABLE `tag_tb` DISABLE KEYS */;
INSERT INTO `tag_tb` VALUES (1,'praise','praise.jpg'),(2,'worship','worship.jpg'),(3,'christmas','christmas.jpg'),(4,'easter','easter.jpg'),(5,'latin','latin_mass.jpg'),(6,'eucharistic','eucharistic.jpg'),(7,'holy-friday','holy_friday.jpg'),(8,'other','other.jpg');
/*!40000 ALTER TABLE `tag_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thumb_tb`
--

DROP TABLE IF EXISTS `thumb_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thumb_tb` (
  `thumb_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `song_id` int(11) NOT NULL,
  PRIMARY KEY (`thumb_id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thumb_tb`
--

LOCK TABLES `thumb_tb` WRITE;
/*!40000 ALTER TABLE `thumb_tb` DISABLE KEYS */;
INSERT INTO `thumb_tb` VALUES (76,'root',14),(79,'root',12),(80,'root',13);
/*!40000 ALTER TABLE `thumb_tb` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-26  0:37:18
-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: kitabkord_secure_login_db
-- ------------------------------------------------------
-- Server version	5.5.41-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `login_attempts_tb`
--

DROP TABLE IF EXISTS `login_attempts_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_attempts_tb` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_attempts_tb`
--

LOCK TABLES `login_attempts_tb` WRITE;
/*!40000 ALTER TABLE `login_attempts_tb` DISABLE KEYS */;
INSERT INTO `login_attempts_tb` VALUES (1,'1441647336'),(1,'1442181993'),(1,'1442385825'),(1,'1442558652'),(1,'1442558662'),(1,'1442728997'),(1,'1445730285'),(1,'1445731559'),(1,'1445736534'),(1,'1445737321'),(1,'1445737569'),(1,'1445812657'),(1,'1449444628'),(1,'1449447147'),(1,'1449451520'),(1,'1449451529');
/*!40000 ALTER TABLE `login_attempts_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `members_privilege_tb`
--

DROP TABLE IF EXISTS `members_privilege_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members_privilege_tb` (
  `user_username` varchar(30) NOT NULL,
  `user_privilege` varchar(100) NOT NULL,
  UNIQUE KEY `user_username` (`user_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members_privilege_tb`
--

LOCK TABLES `members_privilege_tb` WRITE;
/*!40000 ALTER TABLE `members_privilege_tb` DISABLE KEYS */;
INSERT INTO `members_privilege_tb` VALUES ('root','admin'),('test_user','admin');
/*!40000 ALTER TABLE `members_privilege_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `members_tb`
--

DROP TABLE IF EXISTS `members_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  `register_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`,`username`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members_tb`
--

LOCK TABLES `members_tb` WRITE;
/*!40000 ALTER TABLE `members_tb` DISABLE KEYS */;
INSERT INTO `members_tb` VALUES (1,'root','root@root.com','b6829a0ba3abe5f07b8b6eed79c095cdc1d201a312724b93e878f0c577236711d7737374708211e0431d2972abc96b0015198c9c5cf168bdfcb13dbd67655208','b58b24c70d0980c37bea830525b2e01cc73f6f4ea572bc252e8261ffabe4f3ae2b22c4c1db3f9349d7292ba13e7f61e8a6b6393f4c07a148b878b00bf8a8fddd','2015-09-07 10:22:32'),(2,'test_user','test@example.com','00807432eae173f652f2064bdca1b61b290b52d40e429a7d295d76a71084aa96c0233b82f1feac45529e0726559645acaed6f3ae58a286b9f075916ebf66cacc','f9aab579fc1b41ed0c44fe4ecdbfcdb4cb99b9023abb241a6db833288f4eea3c02f76e0d35204a8695077dcf81932aa59006423976224be0390395bae152d4ef','2015-09-07 10:22:34'),(3,'nonroot','nonroot@nonroot.com','899b60e3ac7eead4aeee4f83e1ccc2ab77d10068be9f93a06da92e6913101a121b416a5d9505e6a128f1e3588cba5ab29905330a5b6e0bf90d000792d21a79ab','8458c6082d37b05931d12e5600d36d4e8c23898e6ccd971f5b153261f2f9c5511206676359f10be5b11c1dc6a9b4ac3ad187fabbead73ecc0d612c7a3b808950','2015-09-13 22:32:42');
/*!40000 ALTER TABLE `members_tb` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-26  0:37:18
