CREATE DATABASE  IF NOT EXISTS `kitabkord_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `kitabkord_db`;
-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: kitabkord_db
-- ------------------------------------------------------
-- Server version	5.5.41-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `note_tb`
--

DROP TABLE IF EXISTS `note_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `note_tb` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `note_value` varchar(100) NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `note_tb`
--

LOCK TABLES `note_tb` WRITE;
/*!40000 ALTER TABLE `note_tb` DISABLE KEYS */;
INSERT INTO `note_tb` VALUES (1,'C'),(2,'C#'),(3,'D'),(4,'D#'),(5,'E'),(6,'F'),(7,'F#'),(8,'G'),(9,'G#'),(10,'A'),(11,'A#'),(12,'B');
/*!40000 ALTER TABLE `note_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `song_info_tb`
--

DROP TABLE IF EXISTS `song_info_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `song_info_tb` (
  `song_id` int(11) NOT NULL AUTO_INCREMENT,
  `song_title` varchar(200) NOT NULL,
  `song_writer` varchar(200) DEFAULT NULL,
  `song_singer` varchar(200) NOT NULL,
  `song_album` varchar(200) DEFAULT NULL,
  `song_tag_id` varchar(100) NOT NULL,
  `song_base_note_id` int(11) NOT NULL,
  `song_lyric` longtext NOT NULL,
  `song_thumbs` int(11) DEFAULT NULL,
  `song_youtube` varchar(100) DEFAULT NULL,
  `song_itunes` varchar(100) DEFAULT NULL,
  `song_spotify` varchar(100) DEFAULT NULL,
  `song_google_play` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`song_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `song_info_tb`
--

LOCK TABLES `song_info_tb` WRITE;
/*!40000 ALTER TABLE `song_info_tb` DISABLE KEYS */;
INSERT INTO `song_info_tb` VALUES (1,'Flawless','','MercyMe','Welcome to the New','1, 6',8,'Intro: G 1x\r\n\r\nVerse 1:\r\n                  G\r\nThere\'s got to be more, than going back and forth \r\n     D\r\nFrom doing right to doing wrong \'cause we were taught that\'s who we are \r\nC\r\nCome on get in line right behind me you along with everybody \r\nG\r\nThinking there\'s worth in what you do \r\n\r\n\r\nG\r\nThen Like a hero who takes the stage when we\'re\r\nD                                        C\r\non the edge of our seats saying it\'s too late \r\n                             G\r\nWell let me introduce you to amazing grace \r\n\r\nChorus:\r\nG\r\nNo matter the bumps, no matter the bruises\r\nD \r\nNo matter the scars, still the truth is \r\n    C                                      G\r\nThe cross has made, the cross has made you flawless (the cross has made you flawless)\r\nG\r\nNo matter the hurt or how deep the wound is \r\nD\r\nNo matter the pain, still the truth is \r\n    C                                      G\r\nThe cross has made, the cross has made you flawless\r\n\r\nVerse 2:\r\n                 G\r\nCould it possibly be, that we simply can\'t believe \r\n          D\r\nThat this unconditional, kind of love would be enough to\r\nC\r\ntake a filthy wretch like this and wrap him up in righteousness \r\nG\r\nBut that\'s exactly what He did \r\n\r\n(Chorus)\r\n\r\nBridge:\r\nF       C            G\r\nOh yeah oooh-ooo-ooo oooh-ooo-ooo\r\n F\r\nTake a breath smile and say \r\n\r\nRight here right now I\'m ok \r\nC                     G\r\nBecause the cross was enough \r\n\r\n\r\nG\r\nThen Like a hero who takes the stage when we\'re\r\nD                                        C\r\non the edge of our seats saying it\'s too late \r\n                             G\r\nWell let me introduce you to grace grace God\'s grace \r\n\r\n(Chorus)\r\n\r\n\r\nG                                              D\r\nNo matter what they say, or what you think you are\r\n                                         C\r\nThe day you called His name, He made you flawless\r\n            G\r\nHe made you flawless\r\n\r\n\r\nG\r\nNo matter the bumps, no matter the bruises \r\nD\r\nNo matter the scars, still the truth is \r\n    C                                      G\r\nThe cross has made, the cross has made you flawless',0,'wjLlLPZderk','','',''),(2,'Sejauh Timur','','Nikita',NULL,'2, 6',1,'Intro: C F G Dm G C 1x\r\n\r\nVerse:\r\n  C         Am                Em           F\r\nSejauh Timur dari Barat Engkau membuang dosaku\r\n   Dm            G             C  G\r\nTiada kau ingat lagi pelanggaranku\r\n  C           Am               Em            F\r\nJauh ke dalam tubir laut Kau melemparkan dosaku\r\n   Dm            G              C  G\r\nTiada kau perhitungkan kesalahanku\r\n\r\nReff:\r\n          C     C        F\r\nBetapa besar kasih pengampunanMu Tuhan\r\n        Dm            G            C  G\r\nTak Kau pandang hina hati yang hancur\r\n          C     C      F\r\nKu berterimakasih kepadaMu ya Tuhan\r\n      Dm             G             C\r\nPengampunan yang kau beri pulihkanku',0,'nj-Cw3nepI0','','',''),(3,'Jangan Lelah','','Franky Sihombing',NULL,'1, 6',8,'Intro: G Am D C G 1x\r\n\r\nVerse: \r\n           G                        Em\r\nJangan lelah bekerja di ladangnya Tuhan\r\n                       C                Am            D\r\nRoh Kudus yang b\'ri kekuatan yang mengajar dan menopang\r\n          G                      Em\r\nTiada lelah bekerja bersamamu Tuhan\r\n                    C    D                      G\r\nYang selalu mencukupkan ...........akan segalanya\r\n\r\nReff:\r\n           G          D\r\nRatakan tanah bergelombang\r\n             Em         C   Am\r\nTimbunlah tanah yang berlubang\r\n           G       Am                 D\r\nMenjadi siap dibangun di atas dasar iman\r\n\r\n\r\n           G          D\r\nRatakan tanah bergelombang\r\n             Em          C    Am\r\nTimbunlah tanah yang berlubang\r\n           G         Am      D         G\r\nMenjadi siap dibangun di atas dasar iman',50,'JnyW00ZEFjY','','',''),(4,'Bagaikan Bejana','','','','1, 2, 6',8,'Intro: Em Am C D G 1x\r\n\r\nVerse 1:\r\nG                 Am\r\nBagaikan bejana siap dibentuk\r\n      D            C      G\r\nDemikian hidupku ditangan-Mu\r\n      G          C\r\nDengan urapan kuasa Roh-Mu\r\n     A           D\r\nKu dibaharui selalu\r\n\r\nVerse 2:\r\n   G              C \r\nJadikan ku alat dalam rumah-Mu\r\n   D              C      G\r\nInilah hidupku di tangan-Mu\r\n       G              C\r\nBentuklah s\'turut kehendak-Mu\r\n      Am          D      G\r\nPakailah sesuai rencana-Mu\r\n\r\nChorus:\r\nG              Am\r\nKu mau s\'perti-Mu Yesus\r\nD          G\r\nDisempurnakan selalu\r\nEm             Am\r\nDalam segenap jalanku\r\n  C       D    G\r\nMemuliakan nama-Mu',0,'PqELhwMaYu0','','',''),(5,'Terima Kasih Tuhan','','Niko Njotorahardjo','','2, 6',8,'Intro: D C G Em7 Am7 D G 1x\r\n\r\nVerse:\r\n           G           Em            Am7\r\nT\'rima kasih Tuhan untuk kasih setiaMu\r\n              D                  C       G    D\r\nYang ku alami dalam hidupku\r\n           G            Em              C      Am7\r\nT\'rima kasih Yesus untuk kebaikanMu\r\n           C     D  G        D\r\nSepanjang hidupku\r\n\r\nChorus:\r\n  G        C    G     D\r\nT\'rima kasih Yesusku\r\n    G     C          A7          D\r\nBuat anugrah yang Kau b\'ri\r\n             C Cm           G    Em7\r\nS\'bab hari ini Tuhan adakan\r\nAm7     D    G\r\nSyukur bagiMu',0,'KE3edrlvshw','','',''),(6,'Seperti Rusa Rindu SungaiMu','','','','2, 6',1,'Intro: C Em Am C7 F G C 1x\r\n\r\nVerse 1:\r\nC       Em      Am    C7\r\nS\'perti rusa rindu sungai-Mu\r\n    F     G     C    G7\r\nJiwaku rindu Engkau\r\nC      Em       Am    C7\r\nKaulah Tuhan hasrat hatiku\r\n     F     G      C\r\nKurindu menyembah-Mu\r\n\r\nPre-Chorus:\r\nAm                   F       C\r\nEengkau kekuatan dan perisaiku\r\n   F             Dm   E\r\nKepada-Mu rohku berserah\r\n\r\nVerse 2:\r\nC      Em       Am    C7\r\nKaulah Tuhan hasrat hatiku\r\n     F     G      C  G7\r\nKurindu menyembah-Mu\r\n\r\nChorus:\r\nAm Em  F  C        F    Em     Dm   G\r\nYesus, Yesus, Kau berarti bagiku.......\r\nAm Em  F  C        Dm     G      C\r\nYesus, Yesus, Kau segalanya bagiku',0,'FN7rircGG-o','','',''),(7,'JanjiMu Seperti Fajar','','Franky Sihombing','','2, 6',6,'Intro: F C Gm C F 1x\r\n\r\nVerse 1:\r\n    F                 Am\r\nKetika kuhadapi kehidupan ini\r\n      Bb        Gm          C\r\nJalan mana yang harus kupilih\r\n    Dm          A7\r\nKutahu kutak mampu\r\n    F/C          G/B\r\nKutahu kutak sanggup\r\n      Bb           Gm          C\r\nHanya Kau Tuhan tempat jawabanku\r\n\r\nVerse 2:\r\n   F                        Am\r\nAkupun tahu kutak pernah sendiri\r\n         Bb        Gm              C\r\nS\'bab Engkau Allah yang menggendongku\r\n       Dm      A7           F         G\r\nTangan-Mu membelaiku, cinta-Mu memuaskanku\r\n        Bb          Gm              C\r\nKau mengangkatku ke tempat yang tinggi\r\n\r\nChorus:\r\n      F                       C   \r\nJanji-Mu s\'perti fajar pagi hari\r\n      Gm          C           F    Bb C\r\nDan tiada pernah terlambat bersinar\r\n      F                           C\r\nCinta-Mu s\'perti sungai yang mengalir\r\n      Gm           C          F\r\nDan kutahu betapa dalam Kasih-Mu',0,'AT1JWhDOJos','','','');
/*!40000 ALTER TABLE `song_info_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag_tb`
--

DROP TABLE IF EXISTS `tag_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag_tb` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_value` varchar(100) NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag_tb`
--

LOCK TABLES `tag_tb` WRITE;
/*!40000 ALTER TABLE `tag_tb` DISABLE KEYS */;
INSERT INTO `tag_tb` VALUES (1,'praise'),(2,'worship'),(3,'christmas'),(4,'easter'),(5,'latin'),(6,'eucharistic'),(7,'holy-friday'),(8,'other');
/*!40000 ALTER TABLE `tag_tb` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-27  0:59:25
CREATE DATABASE  IF NOT EXISTS `kitabkord_secure_login_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `kitabkord_secure_login_db`;
-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: kitabkord_secure_login_db
-- ------------------------------------------------------
-- Server version	5.5.41-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `login_attempts_tb`
--

DROP TABLE IF EXISTS `login_attempts_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_attempts_tb` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_attempts_tb`
--

LOCK TABLES `login_attempts_tb` WRITE;
/*!40000 ALTER TABLE `login_attempts_tb` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempts_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `members_tb`
--

DROP TABLE IF EXISTS `members_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members_tb`
--

LOCK TABLES `members_tb` WRITE;
/*!40000 ALTER TABLE `members_tb` DISABLE KEYS */;
INSERT INTO `members_tb` VALUES (1,'test_user','test@example.com','00807432eae173f652f2064bdca1b61b290b52d40e429a7d295d76a71084aa96c0233b82f1feac45529e0726559645acaed6f3ae58a286b9f075916ebf66cacc','f9aab579fc1b41ed0c44fe4ecdbfcdb4cb99b9023abb241a6db833288f4eea3c02f76e0d35204a8695077dcf81932aa59006423976224be0390395bae152d4ef'),(2,'root','root@root.com','b6829a0ba3abe5f07b8b6eed79c095cdc1d201a312724b93e878f0c577236711d7737374708211e0431d2972abc96b0015198c9c5cf168bdfcb13dbd67655208','b58b24c70d0980c37bea830525b2e01cc73f6f4ea572bc252e8261ffabe4f3ae2b22c4c1db3f9349d7292ba13e7f61e8a6b6393f4c07a148b878b00bf8a8fddd');
/*!40000 ALTER TABLE `members_tb` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-27  0:59:25
