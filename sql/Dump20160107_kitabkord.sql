-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: kitabkord_db
-- ------------------------------------------------------
-- Server version	5.5.41-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category_song_tb`
--

DROP TABLE IF EXISTS `category_song_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_song_tb` (
  `category_id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_song_tb`
--

LOCK TABLES `category_song_tb` WRITE;
/*!40000 ALTER TABLE `category_song_tb` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_song_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_tb`
--

DROP TABLE IF EXISTS `category_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_tb` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_tb`
--

LOCK TABLES `category_tb` WRITE;
/*!40000 ALTER TABLE `category_tb` DISABLE KEYS */;
INSERT INTO `category_tb` VALUES (1,'Pop',NULL),(2,'Jazz',NULL),(3,'Blues',NULL),(4,'R&B',NULL),(5,'Classic',NULL),(6,'Religious',NULL);
/*!40000 ALTER TABLE `category_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `note_tb`
--

DROP TABLE IF EXISTS `note_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `note_tb` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `note_value` varchar(100) NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `note_tb`
--

LOCK TABLES `note_tb` WRITE;
/*!40000 ALTER TABLE `note_tb` DISABLE KEYS */;
INSERT INTO `note_tb` VALUES (1,'C'),(2,'C#'),(3,'D'),(4,'D#'),(5,'E'),(6,'F'),(7,'F#'),(8,'G'),(9,'G#'),(10,'A'),(11,'A#'),(12,'B');
/*!40000 ALTER TABLE `note_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlists_songs_tb`
--

DROP TABLE IF EXISTS `playlists_songs_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlists_songs_tb` (
  `playlists_songs_id` int(11) NOT NULL AUTO_INCREMENT,
  `playlists_songs_playlist_id` int(11) NOT NULL,
  `playlists_songs_song_id` int(11) NOT NULL,
  `playlists_songs_song_idx` int(11) DEFAULT NULL,
  `playlists_songs_target_note_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`playlists_songs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlists_songs_tb`
--

LOCK TABLES `playlists_songs_tb` WRITE;
/*!40000 ALTER TABLE `playlists_songs_tb` DISABLE KEYS */;
INSERT INTO `playlists_songs_tb` VALUES (49,19,13,1,8),(50,19,12,2,8),(51,21,12,1,8),(52,19,14,3,8),(53,22,14,1,8),(54,21,14,2,8),(55,19,11,4,1),(57,21,11,3,1),(58,22,11,2,1),(59,20,11,1,5),(60,25,13,1,8),(65,27,12,1,8),(66,28,12,1,8);
/*!40000 ALTER TABLE `playlists_songs_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlists_tb`
--

DROP TABLE IF EXISTS `playlists_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlists_tb` (
  `playlist_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `playlist_name` varchar(50) NOT NULL,
  PRIMARY KEY (`playlist_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlists_tb`
--

LOCK TABLES `playlists_tb` WRITE;
/*!40000 ALTER TABLE `playlists_tb` DISABLE KEYS */;
INSERT INTO `playlists_tb` VALUES (1,'0','root'),(2,'0','dummy_playlist'),(3,'0','test3'),(4,'0','test4'),(5,'0','test5'),(6,'0','test6'),(19,'root','KTM PW 12-09'),(20,'root','KTM PW 10-11'),(21,'root','KTM PW 11-20'),(22,'root','KTM PW 11-30'),(23,'root','KTM PW 07-03'),(24,'testing','WKICU - 12-25'),(25,'testing','WKICU - 01-05'),(26,'testing','WKICU - 04-10'),(27,'root','Gladi Resik '),(28,'root','Paskah');
/*!40000 ALTER TABLE `playlists_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `song_info_tb`
--

DROP TABLE IF EXISTS `song_info_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `song_info_tb` (
  `song_id` int(11) NOT NULL AUTO_INCREMENT,
  `song_title` varchar(200) NOT NULL,
  `song_writer` varchar(200) DEFAULT NULL,
  `song_singer` varchar(200) NOT NULL,
  `song_album` varchar(200) DEFAULT NULL,
  `song_base_note_id` int(11) NOT NULL,
  `song_lyric` longtext NOT NULL,
  `song_thumbs` int(11) DEFAULT NULL,
  `song_youtube` varchar(100) DEFAULT NULL,
  `song_itunes` varchar(100) DEFAULT NULL,
  `song_spotify` varchar(100) DEFAULT NULL,
  `song_google_play` varchar(100) DEFAULT NULL,
  `song_approval_status` varchar(100) NOT NULL,
  `song_username` varchar(30) NOT NULL,
  `song_submit_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`song_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `song_info_tb`
--

LOCK TABLES `song_info_tb` WRITE;
/*!40000 ALTER TABLE `song_info_tb` DISABLE KEYS */;
INSERT INTO `song_info_tb` VALUES (11,'Sejauh Timur','','Nikita','',1,':Intro: C F G Dm G C 1x\r\n\r\n:Verse:\r\n  C         Am                Em           F\r\nSejauh Timur dari Barat Engkau membuang dosaku\r\n   Dm            G             C  G\r\nTiada kau ingat lagi pelanggaranku\r\n  C           Am               Em            F\r\nJauh ke dalam tubir laut Kau melemparkan dosaku\r\n   Dm            G              C  G\r\nTiada kau perhitungkan kesalahanku\r\n\r\n:Reff:\r\n          C     C        F\r\nBetapa besar kasih pengampunanMu Tuhan\r\n        Dm            G            C  G\r\nTak Kau pandang hina hati yang hancur\r\n          C     C      F\r\nKu berterimakasih kepadaMu ya Tuhan\r\n      Dm             G             C\r\nPengampunan yang kau beri pulihkanku',0,'nj-Cw3nepI0','','','','approved','testing','2015-09-18 06:46:49'),(12,'Jangan Lelah                ','','Franky Sihombing','',8,':Intro: G Am D C G 1x\r\n\r\n:Verse: \r\n           G                        Em\r\nJangan lelah bekerja di ladangnya Tuhan\r\n                       C                Am            D\r\nRoh Kudus yang b\'ri kekuatan yang mengajar dan menopang\r\n          G                      Em\r\nTiada lelah bekerja bersamamu Tuhan\r\n                    C    D                      G\r\nYang selalu mencukupkan ...........akan segalanya\r\n\r\n:Reff:\r\n           G          D\r\nRatakan tanah bergelombang\r\n             Em         C   Am\r\nTimbunlah tanah yang berlubang\r\n           G       Am                 D\r\nMenjadi siap dibangun di atas dasar iman',0,'JnyW00ZEFjY','','','','approved','root','2015-09-18 06:47:40'),(13,'Flawless                ','','MercyMe','',8,':Intro: G 1x\r\n\r\n:Verse 1:\r\n                  G\r\nThere\'s got to be more, than going back and forth \r\n     D\r\nFrom doing right to doing wrong \'cause we were taught that\'s who we are \r\nC\r\nCome on get in line right behind me you along with everybody \r\nG\r\nThinking there\'s worth in what you do \r\n\r\nG\r\nThen Like a hero who takes the stage when we\'re\r\nD                                        C\r\non the edge of our seats saying it\'s too late \r\n                             G\r\nWell let me introduce you to amazing grace \r\n\r\n:Chorus:\r\nG\r\nNo matter the bumps, no matter the bruises\r\nD \r\nNo matter the scars, still the truth is \r\n    C                                      G\r\nThe cross has made, the cross has made you flawless (the cross has made you flawless)\r\nG\r\nNo matter the hurt or how deep the wound is \r\nD\r\nNo matter the pain, still the truth is \r\n    C                                      G\r\nThe cross has made, the cross has made you flawless\r\n\r\n:Verse 2:\r\n                 G\r\nCould it possibly be, that we simply can\'t believe \r\n          D\r\nThat this unconditional, kind of love would be enough to\r\nC\r\ntake a filthy wretch like this and wrap him up in righteousness \r\nG\r\nBut that\'s exactly what He did \r\n\r\n:(Chorus)\r\n\r\n:Bridge:\r\nF       C            G\r\nOh yeah oooh-ooo-ooo oooh-ooo-ooo\r\n F\r\nTake a breath smile and say \r\nF\r\nRight here right now I\'m ok \r\nC                     G\r\nBecause the cross was enough \r\n\r\nG\r\nThen Like a hero who takes the stage when we\'re\r\nD                                        C\r\non the edge of our seats saying it\'s too late \r\n                             G\r\nWell let me introduce you to grace grace \r\nG\r\nGod\'s grace \r\n\r\n:(Chorus)\r\n\r\nG                                              D\r\nNo matter what they say, or what you think you are\r\n                                         C\r\nThe day you called His name, He made you flawless\r\n            G\r\nHe made you flawless\r\n\r\nG\r\nNo matter the bumps, no matter the bruises \r\nD\r\nNo matter the scars, still the truth is \r\n    C                                      G\r\nThe cross has made, the cross has made you flawless',0,'wjLlLPZderk','','','','approved','testing','2015-09-18 06:53:17'),(14,'Amazing Grace                                 ','','Chris Tomlin','',8,':Intro: G C G G D7 G 1x\r\n\r\n:Verse 1:\r\n       G            C          G\r\nAmazing Grace, how sweet the sound,\r\n			 D7\r\nThat saved a wretch like me.\r\n     G                C       G\r\nI once was lost, but now im found,\r\n               D7     G\r\nWas blind, but now I see.\r\n\r\n:Verse 2:\r\n    G                        C       G\r\nTwas grace that thought my heart to fear,\r\n                      D7            G\r\nand grace my fears released. How precious\r\n      C          G                  D7     G\r\ndid that grace appear, the  hour I first believed.\r\n\r\n:Verse 3:\r\n G                           C       G\r\nWhen we\'ve been there ten thousand years,\r\n G                     D7\r\nbright shining as the sun,\r\n  G                     C           G\r\nwe\'ve no less days to sing god\'s praise\r\n  G                D7         G\r\nthan when did when we first begun.\r\n\r\n:Verse 4:\r\nG            C          G\r\nAmazing Grace, how sweet the sound,\r\n			 D7\r\nThat saved a wretch like me.\r\n     G                C       G\r\nI once was lost, but now im found,\r\n               D7     G\r\nWas blind, but now I see.',0,'Jbe7OruLk8I','','','','approved','root','2015-09-18 07:03:49'),(31,'Dummy','','Dummy Band','',7,'Dummy Title',0,'','','','','waiting','root','2016-01-06 09:36:26');
/*!40000 ALTER TABLE `song_info_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag_song_tb`
--

DROP TABLE IF EXISTS `tag_song_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag_song_tb` (
  `song_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag_song_tb`
--

LOCK TABLES `tag_song_tb` WRITE;
/*!40000 ALTER TABLE `tag_song_tb` DISABLE KEYS */;
INSERT INTO `tag_song_tb` VALUES (11,2),(12,1),(13,1),(14,2),(14,3),(31,5),(31,7);
/*!40000 ALTER TABLE `tag_song_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag_tb`
--

DROP TABLE IF EXISTS `tag_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag_tb` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_value` varchar(100) NOT NULL,
  `tag_picture` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag_tb`
--

LOCK TABLES `tag_tb` WRITE;
/*!40000 ALTER TABLE `tag_tb` DISABLE KEYS */;
INSERT INTO `tag_tb` VALUES (1,'praise','praise.jpg'),(2,'worship','worship.jpg'),(3,'christmas','christmas.jpg'),(4,'easter','easter.jpg'),(5,'latin','latin_mass.jpg'),(6,'eucharistic','eucharistic.jpg'),(7,'holy-friday','holy_friday.jpg'),(8,'other','other.jpg');
/*!40000 ALTER TABLE `tag_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thumb_tb`
--

DROP TABLE IF EXISTS `thumb_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thumb_tb` (
  `thumb_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `song_id` int(11) NOT NULL,
  PRIMARY KEY (`thumb_id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thumb_tb`
--

LOCK TABLES `thumb_tb` WRITE;
/*!40000 ALTER TABLE `thumb_tb` DISABLE KEYS */;
INSERT INTO `thumb_tb` VALUES (76,'root',14),(83,'root',12);
/*!40000 ALTER TABLE `thumb_tb` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-07  1:20:33
