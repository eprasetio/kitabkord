<?php
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/functions.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_functions.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_dbconnect.php';

sec_session_start();

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <?php headTag(); ?>
  </head>

  <body>
    <?php navBar($mysqli); ?>

    <div>
      <img class="cover-img" src="/kitabkord/img/guitar11.jpg">
        <div class="main-search-box-holder">
          <div class="main-search-table">
            <div class="main-search-cell">
              <div class="main-search-box">
                <h1>Cari kord lagu favorit mu!</h1>
                <!--  Search Form -->
                <form class="main-search-form form-inline" action="listing_page.php" method="get">
                  <div class="form-group">
                    <textarea class="main-form-control form-control" id="lyricForm" name="search_song_info" rows="1" placeholder="ie: Judul Lagu, penyanyi, penulis, etc"></textarea>
                  </div>
                  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search search-icon" aria-hidden="true"></span></button>
                </form>
              </div>
            </div>
          </div>
        </div> 
      </img>
    </div>

<!--     <div class="container main-container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
          <div class="row mainbar-offcanvas"> -->

<!--           </div> --><!--/row-->
<!--         </div> --><!--/.col-xs-12.col-sm-9-->

<!--         <div class="col-xs-6 col-sm-3" id="sidebar">
          <div class="row sidebar-offcanvas">
            <?php facebookPage(); ?>
          </div>
        </div> --><!--/.sidebar-offcanvas-->
<!--       </div> --><!--/row-->

<!--     </div> --><!--/.container-->

    <footer>
      <?php footerTag(); ?>
    </footer>

    <?php footerInclude(); ?>
  </body>
</html>



