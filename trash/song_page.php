<?php
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_dbconnect.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_functions.php';
sec_session_start();

require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/functions.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/classes/song_manager.php';

$song_id = ( isset( $_GET['song_id'] ) ) ? $_GET['song_id'] : 1;

$song_mgr = new song_manager();

$song_thumbs = $song_mgr->getThumb($song_id);
$song_data = $song_mgr->getData($song_id);
$song_lyric_temp = $song_data['song_lyric'];

$song_base_note = $song_data['note_value'];
$song_lyric_default = parseSong($song_lyric_temp, $song_base_note, $song_base_note);
$song_title = $song_data['song_title'];
$song_singer = $song_data['song_singer'];
$song_writer = $song_data['song_writer'];
$song_album = $song_data['song_album'];
$song_youtube = $song_data['song_youtube'];
$song_itunes = $song_data['song_itunes'];
$song_spotify = $song_data['song_spotify'];
$song_google_play = $song_data['song_google_play'];
$song_username = $song_data['song_username'];
$song_submit_time = $song_data['song_submit_time'];
$song_approval_status = $song_data['song_approval_status'];

$song_tag_id = $song_data['song_tag_id'];
$song_tag_array = $song_mgr->getTag($song_id, $song_tag_id);

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <?php headTag(); ?>
  </head>

  <body>
    <?php navBar($mysqli); ?>

    <div class="container main-container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>
          <div class="row mainbar-offcanvas">

            <?php 
              if(!empty($song_title)){
                echo '<h1>' . $song_title ; 
              }
              if(!empty($song_singer)){
                echo ' - ' . $song_singer . '</h3>';
              }else{
                echo '</h1>';
              }

            ?>

            <div class="song-page-subtitle">
            <?php

              if(!empty($song_username)){
                echo '<h6>Posted by: ' . $song_username . ' on ' . $song_submit_time . '</h6>';
              }
            ?>

              <div class="thumbs-template">
                <p id="song-thumbs-count"> <?= $song_thumbs ?></p>

                <?php
                if (login_check($mysqli) == true) {
                ?>
                  <span onclick="updateThumbsCount('<?= $_SESSION['username'] ?>', <?= $song_id ?>)" class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                <?php
                }else{
                ?>
                  <span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                <?php
                }
                ?>
              </div>
              
          
            </div>

            <?php
              echo '<hr>';
              
              if(!empty($song_writer)){
                echo '<h5>Written by: ' . $song_writer . '</h5>';
              }

              if(!empty($song_album)){
                echo '<h5>Album: ' . $song_album . '</h5>';
              }
            ?>
            
            <br>

            <iframe class="youtube-video" src="https://www.youtube.com/embed/<?=$song_youtube?>" frameborder="0" allowfullscreen></iframe>

            <br>
            <br>

            <div id="DEBUG">This is a place for debug messages</div>

            <!-- Small modal for adding playlist -->
            <button type="button" class="btn btn-primary" onclick="getPlaylistsModal('<?= $_SESSION['username'] ?>', <?= $song_id ?>)" data-toggle="modal" data-target=".bs-example-modal-sm">
              <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            </button>

            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
              <div class="modal-dialog modal-sm">
                <div class="modal-content playlists-container">
                <?php
                  if (login_check($mysqli) == true) {
                ?>
                  <!-- Get playlists -->
                  <div id="playlists-list"></div>
                  <hr>
                  <!-- Display create playlist form -->
                  <div class="form-group">
                    <label for="new_playlist_name">Create new playlist:</label>
                    <textarea class="form-control" id="new_playlist_name"  rows="1" placeholder="write playlist name here" required="required"></textarea>
                  </div>
                  <button onclick="createPlaylistModal('<?= $_SESSION['username'] ?>', <?= $song_id ?>)">Create</button>
                <?php 
                }else{
                ?>
                  <p>Please log in to manage your playlist</p>
                <?php
                }
                ?>
                </div>
              </div>
            </div>


            <div class="btn-toolbar" role="toolbar" aria-label="chord-trans-btn">
              <div class="btn-group chord-trans-btn" id="chord-trans-btn-1" onclick="updateLyricChord('<?=$song_base_note?>', 'C')" role="group" aria-label="chord-trans-btn">C</div>
              <div class="btn-group chord-trans-btn" id="chord-trans-btn-2" onclick="updateLyricChord('<?=$song_base_note?>', 'C#')" role="group" aria-label="chord-trans-btn">C#</div>
              <div class="btn-group chord-trans-btn" id="chord-trans-btn-3" onclick="updateLyricChord('<?=$song_base_note?>', 'D')" role="group" aria-label="chord-trans-btn">D</div>
              <div class="btn-group chord-trans-btn" id="chord-trans-btn-4" onclick="updateLyricChord('<?=$song_base_note?>', 'D#')" role="group" aria-label="chord-trans-btn">D#</div>
              <div class="btn-group chord-trans-btn" id="chord-trans-btn-5" onclick="updateLyricChord('<?=$song_base_note?>', 'E')" role="group" aria-label="chord-trans-btn">E</div>
              <div class="btn-group chord-trans-btn" id="chord-trans-btn-6" onclick="updateLyricChord('<?=$song_base_note?>', 'F')" role="group" aria-label="chord-trans-btn">F</div>
              <div class="btn-group chord-trans-btn" id="chord-trans-btn-7" onclick="updateLyricChord('<?=$song_base_note?>', 'F#')" role="group" aria-label="chord-trans-btn">F#</div>
              <div class="btn-group chord-trans-btn" id="chord-trans-btn-8" onclick="updateLyricChord('<?=$song_base_note?>', 'G')" role="group" aria-label="chord-trans-btn">G</div>
              <div class="btn-group chord-trans-btn" id="chord-trans-btn-9" onclick="updateLyricChord('<?=$song_base_note?>', 'G#')" role="group" aria-label="chord-trans-btn">G#</div>
              <div class="btn-group chord-trans-btn" id="chord-trans-btn-10" onclick="updateLyricChord('<?=$song_base_note?>', 'A')" role="group" aria-label="chord-trans-btn">A</div>
              <div class="btn-group chord-trans-btn" id="chord-trans-btn-11" onclick="updateLyricChord('<?=$song_base_note?>', 'A#')" role="group" aria-label="chord-trans-btn">A#</div>
              <div class="btn-group chord-trans-btn" id="chord-trans-btn-12" onclick="updateLyricChord('<?=$song_base_note?>', 'B')" role="group" aria-label="chord-trans-btn">B</div>
            </div>

            <br>
            
            <div id="song-target-note-template" style="display:none;"><?= $song_base_note ?></div>
            <div class="btn-group get-text-btn" onclick="getSongInText('<?=$song_base_note?>', '<?=$song_title?>', '<?=$song_singer?>')" role="group" aria-label="chord-trans-btn">Get Text</div>
            <a id="get-text-dl-btn" style="display:hidden;" href="/kitabkord/doc/song.txt" target="_blank"></a>

            <br>

          	<!-- Printed Lyric -->
<!--           	<p><?=$song_lyric?></p> -->
            <div id='song-lyric-temp'><?= $song_lyric_temp ?></div>
            <div id='song-lyric-template'><?= $song_lyric_default?></div>
          	<p><br><br></p>

            <?
              if(!empty($song_tag_array)){
                echo '<ul class="list-inline tag-list">Tag(s): ';
                foreach($song_tag_array as $tag){
                  echo '<li>' . $tag . '</li>';
                }
                echo '</ul>';
              }

              if(!empty($song_itunes)){
                echo '<h5>Itunes: ' . $song_itunes . '</h5>';
              }
              if(!empty($song_spotify)){
                echo '<h5>Spotify: ' . $song_spotify. '</h5>';
              }

            ?>
    
          </div><!--/row-->
        </div><!--/.col-xs-12.col-sm-9-->

        <div class="col-xs-6 col-sm-3" id="sidebar">
          <div class="row sidebar-offcanvas">
            <?php facebookPage(); ?>
          </div>
        </div><!--/.sidebar-offcanvas-->
      </div><!--/row-->

    </div><!--/.container-->

    <footer>
      <?php footerTag(); ?>
    </footer>


    <?php footerInclude(); ?>

    <!-- Call js function to mark the foundation tone the first time -->
    <script>
      markBaseNoteBtn('G');
    </script>

  </body>
</html>



