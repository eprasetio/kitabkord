<?php
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_dbconnect.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_functions.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/functions.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/register.inc.php';

  sec_session_start();

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <?php headTag(); ?>
  </head>

  <body>
    <?php navBar($mysqli); ?>

    <div class="container main-container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>
          <div class="row mainbar-offcanvas">

            <!-- Registration form to be output if the POST variables are not
            set or if the registration script caused an error. -->
            <h1>Register with us</h1>
            <?php
            if (!empty($error_msg)) {
                echo $error_msg;
            }
            ?>
            <ul>
                <li>Usernames may contain only digits, upper and lower case letters and underscores</li>
                <li>Emails must have a valid email format</li>
                <li>Passwords must be at least 6 characters long</li>
                <li>Passwords must contain
                    <ul>
                        <li>At least one uppercase letter (A..Z)</li>
                        <li>At least one lower case letter (a..z)</li>
                        <li>At least one number (0..9)</li>
                    </ul>
                </li>
                <li>Your password and confirmation must match exactly</li>
            </ul>
            <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" 
                    method="post" 
                    name="registration_form">
                Username: <input type='text' 
                    name='username' 
                    id='username' /><br>
                Email: <input type="text" name="email" id="email" /><br>
                Password: <input type="password"
                                 name="password" 
                                 id="password"/><br>
                Confirm password: <input type="password" 
                                         name="confirmpwd" 
                                         id="confirmpwd" /><br>
                <input type="button" 
                       value="Register" 
                       onclick="return regformhash(this.form,
                                       this.form.username,
                                       this.form.email,
                                       this.form.password,
                                       this.form.confirmpwd);" /> 
            </form>
            <p>Return to the <a href="/kitabkord/login/login.php">login page</a>.</p>

          </div><!--/row-->
        </div><!--/.col-xs-12.col-sm-9-->

        <div class="col-xs-6 col-sm-3" id="sidebar">
          <div class="row sidebar-offcanvas">
            <?php facebookPage(); ?>
          </div>
        </div><!--/.sidebar-offcanvas-->
      </div><!--/row-->

    </div><!--/.container-->


    <?php footerInclude(); ?>
  </body>
</html>


