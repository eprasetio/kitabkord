<?php
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/classes/table_manager.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/classes/data_manager.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/classes/login_manager.php';

echo "Setup database...";

$tb_mgr = new table_manager();
$tb_mgr->delete_all_tables();
$tb_mgr->create_all_tables();

$data_mgr = new data_manager();
$data_mgr->insert_note_data();
$data_mgr->insert_tag_data();

echo "Setup database completed";

echo "Setup login database...";

$lg_mgr= new login_manager();
$lg_mgr->delete_all_tables();
$lg_mgr->create_all_tables();
$lg_mgr->insert_admin_usr();
$lg_mgr->insert_test_usr();

echo "Setup login database completed";

?>



