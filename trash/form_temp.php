<?php
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/classes/song_manager.php';

// init song manager object
$song_mgr = new song_manager();

if(isset($_POST['song_form_submit']))
{
	insert_song_info($song_mgr);
} 

function insert_song_info($song_mgr)
{	

	$song_title = mysql_real_escape_string( preg_replace('/\s{2,}/', ' ', $_POST['song_title'] ) );
	$song_singer = mysql_real_escape_string( preg_replace('/\s{2,}/', ' ', $_POST['song_singer'] ) );
	$song_writer = mysql_real_escape_string( preg_replace('/\s{2,}/', ' ', $_POST['song_writer'] ) );
	$song_album = mysql_real_escape_string( preg_replace('/\s{2,}/', ' ', $_POST['song_album'] ) );
	$song_tag_id = implode(", ", $_POST['song_tag_id']);
	$song_base_note_id = $_POST['song_base_note_id'];
	echo 'Youtube:' . $song_youtube . '<br>';
	echo 'Itunes:' . $song_itunes . '<br>';
	echo 'Spotify:' . $song_spotify . '<br>';
	echo 'Google Play:' . $song_google_play . '<br>';
	$song_lyric = mysql_real_escape_string( $_POST['song_lyric'] );
	$song_thumbs = $_POST['song_thumbs'];
	$song_youtube = urlencode( $_POST['song_youtube'] );
	$song_itunes =  urlencode( $_POST['song_itunes'] );
	$song_spotify = urlencode( $_POST['song_spotify'] );
	$song_google_play = urlencode( $_POST['song_google_play'] );

	echo 'Title:' . $song_title . '<br>';
	echo 'Singer:' . $song_singer . '<br>';
	echo 'Writer:' . $song_writer . '<br>';
	echo 'Album:' . $song_album . '<br>';
	
	echo 'Tag:' . $song_tag_id . '<br>';
	echo 'Base Note:' . $song_base_note_id . '<br>';
	echo 'Lyric:' . $song_lyric . '<br>';
	echo 'Thumbs:' . $song_thumbs . '<br>';



	$song_mgr->insertData(
			$song_title, 
			$song_singer,
			$song_writer, 
			$song_album, 
			$song_tag_id, 
			$song_base_note_id, 
			$song_lyric, 
			$song_thumbs, 
			$song_youtube, 
			$song_itunes, 
			$song_spotify, 
			$song_google_play
		);
}

header('Refresh: 3;url=./../admin_form.php');

?>