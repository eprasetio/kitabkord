<?php
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/functions.php';

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <?php headTag() ?>;
  </head>

  <body>
    <?php navBar(); ?>

    <div class="container main-container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>
          <div class="row mainbar-offcanvas">
          	<!--  Search Form -->
            <form action="listing_page.php" method="get">
              <div class="form-group">
                <label for="lyricForm">Cari lagu:</label>
                <textarea class="form-control" id="lyricForm" name="search_song_info" rows="1" placeholder="ie: Judul Lagu, penyanyi, penulis, etc"></textarea>
              </div>
              <button type="submit" class="btn btn-default">Submit</button>
            </form>       
          </div><!--/row-->
        </div><!--/.col-xs-12.col-sm-9-->

        <div class="col-xs-6 col-sm-3" id="sidebar">
          <div class="row sidebar-offcanvas">
            <?php facebookPage(); ?>
          </div>
        </div><!--/.sidebar-offcanvas-->
      </div><!--/row-->

    </div><!--/.container-->

    <footer>
      <?php footerTag(); ?>
    </footer>

    <?php footerInclude(); ?>
  </body>
</html>



