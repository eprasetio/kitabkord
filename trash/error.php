<?php
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_dbconnect.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_functions.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/functions.php';

  $error = filter_input(INPUT_GET, 'err', $filter = FILTER_SANITIZE_STRING);
 
  if (! $error) {
    $error = 'Oops! An unknown error happened.';
  }

  sec_session_start();

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <?php headTag() ?>;
  </head>

  <body>
    <?php navBar($mysqli); ?>

    <div class="container main-container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>
          <div class="row">

            <h1>There was a problem</h1>
            <p class="error"><?php echo $error; ?></p>  

          </div><!--/row-->
        </div><!--/.col-xs-12.col-sm-9-->

        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">

        </div><!--/.sidebar-offcanvas-->
      </div><!--/row-->

      <hr>

      <footer>
        <?php footerTag(); ?>
      </footer>

    </div><!--/.container-->


    <?php footerInclude(); ?>
  </body>
</html>