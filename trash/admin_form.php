<?php
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/functions.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_dbconnect.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_functions.php';

sec_session_start();

if (login_check($mysqli) == true) { // check if user has login
    $username = $_SESSION['username']; // get current username
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <?php headTag(); ?>
  </head>

  <body>
    <?php navBar($mysqli); ?>

    <div class="container main-container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>
          <div class="row mainbar-offcanvas">
          	<h3>Song Input Form</h3>

          	<!-- Input Form for new lyric -->
            <form method="post" action="includes/form_submit.php">
              <div class="form-group">
                <label for="title">Title:</label>
                <textarea class="form-control" id="title" name="song_title" rows="1" placeholder="Write song title here" required="required"></textarea>
              </div>
              <div class="form-group">
                <label for="singer">Singer(s)/Band:</label>
                <textarea class="form-control" id="singer" name="song_singer" rows="1" placeholder="Write song singer here"></textarea>
              </div>
              <div class="form-group">
                <label for="writer">Writer(s):</label>
                <textarea class="form-control" id="writer" name="song_writer" rows="1" placeholder="Write song writer here"></textarea>
              </div>
              <div class="form-group">
                <label for="album">Album:</label>
                <textarea class="form-control" id="album" name="song_album" rows="1" placeholder="Write song album here"></textarea>
              </div>

              <!-- Base Note Option -->
              <label for="note_radio">Choose Base Note for the song:</label>
              <br>
              <label class="radio-inline">
                <input type="radio" name="song_base_note_id" id="note_radio" value="1" required="required"> C
              </label>
              <label class="radio-inline">
                <input type="radio" name="song_base_note_id" id="note_radio" value="2" required="required"> C#
              </label>
              <label class="radio-inline">
                <input type="radio" name="song_base_note_id" id="note_radio" value="3" required="required"> D
              </label>
              <label class="radio-inline">
                <input type="radio" name="song_base_note_id" id="note_radio" value="4" required="required"> D#
              </label>
              <label class="radio-inline">
                <input type="radio" name="song_base_note_id" id="note_radio" value="5" required="required"> E
              </label>
              <label class="radio-inline">
                <input type="radio" name="song_base_note_id" id="note_radio" value="6" required="required"> F
              </label>
              <br>
              <label class="radio-inline">
                <input type="radio" name="song_base_note_id" id="note_radio" value="7" required="required"> F#
              </label>
              <label class="radio-inline">
                <input type="radio" name="song_base_note_id" id="note_radio" value="8" required="required"> G
              </label>
              <label class="radio-inline">
                <input type="radio" name="song_base_note_id" id="note_radio" value="9" required="required"> G#
              </label>
              <label class="radio-inline">
                <input type="radio" name="song_base_note_id" id="note_radio" value="10" required="required"> A
              </label>
              <label class="radio-inline">
                <input type="radio" name="song_base_note_id" id="note_radio" value="11" required="required"> A#
              </label>
              <label class="radio-inline">
                <input type="radio" name="song_base_note_id" id="note_radio" value="12" required="required"> B
              </label>
              <br>
              <br>

              <div class="form-group">
                <label for="lyricForm">Lyric with Chord:</label>
                <textarea class="form-control lyric-text-area" id="lyricForm" name ="song_lyric" rows="20" placeholder="Write song lyric + chord here" required="required"></textarea>
              </div>

              <!-- Song Tag Option -->
              <label for="tag_checkbox">Choose tag(s) for the song:</label>
              <br>
              <label class="checkbox-inline">
                <input type="checkbox" id="tag_checkbox" name="song_tag_id[]" value="1"> praise
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" id="tag_checkbox" name="song_tag_id[]" value="2"> worship
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" id="tag_checkbox" name="song_tag_id[]" value="3"> christmas
              </label>
               <label class="checkbox-inline">
                <input type="checkbox" id="tag_checkbox" name="song_tag_id[]" value="4"> easter
              </label>
              <br>
              <label class="checkbox-inline">
                <input type="checkbox" id="tag_checkbox" name="song_tag_id[]" value="5"> latin
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" id="tag_checkbox" name="song_tag_id[]" value="6"> eucharistic
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" id="tag_checkbox" name="song_tag_id[]" value="7"> holy friday
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" id="tag_checkbox" name="song_tag_id[]" value="8"> other
              </label>
              <br>
              <br>

              <div class="form-group">
                <label for="thumbs">Thumbs:</label>
                <textarea class="form-control" id="thumbs" name="song_thumbs" rows="1" placeholder="i.e: 10"></textarea>
              </div>
              <div class="form-group">
                <label for="youtube">Youtube Link:</label>
                <textarea class="form-control" id="youtube" name="song_youtube" rows="1" placeholder="Write youtube link for the song here"></textarea>
              </div>
              <div class="form-group">
                <label for="itunes">Itunes Link:</label>
                <textarea class="form-control" id="itunes" name="song_itunes" rows="1" placeholder="Write itunes link for the song here"></textarea>
              </div>
              <div class="form-group">
                <label for="spotify">Spotify Link:</label>
                <textarea class="form-control" id="spotify" name="song_spotify" rows="1" placeholder="Write spotify link for the song here"></textarea>
              </div>
              <div class="form-group">
                <label for="google_play">Google Play Link:</label>
                <textarea class="form-control" id="google_play" name="song_google_play" rows="1" placeholder="Write google play link for the song here"></textarea>
              </div>

              <div class="form-group">
                <label for="username">Submitted by:</label>
                <textarea class="form-control" id="username" name="song_username" value="<?= $username ?>" rows="1"><?= $username ?></textarea>
              </div>

              <button type="submit" class="btn btn-default" value="click" name="song_form_submit">Submit</button>
            </form>       
          </div><!--/row-->
        </div><!--/.col-xs-12.col-sm-9-->

        <div class="col-xs-6 col-sm-3" id="sidebar">
          <div class="row sidebar-offcanvas">

          </div>
        </div><!--/.sidebar-offcanvas-->
      </div><!--/row-->

    </div><!--/.container-->

    <footer>
      <?php footerTag(); ?>
    </footer>


    <?php footerInclude(); ?>
    
  </body>
</html>



