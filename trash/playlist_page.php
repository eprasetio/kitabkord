<?php
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/classes/song_manager.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/functions.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_dbconnect.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_functions.php';

  sec_session_start();

  $song_playlist_id = ( isset( $_GET['song-playlist-id'] ) ) ? $_GET['song-playlist-id'] : "";
  $song_playlist_length = ( isset( $_GET['song-playlist-length'] ) ) ? $_GET['song-playlist-length'] : 0;

  $song_mgr = new song_manager();
  $song_playlist_id_array = explode(" ", $song_playlist_id);

  // for($i=0;$i<$song_playlist_length;$i++){
  //   $song_data = $song_mgr->getData($song_playlist_id_array[$i]);
  // }
  

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <?php headTag() ?>;
  </head>

  <body>
    <?php navBar($mysqli); ?>

    <div class="container main-container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>
          <div class="row mainbar-offcanvas">

          <div class="main-search-box">
            <h1>Current Playlist</h1>
          </div>

          <?php 
          if( $song_playlist_length > 0 ){
          echo '<table class="song-listing-table table table-striped table-condensed table-rounded">';
          echo '  <thead>';
          echo '       <tr>';
          echo '            <th width="5%">#</th>';
          echo '            <th width="5%">Song ID</th>';
          echo '            <th width="10%">Song Title</th>';
          echo '            <th width="5%">Song Writer</th>';
          echo '            <th width="5%">Song Singer</th>';
          echo '        </tr>';
          echo '    </thead>';
          echo '    <tbody>';
                    for($i=0;$i<$song_playlist_length;$i++){
                      $song_data = $song_mgr->getData($song_playlist_id_array[$i]);

          echo '            <tr>';
          echo '                <td>' . ($i+1) . '</td>';
          echo '                <td>' . $song_data['song_id'] . '</td>';
          echo '                <td><a href="song_page.php?song_id=' . $song_data['song_id'] . '">' . $song_data['song_title'] . '</a></td>';
          echo '                <td>' . $song_data['song_writer'] . '</td>';
          echo '                <td>' . $song_data['song_singer'] . '</td>';
          echo '            </tr>';
                   }
          echo '    </tbody>';
          echo '</table>';
          
          } else {
            echo '<h5>Sorry, no data can be found...</h5>';
          }
          ?>
    
          </div><!--/row-->
        </div><!--/.col-xs-12.col-sm-9-->

        <div class="col-xs-6 col-sm-3" id="sidebar">
          <div class="row sidebar-offcanvas">
            <?php facebookPage(); ?>
          </div>
        </div><!--/.sidebar-offcanvas-->
      </div><!--/row-->

    </div><!--/.container-->

    <footer>
      <?php footerTag(); ?>
    </footer>

    <?php footerInclude(); ?>
  </body>
</html>
