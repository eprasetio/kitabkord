<?php
// require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_dbconnect.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_functions.php';

ini_set('display_errors', 'On');
date_default_timezone_set('Asia/Jakarta');

// ---- Playlist Functions ------



// ---- Song Parsing Functions ---------
// convert a parsed song to a text
function getParsedSongToText($myfile, $base_note, $target_note, $song_title, $song_singer){
	
	$text_file = fopen('../doc/song.txt', "w");

	$separator ="\n";

	$index = 0;
	$final = '';

	$myfile_array = explode($separator, $myfile);

	// write song title and singer first in the beginning
	$final .= $song_title . ' - ' . $song_singer . "\n\n";


	do{
		// get the first/chord line and put all word into array
		$chord_line = $myfile_array[$index];
		$chord_array = str_word_count($chord_line, 2, '0123456789#()+:');
		$index = $index+1;

		if(count($chord_array) == 0){ // check if empty line
		    $final .= "\n";
		}else if( count($chord_array) != 0 && strcasecmp(substr($chord_line, 0, 1), ":") == 0 ){ // check if first character the special character ':'
			
			// check if it's an intro line by checking the first word (case insensitive)
			if ( strcasecmp(substr($chord_array[0], 1, 5), 'intro') == 0 ){
				foreach($chord_array as $intro_chord){
				    if( strcasecmp($intro_chord, ':intro:') == 0 ){
				      $final .= substr($intro_chord, 1);
				    }else if(substr($intro_chord, -1) == 'x'){
				      $final .= $intro_chord;
				    }else{
				      $final .= ' ' . getChord($intro_chord, $base_note, $target_note) . ' ';
				    }
				}
			}else{
				$final .= substr($chord_line, 1);
			}
		}else{
			$start_idx = 0;
	  		$end_idx = 0;
	  		$chord = '';
	  		$lyric = '';

	  		// get lyric line
	  		$lyric_line = $myfile_array[$index];
	  		$index = $index+1;

		  	foreach ($chord_array as $key => $value) // iterate through all chords in chord line
		  	{
		  		// parse chords
		  		for($i = $start_idx; $i<$key; $i++)
		  		{
		  			$chord .= " ";
		  		}
		  		$chord .= getChord($value, $base_note, $target_note);

		  		$start_idx = strlen($chord) - 1;
		  	}

		  	$chord .= "\n";
		  	$lyric = $lyric_line;

		  	$final .= $chord . $lyric;

		}

		$final .= "\n";

	}while($index < count($myfile_array));

	// write to a text file and close the connection
	fwrite($text_file, $final);
	fclose($text_file);
}


// transpose a song to the target note
function parseSong($myfile, $base_note, $target_note){
	$separator = "\n";

	$index = 0;
	$final = '';

	$myfile_array = explode($separator, $myfile);

	// parse intro line at the first line of the song text
	if(count($myfile_array) <= 0){
		return 'No lyric has been found...';
	}

	do{
		// get the first/chord line and put all word into array
		$chord_line = $myfile_array[$index];
		$chord_array = str_word_count($chord_line, 2, '0123456789#()+:');
		$index = $index+1;

		if(count($chord_array) == 0){ // check if empty line
			$chord = '';
		    $lyric = '';

		    $final .= '<br>';
		    $final .= createSegment($chord, $lyric, $base_note, $target_note);
		}else if( count($chord_array) != 0 && strcasecmp(substr($chord_line, 0, 1), ":") == 0 ){ // check if first character the special character ':'
			
			// check if it's an intro line by checking the first word (case insensitive)
			if ( strcasecmp(substr($chord_array[0], 1, 5), 'intro') == 0 ){
				foreach($chord_array as $intro_chord){
				    if( strcasecmp($intro_chord, ':intro:') == 0 ){
				      $final .= '<div class="text-line">';
				      $final .= '<div class="text-seg">';
				      $final .= substr($intro_chord, 1);
				      $final .= '</div>';
				    }else if(substr($intro_chord, -1) == 'x'){
				      $final .= $intro_chord;
				      $final .= '</div>';
				    }else{
				      $final .= '<div class="chord-seg">';
				      $final .= ' ' . getChord($intro_chord, $base_note, $target_note) . ' ';
				      $final .= '</div>';
				    }
				}
			}else{
				$final .= '<div class="text-line">';
				$final .= '<div class="text-seg">';
				$final .= substr($chord_line, 1);
				$final .= '</div></div>';
			}
		}else{

			// get the second/text line and put all word into array
			$lyric_line = $myfile_array[$index];
			$lyric_array = str_word_count($lyric_line, 2, '0123456789#()+:');
			$index = $index+1;

			$i = 0;
		
			$start_idx = 0;
			$end_idx = 0;
			$chord = '';
			$lyric = '';
			$prev_value = '';

			$final .= '<div class="text-line">';

		  	foreach ($chord_array as $key => $value) { // iterate through all chords in chord line
		      $end_idx = $key;

		      if($i == 0 && $key > 0){ // check if first chord is not on the first character
		        $chord = '';
		        $lyric = substr($lyric_line, 0, $key );

		        $final .= createSegment($chord, $lyric, $base_note, $target_note);

		      }elseif($i != 0){ // process the chord on the next iteration
		        $chord = $prev_value;
		        $lyric = substr($lyric_line, $start_idx, ($end_idx - $start_idx) );

		        $final .= createSegment($chord, $lyric, $base_note, $target_note);

		      }

		      $prev_value = $value;
		      $start_idx = $key;
		      $i = $i + 1;
		      
		      if( $i == (count($chord_array)) ){ // get the remaining chord and lyric at the very last chord
		        $chord = $value;
		        $lyric = substr($lyric_line, $start_idx);

		        $final .= createSegment($chord, $lyric, $base_note, $target_note);

		      }
		  	
		  	}

		 	$final .= '</div>';
	  	}

	}while($index < count($myfile_array));

	return $final;
}


// create a segment for the song
function createSegment($chord_str, $lyric_str, $base_note, $target_note){
    $segment = '';
    $chord = '';

    if(empty($chord_str) && empty($lyric_str)){
	    $segment .= '<div class="text-seg">';
	    $segment .= '<div class="chord-seg">';
	    $segment .= ' ';
	    $segment .= '</div>';
	    $segment .= '<div class="lyric-seg">';
	    $segment .= ' ';
	    $segment .= '</div>';
	    $segment .= '</div>';
    }else if(!empty($chord_str) && empty($lyric_str)){
    	$chord = getChord($chord_str, $base_note, $target_note);

	    $segment .= '<div class="text-seg">';
	    $segment .= '<div class="chord-seg">';
	    $segment .= $chord;
	    $segment .= '</div>';
	    $segment .= '<div class="lyric-seg">';
	    $segment .= ' ';
	    $segment .= '</div>';
	    $segment .= '</div>';
    }else{
	    $chord = getChord($chord_str, $base_note, $target_note);

	    $segment .= '<div class="text-seg">';
	    $segment .= '<div class="chord-seg">';
	    $segment .= $chord;
	    $segment .= '</div>';
	    $segment .= '<div class="lyric-seg">';
	    $segment .= $lyric_str;
	    $segment .= '</div>';
	    $segment .= '</div>';
	}

    return $segment;
}


// get the new chord based on the base note and the target note
function getChord($chord_string, $base_note, $target_note){
  $chord = '';
  $chord_addon = ''; 
  $start_idx = 0;

  if(strlen($chord_string) > 0){
    $first_letter = substr($chord_string, 0, 1);
    $chord .= $first_letter;

    $second_letter = substr($chord_string, 1, 1);
    if($second_letter == '#'){
      $chord .= $second_letter;
      $start_idx = 1;
    }

    $chord_addon = substr($chord_string, $start_idx+1);

  }

  $chord = assignChord($base_note, $target_note, $chord) . $chord_addon;

  return $chord;
}


// assign a new chord based on the base note and target node
function assignChord($base_note, $target_note, $chord){
  $result = '';
  if($base_note == '' || $target_note == '' || $chord == ''){
    return $result;
  }

  $chord_array = array(
      0 => 'C',
      1 => 'C#',
      2 => 'D',
      3 => 'D#',
      4 => 'E',
      5 => 'F',
      6 => 'F#',
      7 => 'G',
      8 => 'G#',
      9 => 'A',
      10 => 'A#',
      11 => 'B'   
    );

  // calculate distance from target to base note
  $start = array_search($base_note, $chord_array);
  $finish = array_search($target_note, $chord_array);
  $current = array_search($chord, $chord_array);
  $distance = $finish - $start;
  $target_idx = (($current+$distance)%12);
  // pseudo circular buffer indexing
  if($target_idx < 0){
  	$target_idx = 12 + $target_idx;
  }

  $result = $chord_array[$target_idx];

  return $result; 


}

// ---- HTML functions ------- 

function headTag(){
	?>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Off Canvas Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="/kitabkord/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/kitabkord/css/custom.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- <script src="../../assets/js/ie-emulation-modes-warning.js"></script> -->
    <!-- Script for Facebook Page Plugin -->
    <div id="fb-root"></div>
	<script>(function(d, s, id) {
  		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

    <!-- Script for secure login -->
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <script type="text/JavaScript" src="/kitabkord/login/js/sha512.js"></script> 
    <script type="text/JavaScript" src="/kitabkord/login/js/forms.js"></script> 

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php
}

function navBar($mysqli){
	?>
	
	<nav class="navbar-style navbar navbar-fixed-top navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/kitabkord">KitabKord</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="/kitabkord">Home</a></li>
					<li><a href="/kitabkord/listing_page.php?search_song_info=">All Songs</a></li>
					<li><a href="#about">About</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<div class="dropdown navbar-dropdown">
						<div id="dLabel" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<?php
							if (login_check($mysqli) == true) {
								echo '<div class="dropdown-title">Welcome ' . htmlentities($_SESSION['username']) . '</div>';
							}else{
								echo '<div class="dropdown-title">Login/Register</div>';
							}
							?>
							<span class="caret"></span>
						</div>
						<ul class="dropdown-menu" aria-labelledby="dLabel">
							<?php
							if (login_check($mysqli) == true) {
								echo '<li><a href="/kitabkord/admin_form.php">Insert Song</a></li>';
								echo '<li><a href="/kitabkord/playlist_page.php">My Playlist</a></li>';
								echo '<li><a href="/kitabkord/login/includes/logout.php">Log Out</a></li>';
							}else{
								echo '<li><a href="/kitabkord/login/login.php">Log In</a></li>';
								echo '<li><a href="/kitabkord/login/register.php">Register</a></li>';
							}
							?>
						</ul>
					</div>

				</ul>
			</div><!-- /.nav-collapse -->
		</div><!-- /.container -->
	</nav><!-- /.navbar -->

	<?php
}

function facebookPage(){
	?>
	<div class="fb-page" data-href="https://www.facebook.com/facebook" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false" data-show-posts="false">
		<div class="fb-xfbml-parse-ignore">
		  <blockquote cite="https://www.facebook.com/facebook">
		    <a href="https://www.facebook.com/facebook">Facebook</a>
		  </blockquote>
		</div>
	</div>
	<?php
}


function footerTag(){
	?>
		<div class="container footer-container">
			<p>&copy; KitabKord 2015</p>
		</div>
	<?php
}


function footerInclude(){
	?>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/kitabkord/js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->

    <!-- Custom JavaScripts-->
    <script src="/kitabkord/js/customScript.js"></script>
	<?php
}


/*===================  v2 =====================*/
function headTag_v2(){
	?>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Off Canvas Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="/kitabkord/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/kitabkord/css/dashboard.css" rel="stylesheet">
    <link href="/kitabkord/css/custom_v2.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- <script src="../../assets/js/ie-emulation-modes-warning.js"></script> -->
    <!-- Script for Facebook Page Plugin -->
    <div id="fb-root"></div>
	<script>(function(d, s, id) {
  		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

    <!-- Script for secure login -->
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <script type="text/JavaScript" src="/kitabkord/login/js/sha512.js"></script> 
    <script type="text/JavaScript" src="/kitabkord/login/js/forms.js"></script> 

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php
}

function footerTag_v2(){
	?>
		<div class="container footer-container">
			<p>&copy; KitabKord 2015</p>
		</div>
	<?php
}


function footerInclude_v2(){
	?>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/kitabkord/js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->

    <!-- Custom JavaScripts-->
    <script src="/kitabkord/js/customScript.js"></script>
    <script src="/kitabkord/js/content_handler.js"></script>
	<?php
}


function navBar_v2($mysqli){
	?>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">TriNada</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<div class="dropdown navbar-dropdown">
					<div id="dLabel" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						<?php
						if (login_check($mysqli) == true) {
							echo '<div class="dropdown-title">Welcome back ' . htmlentities($_SESSION['username']) . ' !</div>';
						}else{
							echo '<div class="dropdown-title">Login/Register</div>';
						}
						?>
						<span class="caret"></span>
					</div>
					<ul class="dropdown-menu" aria-labelledby="dLabel">
						<?php
						if (login_check($mysqli) == true) {
							echo '<li><a href="/kitabkord/admin_form.php">Insert Song</a></li>';
							echo '<li><a href="/kitabkord/playlist_page.php">My Playlist</a></li>';
							echo '<li><a href="/kitabkord/login/includes/logout.php">Log Out</a></li>';
						}else{
							echo '<li><a href="/kitabkord/login/login.php">Log In</a></li>';
							echo '<li><a href="/kitabkord/login/register.php">Register</a></li>';
						}
						?>
					</ul>
				</div>
			</ul>

          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>

    <?php
}

function sidebar(){
	?>

    <div class="col-sm-3 col-md-2 sidebar">
      <h4 class="sidebar-header">Home</h4>
      <ul class="nav nav-sidebar">
        <li class="active"><a href="#">Browse <span class="sr-only">(current)</span></a></li>
      </ul>
      <h4 class="sidebar-header">Playlist</h4>
      <ul class="nav nav-sidebar">
        <li><a href="">Nav item again</a></li>
        <li><a href="">One more nav</a></li>
        <li><a href="">Another nav item</a></li>
      </ul>
    </div>

    <?php
}

function content_browse(){
	?>


    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <h1 class="page-header">Categories</h1>

      <div class="row placeholders">
        <div class="col-xs-6 col-sm-3 placeholder">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Praise</h4>
          <span class="text-muted">Praise</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Worship</h4>
          <span class="text-muted">Worship</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Christmas</h4>
          <span class="text-muted">Christmas</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Easter</h4>
          <span class="text-muted">Easter</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Latin</h4>
          <span class="text-muted">Latin</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Eucharistic</h4>
          <span class="text-muted">Eucharistic</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Holy Friday</h4>
          <span class="text-muted">Holy Friday</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Other</h4>
          <span class="text-muted">Other</span>
        </div>
      </div>

      <h2 class="sub-header">Top 10 Songs</h2>
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>Song Name</th>
              <th>Artist</th>
              <th>Like</th>
              <th>Header</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1,001</td>
              <td>Lorem</td>
              <td>ipsum</td>
              <td>dolor</td>
              <td>sit</td>
            </tr>
            <tr>
              <td>1,002</td>
              <td>amet</td>
              <td>consectetur</td>
              <td>adipiscing</td>
              <td>elit</td>
            </tr>
            <tr>
              <td>1,003</td>
              <td>Integer</td>
              <td>nec</td>
              <td>odio</td>
              <td>Praesent</td>
            </tr>
            <tr>
              <td>1,003</td>
              <td>libero</td>
              <td>Sed</td>
              <td>cursus</td>
              <td>ante</td>
            </tr>
            <tr>
              <td>1,004</td>
              <td>dapibus</td>
              <td>diam</td>
              <td>Sed</td>
              <td>nisi</td>
            </tr>
            <tr>
              <td>1,005</td>
              <td>Nulla</td>
              <td>quis</td>
              <td>sem</td>
              <td>at</td>
            </tr>
            <tr>
              <td>1,006</td>
              <td>nibh</td>
              <td>elementum</td>
              <td>imperdiet</td>
              <td>Duis</td>
            </tr>
            <tr>
              <td>1,007</td>
              <td>sagittis</td>
              <td>ipsum</td>
              <td>Praesent</td>
              <td>mauris</td>
            </tr>
            <tr>
              <td>1,008</td>
              <td>Fusce</td>
              <td>nec</td>
              <td>tellus</td>
              <td>sed</td>
            </tr>
            <tr>
              <td>1,009</td>
              <td>augue</td>
              <td>semper</td>
              <td>porta</td>
              <td>Mauris</td>
            </tr>
            <tr>
              <td>1,010</td>
              <td>massa</td>
              <td>Vestibulum</td>
              <td>lacinia</td>
              <td>arcu</td>
            </tr>
            <tr>
              <td>1,011</td>
              <td>eget</td>
              <td>nulla</td>
              <td>Class</td>
              <td>aptent</td>
            </tr>
            <tr>
              <td>1,012</td>
              <td>taciti</td>
              <td>sociosqu</td>
              <td>ad</td>
              <td>litora</td>
            </tr>
            <tr>
              <td>1,013</td>
              <td>torquent</td>
              <td>per</td>
              <td>conubia</td>
              <td>nostra</td>
            </tr>
            <tr>
              <td>1,014</td>
              <td>per</td>
              <td>inceptos</td>
              <td>himenaeos</td>
              <td>Curabitur</td>
            </tr>
            <tr>
              <td>1,015</td>
              <td>sodales</td>
              <td>ligula</td>
              <td>in</td>
              <td>libero</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

   	<?php
}

function content_browse2(){
	?>


    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <h1 class="page-header">1231231</h1>

      <div class="row placeholders">
        <div class="col-xs-6 col-sm-3 placeholder">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Praise</h4>
          <span class="text-muted">Praise</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Worship</h4>
          <span class="text-muted">Worship</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Christmas</h4>
          <span class="text-muted">Christmas</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Easter</h4>
          <span class="text-muted">Easter</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Latin</h4>
          <span class="text-muted">Latin</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Eucharistic</h4>
          <span class="text-muted">Eucharistic</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Holy Friday</h4>
          <span class="text-muted">Holy Friday</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="150" height="150" class="img-responsive" alt="Generic placeholder thumbnail">
          <h4>Other</h4>
          <span class="text-muted">Other</span>
        </div>
      </div>
 
    <?php
}

?>