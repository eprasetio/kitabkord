<?php
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/classes/paginator.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/functions.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_dbconnect.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_functions.php';

  sec_session_start();

  $search_song_info = ( isset( $_GET['search_song_info'] ) ) ? $_GET['search_song_info'] : "";

  $extra_param = "";
  $extra_param .= ( isset( $_GET['search_song_info'] ) ) ? ('&search_song_info=' . $search_song_info) : "";

  // $conn       = new mysqli( 'localhost', 'root', 'root', 'kitabkord_db' );
  $conn       = new mysqli( '127.0.0.1', 'root', 'root', 'kitabkord_db' );

  $limit      = ( isset( $_GET['limit'] ) ) ? $_GET['limit'] : 25;
  $page       = ( isset( $_GET['page'] ) ) ? $_GET['page'] : 1;
  $links      = ( isset( $_GET['links'] ) ) ? $_GET['links'] : 7;

if( admin_check($mysqli) == true ){
  $query      = "SELECT song_id, song_title, song_writer, song_singer, 
                song_approval_status, song_username " .
              "FROM song_info_tb " .
              "WHERE (song_title LIKE '%" . $search_song_info . "%' OR " .
                  "song_writer LIKE '%" . $search_song_info . "%' OR " .
                  "song_singer LIKE '%" . $search_song_info . "%') ";
}else{
  $query      = "SELECT song_id, song_title, song_writer, song_singer, 
                song_approval_status, song_username " .
              "FROM song_info_tb " .
              "WHERE (song_title LIKE '%" . $search_song_info . "%' OR " .
                  "song_writer LIKE '%" . $search_song_info . "%' OR " .
                  "song_singer LIKE '%" . $search_song_info . "%') AND (" .
                  "song_approval_status = 'approved') ";
}

  $Paginator  = new paginator( $conn, $query );

  $results    = $Paginator->getData( $limit, $page );

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <?php headTag() ?>;
  </head>

  <body>
    <?php navBar($mysqli); ?>

    <div class="container main-container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>
          <div class="row mainbar-offcanvas">

          <div class="main-search-box">
            <h1>Cari kord lagu favorit mu!</h1>
            <!--  Search Form -->
            <form class="main-search-form form-inline" action="listing_page.php" method="get">
              <div class="form-group">
                <textarea class="main-form-control form-control" id="lyricForm" name="search_song_info" rows="1" placeholder="ie: Judul Lagu, penyanyi, penulis, etc"></textarea>
              </div>
              <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search search-icon" aria-hidden="true"></span></button>
            </form>
          </div>

          <?php 
          if( !empty($results) ){
          echo '<table class="song-listing-table table table-striped table-condensed table-rounded">';
          echo '  <thead>';
          echo '       <tr>';
          echo '            <th width="5%">Song ID</th>';
          echo '            <th width="10%">Song Title</th>';
          echo '            <th width="5%">Song Writer</th>';
          echo '            <th width="5%">Song Singer</th>';
          echo '            <th width="5%">Edit</th>';
          echo '            <th width="5%">Add To List</th>';
          echo '        </tr>';
          echo '    </thead>';
          echo '    <tbody>';
                   for( $i = 0; $i < count( $results->data ); $i++ ):
          echo '            <tr>';
          echo '                <td>' . $results->data[$i]['song_id'] . '</td>';
          echo '                <td><a href="song_page.php?song_id=' . $results->data[$i]['song_id'] . '">' . $results->data[$i]['song_title'] . '</a></td>';
          echo '                <td>' . $results->data[$i]['song_writer'] . '</td>';
          echo '                <td>' . $results->data[$i]['song_singer'] . '</td>';

                            if( login_check($mysqli) ){
                              if( admin_check($mysqli) == true || $results->data[$i]['song_username'] == $_SESSION['username']){
                                echo '<td><a href="admin_update.php?song_id=' . $results->data[$i]['song_id'] . '">Edit</a></td>';
                                echo '<td><div onclick="addSongToList(' . $results->data[$i]['song_id'] . ')">Add</div></td>';
                              }else{
                                echo '<td>No permission</td>';
                                echo '<td>No permission</td>';
                              }
                            }else{
                                echo '<td>No permission</td>';
                                echo '<td>No permission</td>';
                            }


          echo '            </tr>';
                   endfor;
          echo '    </tbody>';
          echo '</table>';
          echo $Paginator->createLinks( $links, 'pagination pagination-sm', $extra_param );
          
          } else {
            echo '<h5>Sorry, no data can be found...</h5>';
          }
          ?>
    
          </div><!--/row-->
        </div><!--/.col-xs-12.col-sm-9-->

        <div class="col-xs-6 col-sm-3" id="sidebar">
          <div class="row sidebar-offcanvas">
            <?php facebookPage(); ?>
          </div>
        </div><!--/.sidebar-offcanvas-->
      </div><!--/row-->

    </div><!--/.container-->

    <footer>
      <?php footerTag(); ?>
    </footer>

    <?php footerInclude(); ?>
  </body>
</html>
