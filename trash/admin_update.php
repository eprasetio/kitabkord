<?php
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/functions.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/classes/song_manager.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_dbconnect.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_functions.php';

sec_session_start();

$song_id = ( isset( $_GET['song_id'] ) ) ? $_GET['song_id'] : 1;

$song_mgr = new song_manager();
$song_data = $song_mgr->getData($song_id);

$song_lyric = $song_data['song_lyric'];
$song_base_note = $song_data['note_value'];
$song_title = $song_data['song_title'];
$song_singer = $song_data['song_singer'];
$song_writer = $song_data['song_writer'];
$song_album = $song_data['song_album'];
$song_thumbs = $song_data['song_thumbs'];
$song_youtube = $song_data['song_youtube'];
$song_itunes = $song_data['song_itunes'];
$song_spotify = $song_data['song_spotify'];
$song_google_play = $song_data['song_google_play'];
$song_username = $song_data['song_username'];
$song_submit_time = $song_data['song_submit_time'];
$song_approval_status = $song_data['song_approval_status'];

$song_tag_id = $song_data['song_tag_id'];
$song_tag_array = $song_mgr->getTag($song_id, $song_tag_id);

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <?php headTag(); ?>
  </head>

  <body>
    <?php navBar($mysqli); ?>

    <div class="container main-container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>
          <div class="row mainbar-offcanvas">
          <?php 
          // check if user has privilage to do edit/update
          if( admin_check($mysqli) == true || $song_username == $_SESSION['username']){
          ?>

            <h3>Song Input Form</h3>

            <!-- Input Form for new lyric -->
            <form method="post" action="includes/form_submit.php">
              <div class="form-group">
                <label for="title">Title:</label>
                <textarea class="form-control" id="title" name="song_title" rows="1" placeholder="Write song title here" required="required"><?= $song_title ?>
                </textarea>
              </div>
              <div class="form-group">
                <label for="singer">Singer(s)/Band:</label>
                <textarea class="form-control" id="singer" name="song_singer" rows="1" placeholder="Write song singer here"><?= $song_singer ?></textarea>
              </div>
              <div class="form-group">
                <label for="writer">Writer(s):</label>
                <textarea class="form-control" id="writer" name="song_writer" rows="1" placeholder="Write song writer here"><?= $song_writer ?></textarea>
              </div>
              <div class="form-group">
                <label for="album">Album:</label>
                <textarea class="form-control" id="album" name="song_album" rows="1" placeholder="Write song album here"><?= $song_album ?></textarea>
              </div>

              <!-- Base Note Option -->
              <label for="note_radio">Choose Base Note for the song:</label>
              <br>
              <label class="radio-inline">
                <input type="radio" <?php if($song_base_note == 'C') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="1" required="required"> C
              </label>
              <label class="radio-inline">
                <input type="radio" <?php if($song_base_note == 'C#') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="2" required="required"> C#
              </label>
              <label class="radio-inline">
                <input type="radio" <?php if($song_base_note == 'D') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="3" required="required"> D
              </label>
              <label class="radio-inline">
                <input type="radio" <?php if($song_base_note == 'D#') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="4" required="required"> D#
              </label>
              <label class="radio-inline">
                <input type="radio" <?php if($song_base_note == 'E') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="5" required="required"> E
              </label>
              <label class="radio-inline">
                <input type="radio" <?php if($song_base_note == 'F') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="6" required="required"> F
              </label>
              <br>
              <label class="radio-inline">
                <input type="radio" <?php if($song_base_note == 'F#') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="7" required="required"> F#
              </label>
              <label class="radio-inline">
                <input type="radio" <?php if($song_base_note == 'G') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="8" required="required"> G
              </label>
              <label class="radio-inline">
                <input type="radio" <?php if($song_base_note == 'G#') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="9" required="required"> G#
              </label>
              <label class="radio-inline">
                <input type="radio" <?php if($song_base_note == 'A') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="10" required="required"> A
              </label>
              <label class="radio-inline">
                <input type="radio" <?php if($song_base_note == 'A#') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="11" required="required"> A#
              </label>
              <label class="radio-inline">
                <input type="radio" <?php if($song_base_note == 'B') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="12" required="required"> B
              </label>
              <br>
              <br>

              <div class="form-group">
                <label for="lyricForm">Lyric with Chord:</label>
                <textarea class="form-control lyric-text-area" id="lyricForm" name ="song_lyric" rows="20" placeholder="Write song lyric + chord here" required="required"><?= $song_lyric ?></textarea>
              </div>

              <!-- Song Tag Option -->
              <label for="tag_checkbox">Choose tag(s) for the song:</label>
              <br>
              <label class="checkbox-inline">
                <input type="checkbox" <?php if(in_array('praise', $song_tag_array)) echo( 'checked'); ?> id="tag_checkbox" name="song_tag_id[]" value="1"> praise
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" <?php if(in_array('worship', $song_tag_array)) echo( 'checked'); ?> id="tag_checkbox" name="song_tag_id[]" value="2"> worship
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" <?php if(in_array('christmas', $song_tag_array)) echo( 'checked'); ?> id="tag_checkbox" name="song_tag_id[]" value="3"> christmas
              </label>
               <label class="checkbox-inline">
                <input type="checkbox" <?php if(in_array('easter', $song_tag_array)) echo( 'checked'); ?> id="tag_checkbox" name="song_tag_id[]" value="4"> easter
              </label>
              <br>
              <label class="checkbox-inline">
                <input type="checkbox" <?php if(in_array('latin', $song_tag_array)) echo( 'checked'); ?> id="tag_checkbox" name="song_tag_id[]" value="5"> latin
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" <?php if(in_array('eucharistic', $song_tag_array)) echo( 'checked'); ?> id="tag_checkbox" name="song_tag_id[]" value="6"> eucharistic
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" <?php if(in_array('holy-friday', $song_tag_array)) echo( 'checked'); ?> id="tag_checkbox" name="song_tag_id[]" value="7"> holy-friday
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" <?php if(in_array('other', $song_tag_array)) echo( 'checked'); ?> id="tag_checkbox" name="song_tag_id[]" value="8"> other
              </label>
              <br>
              <br>

              <div class="form-group">
                <label for="thumbs">Thumbs:</label>
                <textarea class="form-control" id="thumbs" name="song_thumbs" rows="1" placeholder="i.e: 10"><?= $song_thumbs ?></textarea>
              </div>
              <div class="form-group">
                <label for="youtube">Youtube Link:</label>
                <textarea class="form-control" id="youtube" name="song_youtube" rows="1" placeholder="Write youtube link for the song here"><?= $song_youtube ?></textarea>
              </div>
              <div class="form-group">
                <label for="itunes">Itunes Link:</label>
                <textarea class="form-control" id="itunes" name="song_itunes" rows="1" placeholder="Write itunes link for the song here"><?= $song_itunes ?></textarea>
              </div>
              <div class="form-group">
                <label for="spotify">Spotify Link:</label>
                <textarea class="form-control" id="spotify" name="song_spotify" rows="1" placeholder="Write spotify link for the song here"><?= $song_spotify ?></textarea>
              </div>
              <div class="form-group">
                <label for="google_play">Google Play Link:</label>
                <textarea class="form-control" id="google_play" name="song_google_play" rows="1" placeholder="Write google play link for the song here"><?= $song_google_play ?></textarea>
              </div>

              <div class="form-group">
                <label for="song_id">Song ID:</label>
                <textarea class="form-control" id="song_id" name="song_id" value="" rows="1"><?= $song_id ?></textarea>
              </div>
              <div class="form-group">
                <label for="song_username">Submitted by:</label>
                <textarea class="form-control" id="song_username" name="song_username" rows="1"><?= $song_username ?></textarea>
              </div>
              <div class="form-group">
                <label for="song_submit_time">Submit Time:</label>
                <textarea class="form-control" id="song_submit_time" name="song_submit_time" rows="1"><?= $song_submit_time ?></textarea>
              </div>

              <div class="form-group">
                <label class="radio-inline">
                  <input type="radio" <?php if($song_approval_status == 'approved') echo( 'checked'); ?> name="song_approval_status" id="approval_status_radio" value="approved" required="required">Approved
                </label>
                <label class="radio-inline">
                  <input type="radio" <?php if($song_approval_status == 'waiting') echo( 'checked'); ?> name="song_approval_status" id="approval_status_radio" value="waiting" required="required">Waiting
                </label>
              </div>

              <button type="submit" class="btn btn-default" value="click" name="song_form_update">Update</button>
            </form>       
            <form method="post" action="includes/form_submit.php">
              <div class="form-group">
                <textarea class="form-control" style="display: none;" name="song_id" rows="1"><?= $song_id ?></textarea>
              </div>
              <button type="submit" class="btn btn-default" value="click" name="song_form_delete">Delete</button>
            </form> 

          <?php
          // closing bracket for checking if user has privilage to do edit/update
          }else{
            echo '<p>Sorry, you do not have permission to access this page...</p>';
          }
          ?>


          </div><!--/row-->
        </div><!--/.col-xs-12.col-sm-9-->


        <div class="col-xs-6 col-sm-3" id="sidebar">
          <div class="row sidebar-offcanvas">
            <?php facebookPage(); ?>
          </div>
        </div><!--/.sidebar-offcanvas-->
      </div><!--/row-->

    </div><!--/.container-->

    <footer>
      <?php footerTag(); ?>
    </footer>


    <?php footerInclude(); ?>

  </body>
</html>



