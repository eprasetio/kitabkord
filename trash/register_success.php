<?php
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_dbconnect.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/login/includes/login_functions.php';
  require_once $_SERVER["DOCUMENT_ROOT"] . '/kitabkord/includes/functions.php';

  sec_session_start();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php headTag() ?>;
  </head>

  <body>
    <?php navBar($mysqli); ?>

    <div class="container main-container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>
          <div class="row">

            <h1>Registration successful!</h1>
            <p>You can now go back to the <a href="/kitabkord/login/login.php">login page</a> and log in</p>


          </div><!--/row-->
        </div><!--/.col-xs-12.col-sm-9-->

        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">

        </div><!--/.sidebar-offcanvas-->
      </div><!--/row-->

      <hr>

      <footer>
        <?php footerTag(); ?>
      </footer>

    </div><!--/.container-->


    <?php footerInclude(); ?>
  </body>
</html>


