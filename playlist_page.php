<?php
/*
author = eprasetio

Playlist Page
*/
require_once __DIR__ . '/includes.php';

$playlist_id = ( isset( $_GET['playlist_id'] ) ) ? $_GET['playlist_id'] : 1;
$playlist_name = ( isset( $_GET['playlist_name'] ) ) ? $_GET['playlist_name'] : '';
$pl_data = playlist_page_query_data($playlist_id);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <?php headTag(); ?>
  </head>

  <body>

    <?php navBar($mysqli); ?>

    <div class="container-fluid">
      <div class="row">
        <?php sidebar($mysqli); ?>
        <div id="main-container">
          <div class="col-sm-8 col-sm-offset-4 col-md-9 col-md-offset-3 main">
            <?php show_playlist($pl_data, $playlist_id, $playlist_name);?>
          </div>
        </div>
      </div>
    </div>


    <footer>
      <?php footerTag(); ?>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <?php footerInclude(); ?>
  </body>
</html>

<?php
?>