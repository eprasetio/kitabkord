<?php
/*
author = eprasetio

Script to include the necessary's classes and functions
*/

// define variables here
$GLOBALS["ROOT_PATH"] = '';

require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/functions_v2.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/login/includes/login_functions.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/login/includes/login_dbconnect.php';

// sec_session_start();

ini_set('display_errors', 'On');
date_default_timezone_set('Asia/Jakarta');

?>