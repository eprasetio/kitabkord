<?php
/*
author = eprasetio

Search Result Page
*/

require_once __DIR__ . '/includes.php';

$search_keywords = ( isset( $_GET['search_keywords'] ) ) ? $_GET['search_keywords'] : "";

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <?php headTag(); ?>
  </head>

  <body>

    <?php navBar($mysqli); ?>

    <div class="container-fluid">
      <div class="row">
        <?php sidebar($mysqli); ?>
        <div id="DEBUG"></div>
        <div id="main-container">
          <div class="col-sm-8 col-sm-offset-4 col-md-9 col-md-offset-3 main">
            <?php search_page_main($search_keywords); ?>
          </div>
        </div>
      </div>
    </div>


    <footer>
      <?php footerTag(); ?>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <?php footerInclude(); ?>
  </body>
</html>

<?php
?>