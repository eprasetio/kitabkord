<?php
/*
author = eprasetio

My Profile Page
*/

require_once __DIR__ . '/includes.php';
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <?php headTag(); ?>
  </head>

  <body>

    <?php navBar($mysqli); ?>

    <div class="container-fluid">
      <div class="row">
        <?php sidebar($mysqli); ?>
        <div id="DEBUG"></div>
        <div id="main-container">
          <div class="col-sm-8 col-sm-offset-4 col-md-9 col-md-offset-3 main">
          <?php 
          profile_page($mysqli, $_SESSION['username']); 
          ?>
          </div>
        </div>
      </div>
    </div>


    <footer>
      <?php footerTag(); ?>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <?php footerInclude(); ?>
  </body>
</html>

<?php
?>