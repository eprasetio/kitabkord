<?php
/*
author = eprasetio

Song Main Page
*/

require_once __DIR__ . '/includes.php';

$song_id = ( isset( $_GET['song_id'] ) ) ? $_GET['song_id'] : 1;
if (login_check($mysqli) == true) {
  list ($song_thumbs, $song_user_thumbed, $song_data, $song_lyric_default, $song_tags, $song_notes, $song_category) = song_page_query_data($song_id, $_SESSION['username']);
}else{
  list ($song_thumbs, $song_user_thumbed, $song_data, $song_lyric_default, $song_tags, $song_notes, $song_category) = song_page_query_data($song_id);
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <?php headTag(); ?>
  </head>

  <body>

    <?php navBar($mysqli); ?>

    <div class="container-fluid">
      <div class="row">
        <?php sidebar($mysqli); ?>
        <div id="main-container">
          <div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 main">
            <?php song_page_main($song_id, $song_thumbs, $song_data, $song_lyric_default);?>
          </div>
        </div>
        <div id="song-right-ctrl-bar" class="col-sm-3 col-md-3">
            <?= song_page_ctrl_panel($song_id, $mysqli, $song_thumbs, $song_user_thumbed, $song_data, $song_tags, $song_notes, $song_category); ?>
        </div>
            <?= playlist_modal($mysqli, $song_id); ?>
      </div>
    </div>


    <footer>
      <?php footerTag(); ?>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <?php footerInclude(); ?>
  </body>
</html>

<?php
?>