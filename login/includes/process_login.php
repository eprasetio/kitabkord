<?php
/*
author = eprasetio

Script to handle log in
*/

require_once $_SERVER["DOCUMENT_ROOT"] . '/login/includes/login_dbconnect.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/login/includes/login_functions.php';

sec_session_start(); // Our custom secure way of starting a PHP session.
 
if (isset($_POST['email'], $_POST['p'])) {
    $email = $_POST['email'];
    $password = $_POST['p']; // The hashed password.
 
    if (login($email, $password, $mysqli) == true) {
        // Login success 
        header('Location: ' . $GLOBALS["ROOT_PATH"] . '/index.php');
    } else {
        // Login failed 
        echo 'Log In Failed';
    }
} else {
    // The correct POST variables were not sent to this page. 
    echo 'Invalid Request';
}

?>