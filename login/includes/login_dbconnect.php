<?php
/*
author = eprasetio

Script to create mysqli object to access the login database
*/

include_once $_SERVER["DOCUMENT_ROOT"] . '/login/includes/psl-config.php';   // As functions.php is not included
$mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);

?>