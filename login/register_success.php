<?php
/*
author = eprasetio

Register Success Page
*/

require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/functions_v2.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/login/includes/login_functions.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/login/includes/login_dbconnect.php';

sec_session_start();

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <?php headTag(); ?>
  </head>

  <body>

    <?php navBar($mysqli); ?>

    <div class="container-fluid">
      <div class="row">
        <?php sidebar($mysqli); ?>
        <div id="DEBUG"></div>
        <div id="main-container">
          <div class="col-sm-8 col-sm-offset-4 col-md-9 col-md-offset-3 main">
            <!-- Throw register success message here and prompt user to login -->
            <h1>Registration successful!</h1>
            <p>You can now log in with your new credential</p>
            <meta http-equiv="refresh" content="3;url=../index.php" />
          </div>
        </div>
      </div>
    </div>


    <footer>
      <?php footerTag(); ?>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <?php footerInclude(); ?>
  </body>
</html>

<?php
?>