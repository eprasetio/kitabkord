<?php
/*
author = eprasetio

Error page related to login
*/

require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/functions_v2.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/login/includes/login_functions.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/login/includes/login_dbconnect.php';

sec_session_start();


$error = filter_input(INPUT_GET, 'err', $filter = FILTER_SANITIZE_STRING);

if (! $error) {
  $error = 'Oops! An unknown error happened.';
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <?php headTag(); ?>
  </head>

  <body>

    <?php navBar($mysqli); ?>

    <div class="container-fluid">
      <div class="row">
        <?php sidebar($mysqli); ?>
        <div id="DEBUG"></div>
        <div id="main-container">
          <div class="col-sm-8 col-sm-offset-4 col-md-9 col-md-offset-3 main">
            <!-- Throw error message here -->
            <h1>There was a problem</h1>
            <p class="error"><?php echo $error; ?></p>  
          </div>
        </div>
      </div>
    </div>


    <footer>
      <?php footerTag(); ?>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <?php footerInclude(); ?>
  </body>
</html>

<?php
?>