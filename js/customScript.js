/*
author = eprasetio

Custom script for Croaka
*/

/**
** Update profile picture
** ::param:: none
** ::return:: none
**/
function updateProfPic(){
	var btn = document.getElementById('fileToUpload');
  	btn.click();
}

/**
** Convert all songs in the playlist to a text file
** ::param:: playlist_id (int): id of the playlist
** ::return:: none
**/
function getPlaylistToText(playlist_id){
    $.post("./includes/playlist_text_generator.php",
    {
    	playlist_id: playlist_id
    },
    function(data, status){
		if( status == 'success' ) {
			console.log(data);
			var dl = document.getElementById('get-playlist-text-dl-btn');
  			dl.click();
		}else{
			// can throw error msg here
		}
    });
	return null;
}

/**
** Delete a playlist.
** ::param:: playlist_id (int): id of the playlist
** ::param:: playlist_name (str): name of the playlist
** ::param:: username (str): current username
** ::return:: none
**/
function deletePlaylist(playlist_id, playlist_name, username){
    $.post("./includes/playlist_updater.php",
    {
    	action: 'delete_playlist',
    	playlist_id: playlist_id,
    	playlist_name: playlist_name,
    	username: username
    },
    function(data, status){
		if( status == 'success' ) {
			$('#sidebar-playlists').html(data);
			$('#main-container').html('<div class="col-sm-8 col-sm-offset-4 col-md-9 col-md-offset-3 main"><h5>Playlist has been removed</h5></div>');
		}else{
			// can throw error msg here
		}
    });
	return null;
}

/**
** Handle the remove song from a playlist action from playlist modal. 
** ::param:: song_id (int): id of the song
** ::param:: playlist_id (int): id of the playlist
** ::param:: username (str): current username
** ::return:: none
**/
function removeSongFromPlaylistModal(song_id, playlist_id, username){
    $.post("./includes/playlist_updater.php",
    {
    	action: 'delete_song',
    	playlist_id: playlist_id,
    	song_id: song_id,
    	username: username
    },
    function(data, status){
		if( status == 'success' ) {
			$('#playlists-list').html(data);
		}else{
			// can throw error msg here
		}
    });
	return null;
}

/**
** Handle the add song to a playlist action from playlist modal. 
** ::param:: song_id (int): id of the song
** ::param:: playlist_id (int): id of the playlist
** ::param:: username (str): current username
** ::return:: none
**/
function addSongToPlaylistModal(song_id, playlist_id, username){
	var target_note = document.getElementById('current-note').innerHTML;
	var target_note_id = getCurrentChordId(target_note);
    $.post("./includes/playlist_updater.php",
    {
    	action: 'insert_song',
    	playlist_id: playlist_id,
    	song_id: song_id,
    	song_target_note_id: target_note_id,
    	username: username
    },
    function(data, status){
		if( status == 'success' ) {
			$('#playlists-list').html(data);
		}else{
			// can throw error msg here
		}
    });
	return null;
}

/**
** Create playlist modal.
** ::param:: song_id (int): id of the song
** ::param:: username (str): current username
** ::return:: none
**/
function createPlaylistModal(username, song_id){
	var playlist_name = document.getElementById('new_playlist_name').value; // get the playlist name from the field
    $.post("./includes/playlist_updater.php",
    {
    	action: 'create_playlist',
    	playlist_name: playlist_name,
    	username: username
    },
    function(data, status){
		if( status == 'success' ) {
			document.getElementById('new_playlist_name').value = ''; // clear playlist name field
			getPlaylistsModal(username, song_id);
		}else{
			// can throw error msg here
		}
    });
	return false;
}

/**
** Get all the playlist for the current username for playlist modal
** ::param:: username (str): current username
** ::param:: song_id (int): id of the song
** ::return:: none
**/
function getPlaylistsModal(username, song_id){
    $.post("./includes/playlist_updater.php",
    {
    	action: 'get_playlists',
    	username: username,
    	song_id: song_id
    },
    function(data, status){
		if( status == 'success' ) {
			var obj = JSON.parse(data)
			$('#playlists-list').html(obj[0]);
			$('#sidebar-playlists-list').html(obj[1]);
		}else{
			// can throw error msg here
		}
    });
	return null;
}

/**
** Update a song's thumb
** ::param:: username (str): current username
** ::param:: song_id (int): id of the song
** ::return:: none
**/
function updateThumbsCount(username, song_id) {
    $.post("./includes/thumbs_updater.php",
    {
    	username: username,
		song_id: song_id
    },
    function(data, status){
		if( status == 'success' ) {
			var obj = JSON.parse(data);
			$('#user-thumbs-count').html(JSON.stringify(obj['song_thumbs']));

			// update the thumbs color
			if (obj['user_thumbed'] == true){
				document.getElementById("user-thumb").style.color = "#96cf54";
			}else{
				document.getElementById("user-thumb").style.color = "#333";
			}

		}else{
			// can throw error msg here
		}
    });

}

/**
** Show add (TODO)
** ::param:: none
** ::return:: none
**/
function postYourAdd () {
    var iframe = $("#forPostyouradd");
    iframe.attr("src", iframe.data("src")); 
}

/**
** Handle get song in text button press
** ::param:: base_note (str): song base note
** ::param:: song_title (str): song title
** ::param:: song_singer (str): song artist
** ::return:: none
**/
function getSongInText(base_note, song_title, song_singer) {
	var lyric_temp = document.getElementById('song-lyric-temp').innerHTML;
	var target_note = document.getElementById('current-note').innerHTML;
    $.post("./includes/lyric_generator.php",
    {
		lyric_temp: lyric_temp, 
		base_note: base_note, 
		target_note: target_note,
		song_title: song_title,
		song_singer: song_singer
    },
    function(data, status){
		if( status == 'success' ) {
			// press the link to get the text file
			var dl = document.getElementById('get-text-dl-btn');
  			dl.click();
		}else{
			// can throw error msg here
		}
    });
	return false;
}

/**
** Handle the transpose song chord button press
** ::param:: base_note (str): song base note
** ::param:: target_note (str): song target note
** ::return:: none
**/
function updateLyricChord(base_note, target_note) {
	var lyric_temp = document.getElementById('song-lyric-temp').innerHTML;
    $.post("./includes/lyric_updater.php",
    {
		lyric_temp: lyric_temp, 
		base_note: base_note, 
		target_note: target_note
    },
    function(data, status){
		if( status == 'success' ) {
			// while(!data);
			$('#song-lyric-template').html(data);
			$('#song-target-note-template').html(target_note);
		}else{
			$('#song-lyric-template').html('Error' + status);
		}
    });
	return false;
}

/**
** Change/transpose a song's chords
** ::param:: reset (int): reset flag to reset current song chords to base note. (1:true, 0:false)
** ::param:: base_note (str): song base note
** ::param:: direction (int): direction where the user wants to transpose the chord to (up or down)
** ::return:: none
**/
function changeBaseNote(reset, base_note, direction){
	var chordArray = {
		'C':1,
		'C#':2,
		'D':3,
		'D#':4,
		'E':5,
		'F':6,
		'F#':7,
		'G':8,
		'G#':9,
		'A':10,
		'A#':11,
		'B':12
	};

	// init changeBaseNote.currentNoteIdx variable
	if(changeBaseNote.currentNoteIdx == null || reset == true){
		changeBaseNote.currentNoteIdx = chordArray[base_note]; 
	}

	// increment or decrement counter
	var temp = counter(reset, changeBaseNote.currentNoteIdx, direction);
	changeBaseNote.currentNoteIdx = temp;

	// search for key based on value
	for(var key in chordArray){
		if(chordArray[key] == changeBaseNote.currentNoteIdx){
			break;
		}
	}

	// display current note
	$('#current-note').html(key);

	// update lyric chord
	updateLyricChord(base_note, key);
}

/**
** Helper function to act as a circular counter.
** ::param:: reset (int): reset flag to reset the counter (1: true, 0: false). 
** ::param:: base_note_idx (int): index of the base note
** ::param:: direction (int): direction where the user wants to move the counter(up or down)
** ::return:: current counter (int)
**/
function counter(reset, base_note_idx, direction){
	// initialize counter here if it's null
	if(counter.count == null){
		counter.count = base_note_idx;
	}

	// increment counter based on the direction
	if (reset != true){
		if(direction == 1){
			if (counter.count == 12){
				counter.count = 1;
			}else{
				counter.count++;
			}
		}else if(direction == 0){
			if (counter.count == 1){
				counter.count = 12;
			}else{
				counter.count--;
			}
		}
	}else{
		counter.count = base_note_idx;
	}
	return counter.count;
}

/**
** Helper function to get current chord ID in the database
** ::param:: current_chord (str): a string represenataion of the current chord
** ::return:: input chord's database ID (int)
**/
function getCurrentChordId(current_chord){
	var chordArray = {
		'C':1,
		'C#':2,
		'D':3,
		'D#':4,
		'E':5,
		'F':6,
		'F#':7,
		'G':8,
		'G#':9,
		'A':10,
		'A#':11,
		'B':12
	};

	return chordArray[current_chord];
}

