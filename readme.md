# **# CROAKA Project #** # 
## (Do not share the project without eprasetio permission!) ##  
  
### **Folder Hierarchy** ###  
**./** - *{all pages are located in the root directory}*  
**/css** - *{Use custom_v2.css to modify the styling or create a new one, DO NOT use the other css files}*  
**/doc** - *{Put all documents or output files here}*  
**/fonts**   
**/img** - *{Put all images here}*  
**	/profile** *{all users uploaded pictures will go here. DO NOT delete the images!}*   
**/includes** - *{stores all the php classes and functions. Most of the web app functions are in the functions_v2.php}*  
**	/classes**    
**/js** - *{javascript stuff. Use customScript.js or add a new one to add more scripts}*  
**/login** - *{login related stuff. Some functions use login_manager class in ./includes/classes directory}*  
**	/includes  
	/js**  
**/sql** - *{backup SQL}*   
**/trash** - *{trashes, but DO NOT delete. LOL}*