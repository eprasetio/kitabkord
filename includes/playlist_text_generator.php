<?php
/*
author = eprasetio

Php script to generate a text version of a song lyric+chords.
*/

require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/functions_v2.php';

sec_session_start();

$playlist_id = ( isset( $_POST['playlist_id'] ) ) ? $_POST['playlist_id'] : '';

$text_file = fopen('../doc/playlistsongs.txt', "w");
$final = getPlaylistToText($playlist_id, $text_file);
// write to a text file and close the connection
fwrite($text_file, $final);
fclose($text_file);
?>