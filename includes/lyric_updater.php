<?php
/*
author = eprasetio

Php script to update a lyric song by transposing all the chrods to the right base note.
*/

require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/functions_v2.php';

sec_session_start();

$song_lyric_temp = ( isset( $_POST['lyric_temp'] ) ) ? $_POST['lyric_temp'] : '';
$song_base_note = ( isset( $_POST['base_note'] ) ) ? $_POST['base_note'] : 'C';
$target_note = ( isset( $_POST['target_note'] ) ) ? $_POST['target_note'] : 'C';

echo parseSong($song_lyric_temp, $song_base_note, $target_note);
?>