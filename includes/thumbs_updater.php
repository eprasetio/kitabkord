<?php
/*
author = eprasetio

Php script to add/remove a thumb of a song.
*/

require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/classes/song_manager.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/functions_v2.php';

sec_session_start();

$username = ( isset( $_POST['username'] ) ) ? $_POST['username'] : '';
$song_id = ( isset( $_POST['song_id'] ) ) ? $_POST['song_id'] : '';

$song_thumbs = 0;
$operation = '';
$user_thumbed = false;

if( $song_id != '' && $username != ''){
	$song_mgr = new song_manager();

	if ( $song_mgr->checkUserThumb($_SESSION['username'], $song_id) == false){ // check if user has not given a thumb or not
		$song_mgr->updateThumb($username, $song_id, 'add_thumb');
		$operation = 'add_thumb';
		$user_thumbed = true;
	}else{
		$song_mgr->updateThumb($username, $song_id, 'remove_thumb');
		$operation = 'remove_thumb';
		$user_thumbed = false;
	}

	$song_thumbs = $song_mgr->getThumb($song_id);
}

// put return values into an array 
$result_array = array(
					'operation'  => $operation,
					'song_thumbs' => $song_thumbs,
					'user_thumbed' => $user_thumbed
				);

echo json_encode($result_array);
?>