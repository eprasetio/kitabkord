<?php
/*
author = eprasetio

Php script to handle a form submission for adding/deleting/updating a song.
*/

require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/classes/song_manager.php';

// init song manager object
$song_mgr = new song_manager();

// check what action will be performed based on the input value
if(isset($_POST['song_form_submit']))
{
	insert_song_info($song_mgr);
} 
else if(isset($_POST['song_form_update']))
{
	// echo 'Calling update song info....';
	update_song_info($song_mgr);
}
else if(isset($_POST['song_form_delete']))
{
	// echo 'Delete song info....';
	delete_song_info($song_mgr);

}

/**
** Insert song info
** ::param:: song_mgr (song_manager object)
** ::return:: none
**/
function insert_song_info($song_mgr)
{	
	$song_username = ( isset( $_POST['song_username'] ) ) ? $_POST['song_username'] : '';
	$song_approval_status = 'waiting';
	
	if( empty($song_username) == true ){
		echo 'username is not specified';
	}else{
		$song_title = mysql_real_escape_string( preg_replace('/\s{2,}/', ' ', $_POST['song_title'] ) );
		$song_singer = mysql_real_escape_string( preg_replace('/\s{2,}/', ' ', $_POST['song_singer'] ) );
		$song_writer = mysql_real_escape_string( preg_replace('/\s{2,}/', ' ', $_POST['song_writer'] ) );
		$song_album = mysql_real_escape_string( preg_replace('/\s{2,}/', ' ', $_POST['song_album'] ) );
		$song_tag_id = $_POST['song_tag_id'];
		$song_base_note_id = $_POST['song_base_note_id'];
		$song_lyric = mysql_real_escape_string( $_POST['song_lyric'] );
		$song_thumbs = $_POST['song_thumbs'];
		$song_youtube = urlencode( $_POST['song_youtube'] );
		$song_itunes =  urlencode( $_POST['song_itunes'] );
		$song_spotify = urlencode( $_POST['song_spotify'] );
		$song_google_play = urlencode( $_POST['song_google_play'] );

		// echo 'Youtube:' . $song_youtube . '<br>';
		// echo 'Itunes:' . $song_itunes . '<br>';
		// echo 'Spotify:' . $song_spotify . '<br>';
		// echo 'Google Play:' . $song_google_play . '<br>';
		// echo 'Title:' . $song_title . '<br>';
		// echo 'Singer:' . $song_singer . '<br>';
		// echo 'Writer:' . $song_writer . '<br>';
		// echo 'Album:' . $song_album . '<br>';
		
		// echo 'Tag:' . $song_tag_id . '<br>';
		// echo 'Base Note:' . $song_base_note_id . '<br>';
		// echo 'Lyric:' . $song_lyric . '<br>';
		// echo 'Thumbs:' . $song_thumbs . '<br>';
		// echo 'Username: ' . $song_username;

		// insert song data
		$song_mgr->insertData(
				$song_title, 
				$song_singer,
				$song_writer, 
				$song_album, 
				$song_tag_id, 
				$song_base_note_id, 
				$song_lyric, 
				$song_thumbs, 
				$song_youtube, 
				$song_itunes, 
				$song_spotify, 
				$song_google_play,
				$song_approval_status,
				$song_username
			);
	}

	echo "New song is inserted...";
	header('Refresh: 1;url=./../admin_add.php'); // redirect to the admin form page

}

/**
** Update song info
** ::param:: song_mgr (song_manager object)
** ::return:: none
**/
function update_song_info($song_mgr)
{	
	$song_id = mysql_real_escape_string( $_POST['song_id'] );
	$song_title = mysql_real_escape_string( $_POST['song_title'] );
	$song_singer = mysql_real_escape_string( $_POST['song_singer'] );	
	$song_writer = mysql_real_escape_string( $_POST['song_writer'] );
	$song_album = mysql_real_escape_string( $_POST['song_album'] );
	$song_tag_id = implode(", ", $_POST['song_tag_id']);
	$song_base_note_id = $_POST['song_base_note_id'];
	$song_approval_status = mysql_real_escape_string( $_POST['song_approval_status'] );

	$song_lyric = mysql_real_escape_string( $_POST['song_lyric'] );
	$song_thumbs = $_POST['song_thumbs'];
	$song_youtube = urlencode( $_POST['song_youtube'] );
	$song_itunes =  urlencode( $_POST['song_itunes'] );
	$song_spotify = urlencode( $_POST['song_spotify'] );
	$song_google_play = urlencode( $_POST['song_google_play'] );

	// echo 'Youtube:' . $song_youtube . '<br>';
	// echo 'Itunes:' . $song_itunes . '<br>';
	// echo 'Spotify:' . $song_spotify . '<br>';
	// echo 'Google Play:' . $song_google_play . '<br>';

	// echo 'Title:' . $song_title . '<br>';
	// echo 'Singer:' . $song_singer . '<br>';
	// echo 'Writer:' . $song_writer . '<br>';
	// echo 'Album:' . $song_album . '<br>';
	
	// echo 'Tag:' . $song_tag_id . '<br>';
	// echo 'Base Note:' . $song_base_note_id . '<br>';
	// echo 'Lyric:' . $song_lyric . '<br>';
	// echo 'Thumbs:' . $song_thumbs . '<br>';

	// echo 'ID:' . $song_id . '<br>';
	// echo 'Approval Status:' . $song_approval_status . '<br>';

	$song_mgr->updateData(
			$song_title, 
			$song_singer, 			
			$song_writer, 
			$song_album, 
			$song_tag_id, 
			$song_base_note_id, 
			$song_lyric, 
			$song_thumbs, 
			$song_youtube, 
			$song_itunes, 
			$song_spotify, 
			$song_google_play,
			$song_id,
			$song_approval_status
		);

	echo "Updated song";
	header('Refresh: 1;url=./../admin_update.php?song_id='.$song_id); // redirect to previous admin page
}

/**
** Delete a song
** ::param:: song_mgr (song_manager object)
** ::return:: none
**/
function delete_song_info($song_mgr)
{
	$song_id = mysql_real_escape_string( $_POST['song_id'] );
	$song_mgr->deleteData($song_id);

	// echo 'ID:' . $song_id . '<br>';

	header('Refresh: 1;url=/index.php'); // redirect to previous admin page
}
?>
