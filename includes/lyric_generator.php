<?php
/*
author = eprasetio

Php script to parse and transpose a song lyric from a database to 
	a well-formated html segments with the right base note.
*/

require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/functions_v2.php';

sec_session_start();

$song_lyric_temp = ( isset( $_POST['lyric_temp'] ) ) ? $_POST['lyric_temp'] : '';
$song_base_note = ( isset( $_POST['base_note'] ) ) ? $_POST['base_note'] : 'C';
$target_note = ( isset( $_POST['target_note'] ) ) ? $_POST['target_note'] : 'C';
$song_title = ( isset( $_POST['song_title'] ) ) ? $_POST['song_title'] : '';
$song_singer = ( isset( $_POST['song_singer'] ) ) ? $_POST['song_singer'] : '';

$text_file = fopen('../doc/song.txt', "w");
$final = getParsedSongToText($song_lyric_temp, $song_base_note, $target_note, $song_title, $song_singer, $text_file);
// write to a text file and close the connection
fwrite($text_file, $final);
fclose($text_file);

?>