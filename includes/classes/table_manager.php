<?php
/*
author = eprasetio
*/

require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/classes/db_manager.php';

/****
** Class to create tables in the database
*****/
class table_manager extends db_manager{

	/***
	** Constructor
	** ::param:: none
	** ::return:: none
	***/
	public function table_manager(){
		parent::db_manager();
	}

	/***
	** Crease song_info table
	** ::param:: none
	** ::return:: none
	***/
	public function create_song_info_tb(){
		$sql = "CREATE TABLE song_info_tb( ".
			"song_id INT NOT NULL AUTO_INCREMENT, ".
			"song_title VARCHAR(200) NOT NULL, ".
			"song_writer VARCHAR(200), ".
			"song_singer VARCHAR(200) NOT NULL, ".
			"song_album VARCHAR(200), ".
       		"song_tag_id VARCHAR(100) NOT NULL, ".
       		"song_base_note_id INT NOT NULL, ".
       		"song_lyric LONGTEXT NOT NULL, ".
       		"song_thumbs INT, ".   // TODO:: Remove thumbs column
       		"song_youtube VARCHAR(100), ".
       		"song_itunes VARCHAR(100), ".
       		"song_spotify VARCHAR(100), ".
       		"song_google_play VARCHAR(100), ".
       		"song_approval_status VARCHAR(100) NOT NULL, ".
       		"song_username VARCHAR(30) NOT NULL, ".
       		"song_submit_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, ".
       		"PRIMARY KEY ( song_id )); ";
		parent::executeDB($sql);
	}

	/***
	** Create playlists table
	** ::param:: none
	** ::return:: none
	***/
	public function create_playlists_tb(){
		$sql = "CREATE TABLE playlists_tb( ".
			"playlist_id INT NOT NULL AUTO_INCREMENT, ".
			"username VARCHAR(30) NOT NULL, ".
			"playlist_name VARCHAR(50) NOT NULL, ".
			"PRIMARY KEY ( playlist_id )); ";
		parent::executeDB($sql);
	}

	/***
	** Create playlists-songs table
	** ::param:: none
	** ::return:: none
	***/
	public function create_playlists_songs_tb(){
		$sql = "CREATE TABLE playlists_songs_tb( ".
			"playlists_songs_id INT NOT NULL AUTO_INCREMENT, ".
			"playlists_songs_playlist_id INT NOT NULL, ".
			"playlists_songs_song_id INT NOT NULL, ".
			"PRIMARY KEY ( playlists_songs_id )); ";
		parent::executeDB($sql);
	}

	/***
	** Create thumb table
	** ::param:: none
	** ::return:: none
	***/
	public function create_thumb_tb(){
		$sql = "CREATE TABLE thumb_tb( ".
			"thumb_id INT NOT NULL AUTO_INCREMENT, ".
			"username VARCHAR(30) NOT NULL, ".
			"song_id INT NOT NULL, ".
			"PRIMARY KEY ( thumb_id )); ";
		parent::executeDB($sql);
	}

	/***
	** Create tag table
	** ::param:: none
	** ::return:: none
	***/
	public function create_tag_tb(){
		$sql = "CREATE TABLE tag_tb( ".
			"tag_id INT NOT NULL AUTO_INCREMENT, ".
			"tag_value VARCHAR(100) NOT NULL, ".
			"PRIMARY KEY ( tag_id )); ";
		parent::executeDB($sql);
	}

	/***
	** Create song-tag table
	** ::param:: none
	** ::return:: none
	***/
	public function create_song_tag_tb(){
		$sql = "CREATE TABLE song_tag_tb( ".
			"song_id INT NOT NULL, ".
			"tag_id INT NOT NULL, ".
			"PRIMARY KEY ( tag_id )); ";
		parent::executeDB($sql);
	}

	/***
	** Create note table
	** ::param:: none
	** ::return:: none
	***/
	public function create_note_tb(){
		$sql = "CREATE TABLE note_tb( ".
			"note_id INT NOT NULL AUTO_INCREMENT, ".
			"note_value VARCHAR(100) NOT NULL, ".
			"PRIMARY KEY ( note_id )); ";
		parent::executeDB($sql);
	}

	/***
	** Delete song info table
	** ::param:: none
	** ::return:: none
	***/
	public function delete_song_info_tb(){
		$sql = "DROP TABLE song_info_tb;";
		parent::queryDB($sql);
	}

	/***
	** Delete tag table
	** ::param:: none
	** ::return:: none
	***/
	public function delete_tag_tb(){
		$sql = "DROP TABLE tag_tb;";
		parent::executeDB($sql);
	}

	/***
	** Delete playlist table
	** ::param:: none
	** ::return:: none
	***/
	public function delete_playlists_tb(){
		$sql = "DROP TABLE playlists_tb;";
		parent::executeDB($sql);
	}

	/***
	** Delete playlists-songs table
	** ::param:: none
	** ::return:: none
	***/
	public function delete_playlists_songs_tb(){
		$sql = "DROP TABLE playlists_songs_tb;";
		parent::executeDB($sql);
	}

	/***
	** Delete thumb table
	** ::param:: none
	** ::return:: none
	***/
	public function delete_thumb_tb(){
		$sql = "DROP TABLE thumb_tb;";
		parent::executeDB($sql);
	}

	/***
	** Delete song-tag table
	** ::param:: none
	** ::return:: none
	***/
	public function delete_song_tag_tb(){
		$sql = "DROP TABLE song_tag_tb;";
		parent::executeDB($sql);
	}

	/***
	** Delete note table
	** ::param:: none
	** ::return:: none
	***/
	public function delete_note_tb(){
		$sql = "DROP TABLE note_tb;";
		parent::executeDB($sql);
	}

	/***
	** Delete all tables
	** ::param:: none
	** ::return:: none
	***/
	public function delete_all_tables(){
		$this->delete_song_info_tb();
		$this->delete_playlists_tb();
		$this->delete_playlists_songs_tb();
		$this->delete_thumb_tb();
		$this->delete_tag_tb();
		$this->delete_song_tag_tb();
		$this->delete_song_playlist_tb();
		$this->delete_note_tb();
	}

	/***
	** Create all tables
	** ::param:: none
	** ::return:: none
	***/
	public function create_all_tables(){
		$this->create_song_info_tb();
		$this->create_playlists_tb();
		$this->create_playlists_songs_tb();
		$this->create_thumb_tb();
		$this->create_tag_tb();
		$this->create_song_tag_tb();
		$this->create_song_playlist_tb();
		$this->create_note_tb();
	}
}
?>