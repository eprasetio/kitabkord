<?php
/*
author = eprasetio
*/

require_once $_SERVER["DOCUMENT_ROOT"] . '/login/includes/login_dbconnect.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/login/includes/login_functions.php';
sec_session_start();

require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/classes/db_manager.php';

/****
** Class to manage all song data in the database
*****/
class song_manager extends db_manager{

	/***
	** Constructor
	** ::param:: none
	** ::return:: none
	***/
	public function song_manager(){
		parent::db_manager();
	}

	/***
	** Search for songs with the given keywords
	** ::param:: search_keywords (str): the search keywords for finding the songs
	** ::return:: all the songs data that are related with the search_keywords (ass. array)
	***/
	public function search_songs($search_keywords){
		$query = "SELECT DISTINCT 
					song_info_tb.song_id, 
					song_info_tb.song_title, 
					song_info_tb.song_writer, 
					song_info_tb.song_singer, 
		    		song_info_tb.song_approval_status, 
		    		song_info_tb.song_username,
                    category_tb.id,
                    category_tb.name
		    	FROM song_info_tb
				LEFT JOIN category_song_tb
					on (category_song_tb.song_id = song_info_tb.song_id)
				LEFT JOIN category_tb
					on (category_tb.id = category_song_tb.category_id)
 		  		WHERE (
					song_info_tb.song_title LIKE '%" . $search_keywords . "%'  OR
					song_info_tb.song_writer LIKE '%" . $search_keywords ."%' OR
					song_info_tb.song_singer LIKE  '%" . $search_keywords . "%' OR 
					category_tb.name LIKE '%" . $search_keywords . "%' OR
					'%amazin%' LIKE (
							SELECT DISTINCT tag_tb.tag_value 
							FROM tag_tb 
							WHERE tag_tb.tag_id = (
									SELECT DISTINCT tag_song_tb.song_id 
									FROM tag_song_tb
									WHERE tag_song_tb.song_id = song_info_tb.song_id
                                        )
                        )
			    	) 
				AND (song_info_tb.song_approval_status = 'approved')
                ORDER BY song_info_tb.song_title ASC;";
		$rows = parent::queryDB($query);
		$results = $rows->fetchAll(PDO::FETCH_ASSOC);   
		 
		return $results;  
	}

	/***
	** Get song data
	** ::param:: song_id (int): the target song id
	** ::return:: the data of the song (ass. array)
	***/
	public function getData($song_id){
		$query ="SELECT 
				a.song_id,
				a.song_title, 
				a.song_singer,
				a.song_writer,
				a.song_album,
				a.song_lyric,
	       		a.song_thumbs,
	       		a.song_youtube,
	       		a.song_itunes,
	       		a.song_spotify,
	       		a.song_google_play,
				a.song_username,
				a.song_submit_time,
				a.song_approval_status,
				b.note_value " .
			"FROM song_info_tb as a " .
			"LEFT JOIN note_tb as b on a.song_base_note_id = b.note_id " .
			"WHERE a.song_id = " . $song_id . " " .
			"LIMIT 1;";
		$row = parent::queryDB($query);
		$result = $row->fetch(PDO::FETCH_ASSOC);   
		 
		return $result;  
	}

	/***
	** Get song ID given its title
	** ::param:: song_title (str): the target song title
	** ::return:: the id of the song (ass. array)
	***/
	public function getSongId($song_title){
		$query ="SELECT song_id
			FROM song_info_tb
			WHERE song_title = '" . $song_title . "' LIMIT 1;";
		$row = parent::queryDB($query);
		$result = $row->fetch(PDO::FETCH_ASSOC);   
		 
		return $result;  

	}

	/***
	** Get songs data, ordered by the amount of their thumbs. Top is the most thumbed song.
	** ::param:: none
	** ::return:: the song data (ass. array)
	***/
	public function getDataByThumbs(){
		$query ="SELECT 
				song_id,
				song_title, 
				song_singer,
	       		song_thumbs,
				song_username,
				song_submit_time,
				song_approval_status
			FROM song_info_tb
			WHERE song_approval_status = 'approved'
			ORDER BY
				(SELECT COUNT(*) 
				FROM thumb_tb
				WHERE thumb_tb.song_id = song_info_tb.song_id) 
			DESC
			LIMIT 10;";
		$row = parent::queryDB($query);
		$result = $row->fetchAll(PDO::FETCH_ASSOC);   
		 
		return $result;  

	}

	/***
	** Get all songs data, thumbed by a user
	** ::param:: username (str): username that's thumbed the songs
	** ::return:: the songs data (ass. array)
	***/
	public function getThumbedSongs($username){

		$sql = "SELECT 
					song_id,
					song_title, 
					song_singer,
					song_writer,
					song_album,
					song_lyric,
		       		song_thumbs,
		       		song_youtube,
		       		song_itunes,
		       		song_spotify,
		       		song_google_play,
					song_username,
					song_submit_time,
					song_approval_status,
					note_value
				FROM song_info_tb
				LEFT JOIN note_tb on song_base_note_id = note_id
				WHERE song_id IN (
					SELECT song_id FROM thumb_tb WHERE username = '" . $username ."'
				)";
		$rows = parent::queryDB($sql);
		$result = $rows->fetchAll(PDO::FETCH_ASSOC);

		return $result;

	}

	/***
	** Get songs data by a username
	** ::param:: username (str): username that owns the song
	** ::return:: the songs data (ass. array)
	***/
	public function getSongsByUsername($username){
		$sql = "SELECT *
				FROM song_info_tb
				LEFT JOIN note_tb on song_base_note_id = note_id
				WHERE song_username = '" . $username . "' 
				ORDER BY song_title ASC;";

		$rows = parent::queryDB($sql);
		$result = $rows->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

	/***
	** Update the thumb of a song
	** ::param:: username (str): username that's thumbing the song
	** ::param:: song_id (int): id of the song
	** ::param:: operation (int): operate whether user is thumbing or disthumbing the song
	** ::return:: none
	***/
	public function updateThumb($username, $song_id, $operation){
		$sql = '';

		if( $operation == 'add_thumb'){
			$sql = "INSERT INTO thumb_tb (
					username, song_id
				) VALUES (" . 
					"'". $username ."', " . $song_id .
				");";

		}else if( $operation == 'remove_thumb'){
			$sql = "DELETE FROM thumb_tb WHERE (username='" . $username . "' AND song_id=" . $song_id . ");";
		}

		parent::executeDB($sql);
	}

	/***
	** Get total thumbs of a song
	** ::param:: song_id (int): id of the song
	** ::return:: total thumbs (int)
	***/
	public function getThumb($song_id){
		$query = "SELECT count(*) FROM thumb_tb WHERE song_id = " . $song_id . ";";
		$row = parent::queryDB($query);
		$result = $row->fetch(PDO::FETCH_ASSOC);   
		 
		return intval($result['count(*)']);  
	}

	/***
	** Check if a user already thumbed the song
	** ::param:: song_id (int): id of the song
	** ::param:: username (str)
	** ::return:: total thumbs (int)
	***/
	public function checkUserThumb($username, $song_id){
		$query = "SELECT count(*) FROM thumb_tb WHERE (username = '" . $username . "' AND song_id = " . $song_id . ");";
		$row = parent::queryDB($query);
		$result = $row->fetch(PDO::FETCH_ASSOC);   

		// check if result is 0
		if ($result['count(*)'] > 0){
			return true;  
		}else{
			return false;
		}
	}

	/***
	** Get all tags of a song
	** ::param:: song_id (int): id of the song
	** ::return:: tags that belong to the song (ass. array)
	***/
	public function getTag($song_id){
		$query = "SELECT tag_tb.tag_value,
						tag_tb.tag_id 
				FROM tag_tb 
				LEFT JOIN tag_song_tb
					on tag_tb.tag_id = tag_song_tb.tag_id
				WHERE tag_song_tb.song_id = " . $song_id . ";";
		$rows = parent::queryDB($query);
		$results = $rows->fetchAll(PDO::FETCH_ASSOC);   
		 
		return $results;  
	}

	/***
	** Get all tags in the tag table
	** ::param:: none
	** ::return:: all tags in the tag table (ass. array)
	***/
	public function getAllTags(){
		$query = "SELECT * FROM tag_tb;";
		$row = parent::queryDB($query);
		$results = $row->fetchAll(PDO::FETCH_ASSOC);   
		 
		return $results;  
	}

	/***
	** Get all categories for a song
	** ::param:: song id (int): id of the target song
	** ::return:: all categories for the song (ass. array)
	***/
	public function getCategory($song_id){
		$query = "SELECT name 
				FROM category_tb 
				LEFT JOIN category_song_tb
					on category_tb.id = category_song_tb.category_id
				WHERE category_song_tb.song_id = " . $song_id . ";";
		$rows = parent::queryDB($query);
		$results = $rows->fetch(PDO::FETCH_ASSOC);   
		 
		return $results;  
	}

	/***
	** Get all categories from category table
	** ::param:: none
	** ::return:: all categories for the category table (ass. array)
	***/
	public function getAllCategories(){
		$query = "SELECT * FROM category_tb;";
		$row = parent::queryDB($query);
		$results = $row->fetchAll(PDO::FETCH_ASSOC);   
		 
		return $results;  
	}

	/***
	** Get the username that uploaded the song
	** ::param:: song id (int): id of the target song
	** ::return:: uploader username
	***/
	public function getUsername($song_id){
		$query = "SELECT song_username 
				FROM song_info_tb 
				WHERE song_id = " . $song_id . ";";
		$row = parent::queryDB($query);
		$result = $row->fetch(PDO::FETCH_ASSOC);   
		 
		return $result;  
	}

	/***
	** Get all notes data from note table
	** ::param:: none
	** ::return:: all notes data (ass. array)
	***/
	public function getAllNotes(){
		$query = "SELECT note_id, note_value FROM note_tb;";
		$row = parent::queryDB($query);
		$results = $row->fetchAll(PDO::FETCH_ASSOC);   
		 
		return $results;  
	}

	/***
	** Insert a new song data
	** ::param:: song_title (str): title of the song 
	** ::param:: song_signer (str): song artist
	** ::param:: song_writer (str): song writer
	** ::param:: song_album (str): album of the song
	** ::param:: song_tag_id (int): tag id for the song
	** ::param:: song_base_note_id (int): song base note id
	** ::param:: song_lyric (str): lyric of the song, including the chords
	** ::param:: song_thumbs (int): amount of the song thumbs
	** ::param:: song_youtube (str): link to youtube for the song
	** ::param:: song_itunes (str): link to itunes for the song
	** ::param:: song_spotify (str): link to spotify for the song
	** ::param:: song_google_play (str): link to google play for the song
	** ::param:: song_approval_status (str): approval status for the song
	** ::param:: song_username (str): username that uploaded the song
	** ::return:: none
	***/
	public function insertData(
			$song_title, $song_singer, $song_writer, $song_album, $song_tag_id, 
			$song_base_note_id, $song_lyric, $song_thumbs, $song_youtube, 
			$song_itunes, $song_spotify, $song_google_play, $song_approval_status,
			$song_username
		){
		$array = array(
				$song_title, $song_singer, $song_writer, $song_album, 
				$song_base_note_id, $song_lyric, $song_thumbs, $song_youtube, 
				$song_itunes, $song_spotify, $song_google_play, $song_approval_status,
				$song_username
			);

		$value = implode("', '", $array);

		$sql = "INSERT INTO song_info_tb (
				song_title, song_singer, song_writer, song_album,
				song_base_note_id, song_lyric, song_thumbs, song_youtube, 
				song_itunes, song_spotify, song_google_play, song_approval_status,
				song_username
			) VALUES ('" 
				. $value .
			"');";
		$inserted_song_id = parent::executeDB($sql);

		// iterate through all the songs
		foreach($song_tag_id as $tag_id){
			$sql = "INSERT INTO tag_song_tb (
						song_id, tag_id) 
					VALUES (
						" . $inserted_song_id . ", " . $tag_id . ");";
			parent::executeDB($sql); 
		}
	}

	/***
	** UNUSED
	** ::param:: none
	** ::return:: none
	***/
	public function addPlaylist($username, $playlist){
		$value = $username . ', ' .  $playlist;

		$sql = "INSERT INTO song_playlist_tb (
				playlist_username, playlist_list
			) VALUES ('" 
				. $value .
			"');";
		parent::executeDB($sql);
	}

	/***
	** Delete a playlist
	** ::param:: playlist_id (int): id of the target playlist
	** ::return:: none
	***/
	public function deletePlaylist($playlist_id){
		$value = $username . ', ' .  $playlist;

		$sql = "DELETE FROM song_playlist_tb WHERE playlist_id=" . $playlist_id . ";";
		parent::executeDB($sql);
	}

	/***
	** Update a playlist data
	** ::param:: song_title (str): title of the song 
	** ::param:: song_signer (str): song artist
	** ::param:: song_writer (str): song writer
	** ::param:: song_album (str): album of the song
	** ::param:: song_tag_id (int): tag id for the song
	** ::param:: song_base_note_id (int): song base note id
	** ::param:: song_lyric (str): lyric of the song, including the chords
	** ::param:: song_thumbs (int): amount of the song thumbs
	** ::param:: song_youtube (str): link to youtube for the song
	** ::param:: song_itunes (str): link to itunes for the song
	** ::param:: song_spotify (str): link to spotify for the song
	** ::param:: song_google_play (str): link to google play for the song
	** ::param:: song_id (int): id of the target song
	** ::param:: song_approval_status (str): approval status for the song
	** ::return:: none
	***/
	public function updateData(
			$song_title, $song_singer, $song_writer, $song_album, $song_tag_id, 
			$song_base_note_id, $song_lyric, $song_thumbs, $song_youtube, 
			$song_itunes, $song_spotify, $song_google_play, $song_id, $song_approval_status
		){
		$sql = "UPDATE song_info_tb " .
				"SET song_title='" . $song_title . "', " .
					"song_singer='" . $song_singer . "', " .
					"song_writer='" . $song_writer . "', " .
					"song_album='" . $song_album . "', " .
					"song_base_note_id='" . $song_base_note_id . "', " .
					"song_lyric='" . $song_lyric . "', " .
					"song_thumbs='" . $song_thumbs . "', " .
					"song_youtube='" . $song_youtube . "', " .
					"song_itunes='" . $song_itunes . "', " .
					"song_spotify='" . $song_spotify . "', " .
					"song_google_play='" . $song_google_play . "', " .
					"song_approval_status='" . $song_approval_status . "' " .
				"WHERE song_id=" . $song_id . ";";
		parent::executeDB($sql);

		$sql = "DELETE FROM tag_song_tb
				WHERE song_id = " . $song_id . ";";
		parent::executeDB($sql);
		foreach($song_tag_id as $tag_id){
			$sql = "INSERT INTO tag_song_tb (
						song_id, tag_id) 
					VALUES (
						" . $song_id . ", " . $tag_id . ");";
			parent::executeDB($sql); 
		}
	}

	/***
	** Approve a song
	** ::param:: song_id (int): id of the target song
	** ::return:: none
	***/
	public function approveSong($song_id){
		$sql = "UPDATE song_info_tb " .
				"SET song_approval_status='approved'" .
				"WHERE song_id=" . $song_id . ";";
		parent::executeDB($sql);
	}

	/***
	** Disapprove a song
	** ::param:: song_id (int): id of the target song
	** ::return:: none
	***/
	public function disApproveSong($song_id){
		$sql = "UPDATE song_info_tb " .
				"SET song_approval_status='waiting'" .
				"WHERE song_id=" . $song_id . ";";
		parent::executeDB($sql);
	}

	/***
	** Delete a song
	** ::param:: song_id (int): id of the target song
	** ::return:: none
	***/
	public function deleteData($song_id){
		$sql = "DELETE FROM song_info_tb WHERE song_id=" . $song_id . ";";
		parent::executeDB($sql);
	}
}
?>