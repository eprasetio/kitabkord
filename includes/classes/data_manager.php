<?php
/* 
 author = eprasetio
*/

require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/classes/db_manager.php';

/****
** Class to insert croaka data into tables in the database
****/
class data_manager extends db_manager{

	/***
	** Constructor function
	** ::param:: none
	** ::return:: none
	***/
	public function data_manager(){
		parent::db_manager();
	}

	/***
	** Insert notes
	** ::param:: none
	** ::return:: none
	***/
	public function insert_note_data(){
  		$chord_array = array('C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B');
  		$value = implode("'), ('", $chord_array);

		$sql = "INSERT INTO note_tb (
				note_value
			) VALUES ('" 
				. $value .
			"');";		
		parent::executeDB($sql);
	}

	/***
	** Insert tag
	** ::param:: none
	** ::return:: none
	***/
	public function insert_tag_data(){
  		$tag_array = array(
  			'praise', 
  			'worship', 
  			'christmas',
  			'easter',
  			'latin',
  			'eucharistic', 
  			'holy-friday',
  			'other');

  		$value = implode("'), ('", $tag_array);

		$sql = "INSERT INTO tag_tb (
				tag_value
			) VALUES ('" 
				. $value .
			"');";
		parent::executeDB($sql);
	}
}
?>