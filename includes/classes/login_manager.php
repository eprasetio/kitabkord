<?php
/* 
 author = eprasetio
*/

require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/classes/db_manager.php';

/****
** Class to manage user login related data in the database
*****/
class login_manager extends db_manager{

	/***
	** Constructor
	** ::param:: none
	** ::return:: none
	***/
	public function login_manager(){
		parent::db_manager();

		$this->dbname = "croaka_secure_login_db"; // change database
		$this->hostname = "croaka.com";
		$this->username = "admin_croaka";
		$this->password = "Cr0@k@";
	}

	/***
	** Insert profile picture
	** ::param:: username (str), picture file name w/ file extension (str)
	** ::return:: none
	***/
	public function insert_profile_picture_path($username, $file_name){
		$sql = "UPDATE members_tb 
				SET profile_picture='" . $file_name . "' 
				WHERE username = '" . $username . "';";
		$row = parent::executeDB($sql);
	}

	/***
	** Get user profile picture
	** ::param:: username (str)
	** ::return:: picture file name w/ extension(str)
	***/
	public function get_user_profile_picture($username){
		$sql = "SELECT profile_picture
				FROM members_tb
				WHERE username = '" . $username . "'
				LIMIT 1";
		$row = parent::queryDB($sql);
		$result = $row->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	/***
	** Get user public data
	** ::param:: username (str)
	** ::return:: associative array of user data
	***/
	public function get_user_public_data($username){
		$sql = "SELECT 
					id,
					username,
					email,
					register_time,
					profile_picture, 
					user_privilege
				FROM members_tb
				LEFT JOIN members_privilege_tb
					on user_username = username
				WHERE username = '" . $username . "'
				LIMIT 1";
		$row = parent::queryDB($sql);
		$result = $row->fetch(PDO::FETCH_ASSOC);

		return $result;
	}

	/***
	** Get user privilage
	** ::param:: username (str)
	** ::return:: user privilage level (str)
	***/
	public function get_user_privilege($username){
		$sql = "SELECT a.user_privilege FROM members_privilege_tb as a WHERE user_username ='" . $username . "';";
		$row = parent::queryDB($sql);
		$result = $row->fetch(PDO::FETCH_ASSOC);   
		 
		return $result;  
	}

	/***
	** Create croaka member table in the database
	** ::param:: none
	** ::return:: none
	***/
	public function create_members_table(){
		$sql = "CREATE TABLE members_tb( ".
		"id INT NOT NULL AUTO_INCREMENT, ".
		"username VARCHAR(30) NOT NULL, ".
		"email VARCHAR(50) NOT NULL,".
		"password CHAR(128) NOT NULL, ".
		"salt CHAR(128) NOT NULL," .
		"register_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, ".
		"UNIQUE (id, username, email), ".
		"PRIMARY KEY ( id )); ";
		parent::executeDB($sql);
	}

	/***
	** Delete croaka member table in the database
	** ::param:: none
	** ::return:: none
	***/
	public function delete_members_table(){
		$sql = "DROP TABLE members_tb;";
		parent::executeDB($sql);
	}

	/***
	** Create member privilage table in the database
	** ::param:: none
	** ::return:: none
	***/
	public function create_members_privilege_table(){
		$sql = "CREATE TABLE members_privilege_tb( ".
		"user_username VARCHAR(30) NOT NULL, ".
		"user_privilege VARCHAR(100) NOT NULL, ".
		"UNIQUE (user_username)); ";
		parent::executeDB($sql);
	}

	/***
	** Delete member privilage table in the database
	** ::param:: none
	** ::return:: none
	***/
	public function delete_members_privilege_table(){
		$sql = "DROP TABLE members_privilege_tb;";
		parent::executeDB($sql);
	}

	/***
	** Create login attempt table.
	** ::param:: none
	** ::return:: none
	***/
	public function create_login_attempts_table(){
		$sql = "CREATE TABLE login_attempts_tb ( " .
    		"user_id INT(11) NOT NULL, ".
    		"time VARCHAR(30) NOT NULL )";
		parent::executeDB($sql);
	}

	/***
	** Delete login attemp table
	** ::param:: none
	** ::return:: none
	***/
	public function delete_login_attempts_table(){
		$sql = "DROP TABLE login_attempts_tb;";
		parent::executeDB($sql);
	}

	/***
	** Insert test user
	** ::param:: none
	** ::return:: none
	***/
	public function insert_test_usr(){
		$sql = "INSERT INTO members_tb( ".
			"username, email, password, salt) ".
			"VALUES ('test_user', ".
			"'test@example.com', ".
			"'00807432eae173f652f2064bdca1b61b290b52d40e429a7d295d76a71084aa96c0233b82f1feac45529e0726559645acaed6f3ae58a286b9f075916ebf66cacc', ".
			"'f9aab579fc1b41ed0c44fe4ecdbfcdb4cb99b9023abb241a6db833288f4eea3c02f76e0d35204a8695077dcf81932aa59006423976224be0390395bae152d4ef')";
		parent::executeDB($sql);

		$this->set_to_admin('test_user');
	}

	/***
	** Insert test user
	** ::param:: none
	** ::return:: none
	***/
	public function insert_admin_usr(){
		// insert user
		$sql = "INSERT INTO members_tb( ".
			"username, email, password, salt) ".
			"VALUES ('root', ".
			"'root@root.com', ".
			"'b6829a0ba3abe5f07b8b6eed79c095cdc1d201a312724b93e878f0c577236711d7737374708211e0431d2972abc96b0015198c9c5cf168bdfcb13dbd67655208', ".
			"'b58b24c70d0980c37bea830525b2e01cc73f6f4ea572bc252e8261ffabe4f3ae2b22c4c1db3f9349d7292ba13e7f61e8a6b6393f4c07a148b878b00bf8a8fddd')";
		parent::executeDB($sql);

		$this->set_to_admin('root');

		//Admin user: U: root, P: Rootme1

	}

	/***
	** Insert test user
	** ::param:: none
	** ::return:: none
	***/
	public function insert_nonadmin_usr(){
		// insert user
		$sql = "INSERT INTO members_tb( ".
			"username, email, password, salt) ".
			"VALUES ('nonroot', ".
			"'nonroot@nonroot.com', ".
			"'899b60e3ac7eead4aeee4f83e1ccc2ab77d10068be9f93a06da92e6913101a121b416a5d9505e6a128f1e3588cba5ab29905330a5b6e0bf90d000792d21a79ab', ".
			"'8458c6082d37b05931d12e5600d36d4e8c23898e6ccd971f5b153261f2f9c5511206676359f10be5b11c1dc6a9b4ac3ad187fabbead73ecc0d612c7a3b808950')";
		parent::executeDB($sql);

		$this->set_to_nonadmin('root');

		//Non Admin user: U: Nonroot P: Nonrootme1

	}

	/***
	** Insert test user
	** ::param:: none
	** ::return:: none
	***/
	public function set_to_admin($username){
		// set user to admin
		$sql = "INSERT INTO members_privilege_tb( ".
			"user_username, user_privilege) ".
			"VALUES ('" . $username . "', ".
			"'admin')";
		parent::executeDB($sql);
	}

	/***
	** Insert test user
	** ::param:: none
	** ::return:: none
	***/
	public function set_to_nonadmin($username){
		// unset user to admin
		$sql = "INSERT INTO members_privilege_tb( ".
			"user_username, user_privilege) ".
			"VALUES ('" . $username . "', ".
			"'nonadmin')";
		parent::executeDB($sql);
	}

	/***
	** Create all tables
	** ::param:: none
	** ::return:: none
	***/
	public function create_all_tables(){
		$this->create_members_table();
		$this->create_members_privilege_table();
		$this->create_login_attempts_table();
	}

	/***
	** Delete all tables.
	** ::param:: none
	** ::return:: none
	***/
	public function delete_all_tables(){
		$this->delete_members_table();
		$this->delete_login_attempts_table();
		$this->delete_members_privilege_table();
	}
}
?>