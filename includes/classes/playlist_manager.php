<?php
/*
author = eprasetio
*/

require_once $_SERVER["DOCUMENT_ROOT"] . '/login/includes/login_dbconnect.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/login/includes/login_functions.php';
sec_session_start();

require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/classes/db_manager.php';

/****
** Class to manage song playlist in the database
****/
class playlist_manager extends db_manager{

	/***
	** Constructor
	** ::param:: none
	** ::return:: none
	***/
	public function playlist_manager(){
		parent::db_manager();
	}

	/***
	** Create new playlist
	** ::param:: playlist_name (str): name of the new playlist
	** ::param:: username (str): uploader username
	** ::return:: none
	***/
	public function createPlaylist($name, $username){
		$sql = "INSERT INTO playlists_tb (
				username, playlist_name
			) VALUES ('" 
				. $username . "', '" . $name .
			"');";
		parent::executeDB($sql);
	}

	/***
	** Delete playlist
	** ::param:: playlist_id (int): id of target playlist
	** ::return:: none
	***/
	public function deletePlaylist($playlist_id){
		// delete all songs associated with the playlist
		$sql = "DELETE FROM playlists_songs_tb WHERE playlists_songs_playlist_id=" . $playlist_id . ";";
		parent::executeDB($sql);	

		// delete the playlist
		$sql = "DELETE FROM playlists_tb WHERE playlist_id=" . $playlist_id . ";";
		parent::executeDB($sql);
	}

	/***
	** Insert song to a playlist
	** ::param:: song_id (int): id of the song
	** ::param:: song_target_note_id (int): target note
	** ::param:: playlist_id (int): playlist id where the song will be inserted at
	** ::return:: none
	***/
	public function insertSong($song_id, $song_target_note_id, $playlist_id){
		// get the maximum order number in the playlist
		$sql = "SELECT MAX(playlists_songs_song_idx) 
				FROM playlists_songs_tb 
				WHERE playlists_songs_playlist_id = " . $playlist_id . ";";
		$row = parent::queryDB($sql);
		$playlist_song_idx = $row->fetch(PDO::FETCH_ASSOC); 

		// insert the song
		$sql = "INSERT INTO playlists_songs_tb (
				playlists_songs_playlist_id, 
				playlists_songs_song_id, 
				playlists_songs_song_idx,
				playlists_songs_target_note_id
			) VALUES (" 
				. $playlist_id . ", "
				. $song_id . ", " 
				. ($playlist_song_idx['MAX(playlists_songs_song_idx)']+1) . ", "
				. $song_target_note_id .
			");";
		parent::executeDB($sql);
	}

	/***
	** Get the data of a playlist
	** ::param:: playlist_id (int): target playlist id
	** ::return:: associative array of the playlist data
	***/
	public function getPlaylistData($playlist_id){
		$sql = "SELECT 
					playlists_songs_id,
					playlists_songs_playlist_id, 
					playlists_songs_song_id, 
					playlists_songs_song_idx,
					playlists_songs_target_note_id,
					song_title,
					song_singer,
					note_value
			FROM playlists_songs_tb 
			LEFT JOIN song_info_tb 
				on playlists_songs_song_id = song_id
			LEFT JOIN note_tb 
				on playlists_songs_target_note_id = note_id
			WHERE playlists_songs_playlist_id = " . $playlist_id . "
			ORDER BY playlists_songs_song_idx;";
		$row = parent::queryDB($sql);

		// check if the row is null or not first
		if ($row != null){
			$result = $row->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}else{
			return null;
		}
	}

	/***
	** Get all playlists that belong to a username
	** ::param:: username (str): username that owns all the playlists
	** ::return:: associative array of the playlist data
	***/
	public function getPlaylists($username){
		$sql = "SELECT * FROM playlists_tb WHERE username = '" . $username . "';";
		$row = parent::queryDB($sql);
		$result = $row->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}


	/***
	** Delete a song from a playlist
	** ::param:: song_id (int): id of the target song
	** ::param:: playlist_id (int): id of the target playlist
	** ::return:: none
	***/
	public function deleteSong($song_id, $playlist_id){
		$sql = "DELETE FROM playlists_songs_tb WHERE (" .
			" playlists_songs_playlist_id =" . $playlist_id . " AND playlists_songs_song_id=" . $song_id . ");";
		parent::executeDB($sql);
	}


	/***
	** Check if a song is in a playlist
	** ::param:: song_id (int): id of the target song
	** ::param:: playlist_id (int): id of the target playlist
	** ::return:: none
	***/
	public function checkSong($song_id, $playlist_id){

		$sql = "SELECT count(*) FROM playlists_songs_tb WHERE (playlists_songs_song_id = " . $song_id . " AND playlists_songs_playlist_id = " . $playlist_id . ");";
		$row = parent::queryDB($sql);
		$result = $row->fetch(PDO::FETCH_ASSOC);   

		// check if result is 0 or not
		if ($result['count(*)'] > 0){
			return "true";  
		}else{
			return "false";
		}
	}
}
?>