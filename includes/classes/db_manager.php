<?php
/* 
 author = eprasetio
*/

/****
** Base class to manage main SQL database for Croaka
****/
class db_manager{
	public $hostname;
	public $dbname;
	public $username;
	public $password;

	/***
	** Constructor
	** ::param:: none
	** ::return:: none
	***/
	public function db_manager(){
		$this->hostname = "croaka.com";
		$this->dbname = "croaka_db";
		$this->username = "admin_croaka";
		$this->password = "Cr0@k@";
	}

	/***
	** Connect to database
	** ::param:: none
	** ::return:: database's PDO Object
	***/
	public function connectDB(){
		$sql = sprintf("mysql:host=%s;dbname=%s", $this->hostname, $this->dbname);

  		// Connecting to database
  		$db = new PDO ($sql, $this->username, $this->password);
  		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  		return $db;
	}

	/***
	** Disconnect from database
	** ::param:: db (PDO): database's PDO object 
	** ::return:: none
	***/
	public function disconnectDB($db){
		if($db != NULL){
		  $db = NULL;
		}
	}

	/***
	** Execute sql command
	** ::param:: sql (str): sql query (str)
	** ::return:: last id where the data is inserted
	***/
	public function executeDB($sql){
		$last_id = NULL;

		try{
			$db = $this->connectDB(); // connect to database

			// check if connected to DB
			if($db == NULL){
				echo "ERR:: DB is not connected";
			}

			$db->exec($sql); // start executing
			$last_id = $db->lastInsertId(); // get last inserted id
		}
		catch(PDOException $e)
	    {
	    	echo $sql . "<br>" . $e->getMessage() . "<br>";
	    }

	    $this->disconnectDB($db); // disconnect database

		return $last_id;
	}

	/***
	** Query sql command
	** ::param:: sql (str): string sql query
	** ::return:: result of the query (array)
	***/
	public function queryDB($sql){
		$result = NULL;
		$db = NULL;

		try{
			$db = $this->connectDB(); // connect to database

			// check if connected to DB
			if($db == NULL){
				echo "ERR:: DB is not connected";
				return NULL;
			}

			$result = $db->query($sql); // start query
		}
		catch(PDOException $e)
	    {
	    	echo $sql . "<br>" . $e->getMessage() . "<br>";
	    }

		$this->disconnectDB($db); // disconnect database

		return $result;
	}
}
?>