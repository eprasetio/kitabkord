<?php
/*
author = eprasetio

All the main functions for the website are defined here.
*/

require_once $_SERVER["DOCUMENT_ROOT"] . '/login/includes/login_dbconnect.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/login/includes/login_functions.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/classes/login_manager.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/classes/paginator.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/classes/song_manager.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/classes/playlist_manager.php';

sec_session_start();

ini_set('display_errors', 'On');
date_default_timezone_set('Asia/Jakarta');


//==================================
// ---- Page Contents Functions ----
// =================================

/**
** The content for the <head> section in the html website page
** ::param:: none
** ::return:: none
**/
function headTag(){
	?>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Croaka.com</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/dashboard.css" rel="stylesheet">
    <link href="/css/custom_v2.css" rel="stylesheet">

    <!-- Add Google Fonts-->
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- <script src="../../assets/js/ie-emulation-modes-warning.js"></script> -->
    <!-- Script for Facebook Page Plugin -->
    <div id="fb-root"></div>
	<script>(function(d, s, id) {
  		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

    <!-- Script for secure login -->
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <script type="text/JavaScript" src="/login/js/sha512.js"></script> 
    <script type="text/JavaScript" src="/login/js/forms.js"></script> 

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php
}

/**
** The content for the <footer> tag
** ::param:: none
** ::return:: none
**/
function footerTag(){
	?>
		<div class="container footer-container">
			<p>&copy; KitabKord 2015</p>
		</div>
	<?php
}

/**
** The content for the bottom includes in a html page (usually put the <scripts> here).
** ::param:: none
** ::return:: none
**/
function footerInclude(){
	?>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->

    <!-- Custom JavaScripts-->
    <script src="/js/customScript.js"></script>
	<?php
}

/**
** The content for the top navigation bar.
** ::param:: mysqli (mysqli object)
** ::return:: none
**/
function navBar($mysqli){
	?>
    <nav id="main-navbar" class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a id="logo-container" href="/index.php">
			<img id="logo-img" src="/img/logo.gif"></img>
			<span id="logo-title">Croaka</span>
          </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<?php
				if (login_check($mysqli) == true) {
						$login_mgr = new login_manager();
						$user_data = $login_mgr->get_user_public_data($_SESSION['username']);
				?>
				<li id="profile-picture-thum-container-navbar">
					<div id="profile-picture-thumb" style="background-image:url('./img/profiles/<?= $user_data["profile_picture"] ?>');"></div>
				</li>
				<li class="dropdown navbar-dropdown">
					<div id="dLabel" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						<div class="dropdown-title">
							Welcome back, <?= $user_data["username"] ?> !
						</div>
						<span class="caret"></span>
					</div>
					<ul class="dropdown-menu" aria-labelledby="dLabel">
						<li><a href="/admin_add.php">Insert Song</a></li>
						<li><a href="/profile_page.php">My Profile</a></li>
						<li><a href="/login/includes/logout.php">Log Out</a></li>
					</ul>
				</li>
				<?php
				}else{
				?>
					<div id="login-title" data-toggle="modal" data-target=".login-modal">Login or Register</div>
				<?php 
				}
				?>
			</ul>
        </div>
      </div>
    </nav>

	<!-- Modal window for loging in or registering -->
	<div class="modal fade login-modal" tabindex="-1" role="dialog">
	  <div class="modal-dialog modal-sm">
	    <div class="modal-content" id="login-register-container">
	    	<?php login_modal($mysqli); ?>
	    </div>
	  </div>
	</div>

    <?php
}

/**
** The content for login modal window.
** ::param:: mysqli (mysqli object)
** ::return:: none
**/
function login_modal($mysqli){
	?> 
	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#login-tab" role="tab" data-toggle="tab">Log In</a></li>
		<li role="presentation"><a href="#register-tab" role="tab" data-toggle="tab">Register</a></li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="login-tab">
			<form action="/login/includes/process_login.php" method="post" name="login_form">   
				<div class="form-group">                   
					<lable>Email:</label>
					<input type="text" name="email"/>
				</div>
				<div class="form-group"> 
					<lable>Password:</lable>
					<input type="password" name="password" id="password"/> 
				</div>
				<input type="button" value="Login" onclick="formhash(this.form, this.form.password);" />
			</form>
		</div>
		<div role="tabpanel" class="tab-pane" id="register-tab">
			<!-- Registration form to be output if the POST variables are not
			set or if the registration script caused an error. -->
			<ul>
			    <li>Usernames may contain only digits, upper and lower case letters and underscores</li>
			    <li>Emails must have a valid email format</li>
			    <li>Passwords must be at least 6 characters long</li>
			    <li>Passwords must contain
			        <ul>
			            <li>At least one uppercase letter (A..Z)</li>
			            <li>At least one lower case letter (a..z)</li>
			            <li>At least one number (0..9)</li>
			        </ul>
			    </li>
			    <li>Your password and confirmation must match exactly</li>
			</ul>
			<form action="/login/includes/register.inc.php" method="post" name="registration_form">
			    <div class="form-group"> 
			    	<lable>Username:</lable>
			    	<input type='text' name='username' id='username' />
			    </div>
			    <div class="form-group"> 
			    	<lable>Email:</lable>
			    	<input type="text" name="email" id="email" />
			    </div>
			    <div class="form-group"> 
			    	<lable>Password:</lable>
			    	<input type="password"name="password" id="password"/>
			    </div>
			    <div class="form-group"> 
			    	<lable>Confirm password:</lable> 
			    	<input type="password" name="confirmpwd" id="confirmpwd" />
			    </div>
			    <input type="button" value="Register" 
			           onclick="return regformhash(this.form,
			                           this.form.username,
			                           this.form.email,
			                           this.form.password,
			                           this.form.confirmpwd);" /> 
			</form>
		</div>
	</div>

	<?php
}

/**
** The content for the side navigation bar.
** ::param:: mysqli (mysqli object)
** ::return:: none
**/
function sidebar($mysqli){

	$pl_mgr = new playlist_manager();
	
	?>

	<div class="col-sm-3 col-md-3 sidebar">
		<ul class="nav nav-sidebar">
			<li>
				<form class="navbar-form form-inline" action="search.php" method="get">
				  <input class="form-control" rows="1" name="search_keywords" placeholder="Search song here..."></textarea>
				  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search search-icon" aria-hidden="true"></span></button>
				</form>
			</li>
			<li>
				<a href="<?= $GLOBALS["ROOT_PATH"] ?>/index.php"><span class="sidebar-icon glyphicon glyphicon-home" aria-hidden="true"></span>Home</a>
			</li>
		<?php if (login_check($mysqli) == true) { ?>
			<li>
				<a href="<?= $GLOBALS["ROOT_PATH"] ?>/profile_page.php"><span class="sidebar-icon glyphicon glyphicon-user" aria-hidden="true"></span>My Profile</a>
			</li>
			<li>
				<a href="<?= $GLOBALS["ROOT_PATH"] ?>/my_song_page.php"><span class="sidebar-icon glyphicon glyphicon-music" aria-hidden="true"></span>My Songs</a>
			</li>
			<li>
				<a href="<?= $GLOBALS["ROOT_PATH"] ?>/favorite_song_page.php"><span class="sidebar-icon glyphicon glyphicon-headphones" aria-hidden="true"></span>My Favorite Songs</a>
			</li>
			<li>
				<a href="playlist_page_main.php"><span class="sidebar-icon glyphicon glyphicon-folder-open" aria-hidden="true"></span>My Playlists</a>
				<ul id="sidebar-playlists-list" class="nav nav-sidebar">
					<?php 
					// get playlists
					$results = $pl_mgr->getPlaylists($_SESSION['username']);
			
					// iterate through all playlists
					for($i=0;$i<count($results);$i++){
						// check if song is in playlist
						echo '<li><a href="playlist_page.php?playlist_id=' . $results[$i]["playlist_id"] .  '&playlist_name=' . $results[$i]["playlist_name"] .  '">
						<span class="sidebar-icon glyphicon glyphicon-list" aria-hidden="true"></span>' . $results[$i]["playlist_name"] . '</a></li>';
					}
					?>
				</ul>
			</li>
		<?php } ?>
		</ul>
	</div>

	<?php

   	// destroy all objects here
   	unset($pl_mgr);
}

/**
** The content for the category section in the homepage
** ::param:: none
** ::return:: none
**/
function browse_categories(){
	$song_mgr  = new song_manager();
	$results    = $song_mgr->getAllCategories();
	
	?>

	<h1 class="page-header">Categories</h1>
	<div class="row placeholders">

	<?php 
	for ($i=0;$i<count($results);$i++){
		$img_url = $GLOBALS["ROOT_PATH"] . '/img/' . $results[$i]["image"];
		echo '<div class="col-xs-6 col-sm-3 placeholder">';
		echo "<div id='tag-img' style='background-image:url(" . $img_url . ");'>";
		echo '  <a href="/search.php?search_keywords=' . $results[$i]["name"] . '"><p id="tag-value">' . $results[$i]["name"] . '</p></a>';
		echo '</div></div>';
	}

	?>
	</div>

   	<?php
   	// destroy all objects here
   	unset($song_mgr);
}

/**
** The content for the top songs section in the homepage. Top song is based on the # of thumbs.
** ::param:: none
** ::return:: none
**/
function browse_top_songs(){
	$login_mgr = new login_manager();
	$song_mgr  = new song_manager();
	$results    = $song_mgr->getDataByThumbs();

	?>

	<h2 class="sub-header">Top 10 Songs</h2>
	<div class="table-responsive">
	<table class="table table-striped">
	  <thead>
	    <tr>
	      <th>#</th>
	      <th>Song Title</th>
	      <th>Artist</th>
	      <th>Thumbs</th>
	      <th>Category</th>
	      <th>Uploaded By</th>
	    </tr>
	  </thead>
	  <tbody>

	<?php 
	if( !empty($results) ){
       for( $i = 0; $i < count($results); $i++ ):
       	// get total thumbs for the song
       	$total_thumbs = $song_mgr->getThumb($results[$i]['song_id']);
      	$user_data = $login_mgr->get_user_profile_picture($results[$i]['song_username']);
      	$category = $song_mgr->getCategory($results[$i]['song_id']);

		echo '<tr>';
		echo '	<td>' . ($i+1) . '</td>';
		echo '	<td><a href="song_page.php?song_id=' . $results[$i]['song_id'] . '">' . $results[$i]['song_title'] . '</a></td>';
		echo '	<td>' . $results[$i]['song_singer'] . '</td>';
		echo '	<td>' . $total_thumbs . '</td>';
		echo '  <td>' . $category['name'] . '</td>';

		?>
		<td>
			<ul class="user-profile-picture-combo">
				<li id="profile-picture-thum-container">
					<div id="profile-picture-thumb" style="background-image:url('./img/profiles/<?= $user_data["profile_picture"] ?>');"></div>
				</li>
				<li id="username-container">
					<?= $results[$i]['song_username'] ?>
				</li>
			</ul>
		</td>

		<?php
		echo '</tr>';
       endfor;
	  } else {
            echo '<h5>Sorry, no data can be found...</h5>';
      } 

	  ?>
	  </tbody>
	</table>
	</div>
 
    <?php
   	// destroy all objects here
   	unset($song_mgr);
}

/**
** Display the content of search result page.
** ::param:: search_keywords (str): a string of the user prompted search keywords
** ::return:: none
**/
function search_page_main($search_keywords){
	$song_mgr = new song_manager();
	$login_mgr = new login_manager();
	$results = $song_mgr->search_songs($search_keywords);
	?>

	<h2 class="sub-header">Search result for <?= $search_keywords; ?></h2>
	<div class="table-responsive">
		<table class="table table-striped">
		  <thead>
		    <tr>
		      <th>#</th>
		      <th>Song Name</th>
		      <th>Artist</th>
		      <th>Thumbs</th>
		      <th>Category</th>
		      <th>Submitted By</th>
		    </tr>
		  </thead>
		  <tbody>
		<?php 
		if( !empty($results) ){
			for( $i = 0; $i < count($results); $i++ ):
				// get total thumbs for the song
				$total_thumbs = $song_mgr->getThumb($results[$i]['song_id']);
		      	$user_data = $login_mgr->get_user_profile_picture($results[$i]['song_username']);
		      	$category = $song_mgr->getCategory($results[$i]['song_id']);

				echo '<tr>';
				echo '	<td>' . ($i+1) . '</td>';
				echo '	<td><a href="song_page.php?song_id=' . $results[$i]['song_id'] . '">' . $results[$i]['song_title'] . '</a></td>';
				echo '	<td>' . $results[$i]['song_singer'] . '</td>';
				echo '	<td>' . $total_thumbs . '</td>';
				echo '	<td>' . $category['name'] . '</td>';
				?>
				<td>
					<ul class="user-profile-picture-combo">
						<li id="profile-picture-thum-container">
							<div id="profile-picture-thumb" style="background-image:url('./img/profiles/<?= $user_data["profile_picture"] ?>');"></div>
						</li>
						<li id="username-container">
							<?= $results[$i]['song_username']; ?>
						</li>
					</ul>
				</td>
				<?php
				echo '</tr>';
			endfor;
		} else {
		    echo '<h5>Sorry, no data can be found...</h5>';
		} 
		?>
		  </tbody>
		</table>
	</div>
 
    <?php
}

/**
** Display content for my songs page
** ::param:: mysqli (mysqli object)
** ::param:: username (str): current username
** ::return:: none
**/
function my_song_page($mysqli, $username){
	$login_mgr = new login_manager();
	$song_mgr = new song_manager();
	$results = $song_mgr->getSongsByUsername($username); // get user thumbed song
	?>

	<h2 class="sub-header">Managed your uploaded Songs</h2>
	<div class="table-responsive">
		<table class="table table-striped">
	  		<thead>
	    		<tr>
	    			<th>#</th>
	      			<th>Song Title</th>
	      			<th>Artist</th>
	      			<th>Thumbs</th>
	      			<th>Category</th>
	      			<th>Submitted by</th>
	      		    <th>Edit</th>
	    		</tr>
			</thead>
	  		<tbody>
			<?php
			if( !empty($results) ){
		       for( $i = 0; $i < count($results); $i++ ):
		       	// get the additional data here
		       	$total_thumbs = $song_mgr->getThumb($results[$i]['song_id']);
		      	$user_data = $login_mgr->get_user_profile_picture($results[$i]['song_username']);
		      	$category = $song_mgr->getCategory($results[$i]['song_id']);

				echo '<tr>';
				echo '	<td>' . ($i+1) . '</td>';
				echo '	<td><a href="song_page.php?song_id=' . $results[$i]['song_id'] . '">' . $results[$i]['song_title'] . '</a></td>';
				echo '	<td>' . $results[$i]['song_singer'] . '</td>';
				echo '	<td>' . $total_thumbs . '</td>';
				echo '  <td>' . $category['name'] . '</td>';			
				?>
				<td>
					<ul class="user-profile-picture-combo">
						<li id="profile-picture-thum-container">
							<div id="profile-picture-thumb" style="background-image:url('./img/profiles/<?= $user_data["profile_picture"] ?>');"></div>
						</li>
						<li id="username-container">
							<?= $results[$i]['song_username'] ?>
						</li>
					</ul>
				</td>
				<?php
				echo '<td><a href="admin_update.php?song_id=' . $results[$i]['song_id'] . '"><span class="glyphicon glyphicon-pencil"></span></a></td>';
				echo '</tr>';
		       endfor;
			  } else {
		            echo '<h5>Sorry, no data can be found...</h5>';
		      } 
		 	?>
			</tbody>
		</table>
	</div>
	
	<?php
}

/**
** Display content for favorite songs page
** ::param:: mysqli (mysqli object)
** ::param:: username (str): current username
** ::return:: none
**/
function favorite_song_page($mysqli, $username){
	$login_mgr = new login_manager();
	$song_mgr = new song_manager();
	$results = $song_mgr->getThumbedSongs($username); // get user thumbed song
	?>

	<h2 class="sub-header">Your Favorite Songs</h2>
	<div class="table-responsive">
		<table class="table table-striped">
	  		<thead>
	    		<tr>
	    			<th>#</th>
	      			<th>Song Title</th>
	      			<th>Artist</th>
	      			<th>Thumbs</th>
	      			<th>Category</th>
	      			<th>Submitted by</th>
	    		</tr>
			</thead>
	  		<tbody>
			<?php
			if( !empty($results) ){
		       for( $i = 0; $i < count($results); $i++ ):
		       	// get the additional data here
		       	$total_thumbs = $song_mgr->getThumb($results[$i]['song_id']);
		      	$user_data = $login_mgr->get_user_profile_picture($results[$i]['song_username']);
		      	$category = $song_mgr->getCategory($results[$i]['song_id']);

				echo '<tr>';
				echo '	<td>' . ($i+1) . '</td>';
				echo '	<td><a href="song_page.php?song_id=' . $results[$i]['song_id'] . '">' . $results[$i]['song_title'] . '</a></td>';
				echo '	<td>' . $results[$i]['song_singer'] . '</td>';
				echo '	<td>' . $total_thumbs . '</td>';
				echo '  <td>' . $category['name'] . '</td>';
				?>
				<td>
					<ul class="user-profile-picture-combo">
						<li id="profile-picture-thum-container">
							<div id="profile-picture-thumb" style="background-image:url('./img/profiles/<?= $user_data["profile_picture"] ?>');"></div>
						</li>
						<li id="username-container">
							<?= $results[$i]['song_username'] ?>
						</li>
					</ul>
				</td>
				<?php
				echo '</tr>';
		       endfor;
			  } else {
		            echo '<h5>Sorry, no data can be found...</h5>';
		      } 
		 	?>
			</tbody>
		</table>
	</div>
	
	<?php
}

/**
** Display content for playlist page
** ::param:: mysqli (mysqli object)
** ::param:: username (str): current username
** ::return:: none
**/
function playlist_page_main($mysqli, $username){
	$pl_mgr = new playlist_manager();
	$playlists = $pl_mgr->getPlaylists($username);
	?>

	<h2 class="sub-header">Your Playlists</h2>
	<div class="table-responsive">
		<table class="table table-striped">
	  		<thead>
	    		<tr>
	      			<th>Playlist Name</th>
	    		</tr>
			</thead>
	  		<tbody>
			<?php
			// iterate through all playlists
			for($i=0;$i<count($playlists);$i++){
				echo '	<tr>';
				echo '<td><a href="playlist_page.php?playlist_id=' . $playlists[$i]["playlist_id"] .  '&playlist_name=' . $playlists[$i]["playlist_name"] .  '">' . $playlists[$i]["playlist_name"] . '</a></td>';
				echo '  </tr>';
			}
		 	?>
			</tbody>
		</table>
	</div>

	<?php
}

/**
** Display content for profile page
** ::param:: mysqli (mysqli object)
** ::param:: username (str): current username
** ::return:: none
**/
function profile_page($mysqli, $username){
	$login_mgr = new login_manager();
	$user_data = $login_mgr->get_user_public_data($username);

	// check if user has profole picture or not
	if ($user_data['profile_picture'] == null){ 
		$profile_pic = "background-image:url('./img/profiles/blank.jpg');";
	}else{
		$profile_pic = "background-image:url('./img/profiles/" . $user_data["profile_picture"] . "');";
	}

	?>
	<h2 class="sub-header">Profile Info</h2>
	<div class="profile-container container">
		<div class="col-sm-4 col-md-3">
			<div class="profile-picture-container">
				<div id="profile-picture" style="<?= $profile_pic ?>"></div>
				<span id="profile-picture-edit-btn" class="glyphicon glyphicon-camera" onclick="updateProfPic()" aria-hidden="true"></span>
			</div>
			<!-- Hide the form. The button click will be handled by the camera icon -->
			<div style="height:0px;overflow:hidden">
				<form action="./includes/upload_profile_image.php" method="post" enctype="multipart/form-data">
					Select image to upload:
					<input type="hidden" name="user_id" value="<?= $user_data['id'] ?>">
					<input type="hidden" name="username" value="<?= $user_data['username'] ?>">
					<input type="file" name="fileToUpload" id="fileToUpload" onchange="this.form.submit()">
				</form>
			</div>
		</div> 
		<div class="col-sm-4 col-md-6">
			<div class="profile-info-container">
				<div>
					<label>Username: </label>
					<span><?= $user_data["username"] ?></span>
				</div>
				<div>
					<label>Email: </label>
					<span><?= $user_data["email"] ?></span>
				</div>
				<div>
					<label>Date Joined: </label>
					<span><?= $user_data["register_time"] ?></span>
				</div>
				<div>
					<label>User Privilege: </label>
					<span><?= $user_data["user_privilege"] ?></span>
				</div>
			</div>
		</div> 
	</div>

	<?php
}

/**
** Function to query song data in the song page.
** ::param:: song_id (int): id of the song to be displayed
** ::param:: username (str): current username
** ::return:: array of the song data
**/
function song_page_query_data($song_id, $username=null){
	$song_mgr = new song_manager();

	$song_thumbs = $song_mgr->getThumb($song_id);
	if ($username != null) {
		$song_user_thumbed = $song_mgr->checkUserThumb($username, $song_id);
	}else{
		$song_user_thumbed = false;
	}
	$song_data = $song_mgr->getData($song_id);
	// $song_lyric_default = getParsedSongToHtml($song_data['song_lyric'], $song_data['note_value'], $song_data['note_value'], $song_data['song_title'], $song_data['song_singer']);
	$song_lyric_default = parseSong($song_data['song_lyric'], $song_data['note_value'], $song_data['note_value']);
	$song_tags = $song_mgr->getTag($song_id);
	$song_notes = $song_mgr->getAllNotes();
	$song_category = $song_mgr->getCategory($song_id);

    return array($song_thumbs, $song_user_thumbed, $song_data, $song_lyric_default, $song_tags, $song_notes, $song_category);
}

/**
** The content for the song main page.
** ::param:: song_id (int): id of the song to be displayed
** ::param:: song_thumbs (int): number of thumbs that the song has
** ::param:: song_data (ass. array): an array of the song data
** ::param:: song_lyric_defaule (str): lyric of the song (unparsed)
** ::return:: none
**/
function song_page_main($song_id, $song_thumbs, $song_data, $song_lyric_default){

	echo '<h1 class="page-header">' . $song_data['song_title'] . ' - ' . $song_data['song_singer'] . '</h1>';

    ?>
	
	<div id='song-lyric-temp'><?= $song_data['song_lyric']; ?></div>
	<div id='song-lyric-template'><?= $song_lyric_default;?></div>

	<?php
}

/**
** The content for the song page's control panel.
** ::param:: song_id (int): id of the song to be displayed
** ::param:: mysqli (mysqli object) for login
** ::param:: song_thumbs (int): number of thumbs that the song has
** ::param:: song_user_thumbed (int): status if the current user already thumbed the song or not
** ::param:: song_data (ass. array): an array of the song data
** ::param:: song_tags (array): tags for the song
** ::param:: song_notes (str): the base note for the song
** ::param:: song_category (str): the category for the song
** ::return:: none
**/
function song_page_ctrl_panel($song_id, $mysqli, $song_thumbs, $song_user_thumbed, $song_data, $song_tags, $song_notes, $song_category){
	$login_mgr = new login_manager();
	$user_data = $login_mgr->get_user_profile_picture($song_data['song_username']);

	echo '<ul id="song-ctrl-panel-list">';

    echo '<li>';
    	echo '<iframe class="youtube-video song-ctrl-list" src="https://www.youtube.com/embed/' . $song_data['song_youtube'] . '" frameborder="0" allowfullscreen></iframe>';
    echo '</li>';
	if(!empty($song_data['song_title'])){
		echo '<li class="song-ctrl-list"><label>Title: </label>' . $song_data['song_title'] . '</li>';
	}
	if(!empty($song_data['song_singer'])){
		echo '<li class="song-ctrl-list"><label>Artist(s): </label>' . $song_data['song_singer'] . '</li>';
	}
	if(!empty($song_data['song_writer'])){
		echo '<li class="song-ctrl-list"><label>Writer: </label>' . $song_data['song_writer'] . '</li>';
	}
	if(!empty($song_data['song_album'])){
		echo '<li class="song-ctrl-list"><label>Album: </label>' . $$song_data['song_album'] . '</li>';
	}
	if(!empty($song_data['song_itunes'])){
		echo '<li class="song-ctrl-list"><label>Itunes: </label>' . $song_data['song_itunes'] . '</li>';
	}
	if(!empty($song_data['song_spotify'])){
		echo '<li class="song-ctrl-list"><label>Spotify: </label>' . $song_data['song_spotify'] . '</li>';
	}
	?>
	<li class="song-ctrl-list"><label>Category: </label><?= $song_category['name']; ?></li>
	<!-- song uploader -->
	<li class="song-ctrl-list">
		<label>Uploader: </label>
		<ul class="user-profile-picture-combo">
			<li id="profile-picture-thum-container">
				<div id="profile-picture-thumb" style="background-image:url('./img/profiles/<?= $user_data["profile_picture"] ?>');"></div>
			</li>
			<li id="username-container">
				<?= $song_data['song_username'] ?>
			</li>
		</ul>
	</li>
	<?php
	echo '<li class="song-ctrl-list"><label>Upload Date: </label>' . $song_data['song_submit_time'] . '</li>';

	if( !empty($song_notes) ){
       for( $i = 0; $i < count($song_notes); $i++ ):
       // TODO

       endfor;
	}

	?>
    <li class="song-ctrl-list song-note-ctrl">
		<span onclick="changeBaseNote(false, '<?= $song_data["note_value"]; ?>', 0)" class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span id="current-note"><?= $song_data["note_value"]; ?></span>
		<span onclick="changeBaseNote(false, '<?= $song_data["note_value"]; ?>', 1)" class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    </li>

 	<li class="song-ctrl-list" onclick="changeBaseNote(true, '<?= $song_data["note_value"]; ?>', 1)">
    	<b class="song-ctrl-btn">Reset Base Note</b>
	</li>

	<li class="song-ctrl-list thumb-ctrl">
		<?php
		if (login_check($mysqli) == true) {
			if($song_user_thumbed == true){
				?>
				<span id="user-thumb" onclick="updateThumbsCount('<?= $_SESSION['username'] ?>', <?= $song_id ?>)" class="glyphicon glyphicon-thumbs-up" style="color:#78a543;" aria-hidden="true"></span>
				<?php
			}else{
				?>
				<span id="user-thumb" onclick="updateThumbsCount('<?= $_SESSION['username'] ?>', <?= $song_id ?>)" class="glyphicon glyphicon-thumbs-up" style="color:#333;" aria-hidden="true"></span>
				<?php
			}
		}else{
			?>
			<span id="user-thumb" class="glyphicon glyphicon-thumbs-up" style="color:#333;" aria-hidden="true"></span>
			<?php	
		}
		?>
		<span id="user-thumbs-count"> <?= $song_thumbs ?></span>
	</li>
    
    <!-- get song text + chord -->
    <div id="song-target-note-template" style="display:none;"><?= $song_data['note_value'] ?></div>
    <li class="song-ctrl-list" onclick="getSongInText('<?=$song_data['note_value']?>', '<?=$song_data['song_title']?>', '<?=$song_data['song_singer']?>')">
    	<b class="song-ctrl-btn">Get Text</b>
    </li>
    <a id="get-text-dl-btn" style="display:hidden;" href="<?= $GLOBALS["ROOT_PATH"] ?>/doc/song.txt" target="_blank"></a>

	<!-- Small modal for adding playlist -->
	<li class="song-ctrl-list" onclick="getPlaylistsModal('<?= $_SESSION['username'] ?>', <?= $song_id ?>)" data-toggle="modal" data-target=".bs-example-modal-sm">
    	<b class="song-ctrl-btn">Add Song To Playlist</b>
	</li>



	<?php
	if(!empty($song_tags)){
		echo '<li class="song-ctrl-list"><ul class="list-inline">';
		foreach($song_tags as $tag){
			echo '<li>' . $tag["tag_value"] . '</li>';
		}
		echo '</ul></li>';
	}

	echo '</ul>';

}

/**
** The content for the manage playlist modal in the song page.
** ::param:: mysqli (mysqli object) for login
** ::param:: song_id (int): id of the song to be displayed
** ::return:: none
**/
function playlist_modal($mysqli, $song_id){
	?>
	<div class="playlist-modal modal bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	  <div class="playlist-modal-dialog modal-dialog modal-sm">
	    <div class="modal-content playlists-container">
			<button class="close" data-dismiss="modal" data-target=".bs-example-modal-sm">x</button>
	    	<?php
	      if (login_check($mysqli) == true) {
			?>
			<!-- Get playlists -->
			<div id="playlists-list"></div>
			<hr>
			<!-- Display create playlist form -->
			<div class="form-group">
				<label for="new_playlist_name">Create new playlist:</label>
				<textarea class="form-control" id="new_playlist_name"  rows="1" placeholder="write playlist name here" required="required"></textarea>
			</div>
			<button onclick="createPlaylistModal('<?= $_SESSION['username'] ?>', <?= $song_id ?>)">Create</button>
			<?php 
	    }else{
	    ?>
	    	<p>Please log in to manage your playlist</p>
	    <?php
	    }
	    ?>
	    </div>
	  </div>
	</div>

	<?php
}

/**
** Function to query playlist data in the playlist page.
** ::param:: playlist_id (int): id of the playlist
** ::return:: none
**/
function playlist_page_query_data($playlist_id){
	$pl_mgr = new playlist_manager();
    return $pl_mgr->getPlaylistData($playlist_id);
}

/**
** Display the content of the playlist data in the playlist page.
** ::param:: pl_data (ass. array): the playlist data
** ::param:: playlist_id (int): id of the playlist
** ::param:: playlist_name (str): name of the
** ::return:: none
**/
function show_playlist($pl_data, $playlist_id, $playlist_name){
	$song_mgr = new song_manager();
	$login_mgr = new login_manager();
    ?>

	<h2 class="sub-header"><?= $playlist_name ?></h2>
	<div class="table-responsive">
	<table class="table table-striped">
	  <thead>
	    <tr>
	      <th>#</th>
	      <th>Song Title</th>
	      <th>Artist</th>
	      <th>Category</th>
	      <th>Submitted By</th>
	      <th>Base Note</th>
	    </tr>
	  </thead>
	  <tbody>
	
	<?php 
	if( !empty($pl_data) ){
       for( $i = 0; $i < count($pl_data); $i++ ):
       	$song_username = $song_mgr->getUsername($pl_data[$i]['playlists_songs_song_id']);
      	$user_data = $login_mgr->get_user_profile_picture($song_username["song_username"]);
		$song_category = $song_mgr->getCategory($pl_data[$i]['playlists_songs_song_id']);

		echo '<tr>';
		echo '	<td>' . $pl_data[$i]['playlists_songs_song_idx'] . '</td>';
		echo '	<td><a href="song_page.php?song_id=' . $pl_data[$i]['playlists_songs_song_id'] . '">' . $pl_data[$i]['song_title'] . '</a></td>';
		echo '	<td>' . $pl_data[$i]['song_singer'] . '</td>';
		echo '  <td>' . $song_category['name'] . '</td>';
		?>
		<td>
			<ul class="user-profile-picture-combo">
				<li id="profile-picture-thum-container">
					<div id="profile-picture-thumb" style="background-image:url('./img/profiles/<?= $user_data["profile_picture"] ?>');"></div>
				</li>
				<li id="username-container">
					<?= $song_username['song_username']; ?>
				</li>
			</ul>
		</td>
		<?php
		echo '	<td>' . $pl_data[$i]['note_value'] . '</td>';
		echo '</tr>';
       endfor;
	} else {
		echo '<tr>';
		echo '<td>-</td>';
	    echo '<td>Playlist is empty</td>';
		echo '<td>-</td>';
		echo '</tr>';
	} 
	?>

	  </tbody>
	</table>
	</div>

	<button id="playlist-get-text-btn" onclick="getPlaylistToText(<?= $playlist_id ?>)">Get Text</button>
	<a id="get-playlist-text-dl-btn" style="display:hidden;" href="<?= $GLOBALS["ROOT_PATH"] ?>/doc/playlistsongs.txt" target="_blank"></a>
	<button id="playlist-delete-btn" onclick="deletePlaylist(<?= $playlist_id ?>, '<?= $playlist_name ?>', '<?= $_SESSION['username'] ?>')">Delete</button>

	<?php
}

/**
** Display the content of the admin's update song page.
** ::param:: mysqli (mysqli object) for login
** ::param:: song_id (int): id of the song
** ::param:: username (str): current username
** ::return:: none
**/
function admin_page_update($mysqli, $song_id, $username){
	$song_mgr = new song_manager();
	$song_data = $song_mgr->getData($song_id);
	$song_tags = $song_mgr->getTag($song_id);

	// check if user has privilage to do edit/update
	if( admin_check($mysqli) == true || $song_data['song_username'] == $username){
	?>

		<h2 class="sub-header">Song Input Form</h2>

		<!-- Input Form for new lyric -->
		<form method="post" action="includes/form_submit.php">
		  <div class="form-group">
		    <label for="title">Title:</label>
		    <textarea class="form-control" id="title" name="song_title" rows="1" placeholder="Write song title here" required="required"><?= $song_data['song_title']; ?>
		    </textarea>
		  </div>
		  <div class="form-group">
		    <label for="singer">Singer(s)/Band:</label>
		    <textarea class="form-control" id="singer" name="song_singer" rows="1" placeholder="Write song singer here"><?= $song_data['song_singer']; ?></textarea>
		  </div>
		  <div class="form-group">
		    <label for="writer">Writer(s):</label>
		    <textarea class="form-control" id="writer" name="song_writer" rows="1" placeholder="Write song writer here"><?= $song_data['song_writer']; ?></textarea>
		  </div>
		  <div class="form-group">
		    <label for="album">Album:</label>
		    <textarea class="form-control" id="album" name="song_album" rows="1" placeholder="Write song album here"><?= $song_data['song_album']; ?></textarea>
		  </div>

		  <!-- Base Note Option -->
		  <label for="note_radio">Choose Base Note for the song:</label>
		  <br>
		  <label class="radio-inline">
		    <input type="radio" <?php if($song_data['note_value'] == 'C') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="1" required="required"> C
		  </label>
		  <label class="radio-inline">
		    <input type="radio" <?php if($song_data['note_value'] == 'C#') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="2" required="required"> C#
		  </label>
		  <label class="radio-inline">
		    <input type="radio" <?php if($song_data['note_value'] == 'D') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="3" required="required"> D
		  </label>
		  <label class="radio-inline">
		    <input type="radio" <?php if($song_data['note_value'] == 'D#') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="4" required="required"> D#
		  </label>
		  <label class="radio-inline">
		    <input type="radio" <?php if($song_data['note_value'] == 'E') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="5" required="required"> E
		  </label>
		  <label class="radio-inline">
		    <input type="radio" <?php if($song_data['note_value'] == 'F') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="6" required="required"> F
		  </label>
		  <br>
		  <label class="radio-inline">
		    <input type="radio" <?php if($song_data['note_value'] == 'F#') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="7" required="required"> F#
		  </label>
		  <label class="radio-inline">
		    <input type="radio" <?php if($song_data['note_value'] == 'G') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="8" required="required"> G
		  </label>
		  <label class="radio-inline">
		    <input type="radio" <?php if($song_data['note_value'] == 'G#') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="9" required="required"> G#
		  </label>
		  <label class="radio-inline">
		    <input type="radio" <?php if($song_data['note_value'] == 'A') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="10" required="required"> A
		  </label>
		  <label class="radio-inline">
		    <input type="radio" <?php if($song_data['note_value'] == 'A#') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="11" required="required"> A#
		  </label>
		  <label class="radio-inline">
		    <input type="radio" <?php if($song_data['note_value'] == 'B') echo( 'checked'); ?> name="song_base_note_id" id="note_radio" value="12" required="required"> B
		  </label>
		  <br>
		  <br>

		  <div class="form-group">
		    <label for="lyricForm">Lyric with Chord:</label>
		    <textarea class="form-control lyric-text-area" id="lyricForm" name ="song_lyric" rows="20" placeholder="Write song lyric + chord here" required="required"><?= $song_data['song_lyric']; ?></textarea>
		  </div>

		  <!-- Song Tag Option -->
		  <label for="tag_checkbox">Choose tag(s) for the song:</label>
		  <br>
		  <label class="checkbox-inline">
		    <input type="checkbox" <?php if(in_array('praise', $song_tags)) echo( 'checked'); ?> id="tag_checkbox" name="song_tag_id[]" value="1"> praise
		  </label>
		  <label class="checkbox-inline">
		    <input type="checkbox" <?php if(in_array('worship', $song_tags)) echo( 'checked'); ?> id="tag_checkbox" name="song_tag_id[]" value="2"> worship
		  </label>
		  <label class="checkbox-inline">
		    <input type="checkbox" <?php if(in_array('christmas', $song_tags)) echo( 'checked'); ?> id="tag_checkbox" name="song_tag_id[]" value="3"> christmas
		  </label>
		   <label class="checkbox-inline">
		    <input type="checkbox" <?php if(in_array('easter', $song_tags)) echo( 'checked'); ?> id="tag_checkbox" name="song_tag_id[]" value="4"> easter
		  </label>
		  <br>
		  <label class="checkbox-inline">
		    <input type="checkbox" <?php if(in_array('latin', $song_tags)) echo( 'checked'); ?> id="tag_checkbox" name="song_tag_id[]" value="5"> latin
		  </label>
		  <label class="checkbox-inline">
		    <input type="checkbox" <?php if(in_array('eucharistic', $song_tags)) echo( 'checked'); ?> id="tag_checkbox" name="song_tag_id[]" value="6"> eucharistic
		  </label>
		  <label class="checkbox-inline">
		    <input type="checkbox" <?php if(in_array('holy-friday', $song_tags)) echo( 'checked'); ?> id="tag_checkbox" name="song_tag_id[]" value="7"> holy-friday
		  </label>
		  <label class="checkbox-inline">
		    <input type="checkbox" <?php if(in_array('other', $song_tags)) echo( 'checked'); ?> id="tag_checkbox" name="song_tag_id[]" value="8"> other
		  </label>
		  <br>
		  <br>

		  <div class="form-group">
		    <label for="thumbs">Thumbs:</label>
		    <textarea class="form-control" id="thumbs" name="song_thumbs" rows="1" placeholder="i.e: 10"><?= $song_data['song_thumbs']; ?></textarea>
		  </div>
		  <div class="form-group">
		    <label for="youtube">Youtube Link:</label>
		    <textarea class="form-control" id="youtube" name="song_youtube" rows="1" placeholder="Write youtube link for the song here"><?= $song_data['song_youtube']; ?></textarea>
		  </div>
		  <div class="form-group">
		    <label for="itunes">Itunes Link:</label>
		    <textarea class="form-control" id="itunes" name="song_itunes" rows="1" placeholder="Write itunes link for the song here"><?= $song_data['song_itunes']; ?></textarea>
		  </div>
		  <div class="form-group">
		    <label for="spotify">Spotify Link:</label>
		    <textarea class="form-control" id="spotify" name="song_spotify" rows="1" placeholder="Write spotify link for the song here"><?= $song_data['song_spotify']; ?></textarea>
		  </div>
		  <div class="form-group">
		    <label for="google_play">Google Play Link:</label>
		    <textarea class="form-control" id="google_play" name="song_google_play" rows="1" placeholder="Write google play link for the song here"><?= $song_data['song_google_play']; ?></textarea>
		  </div>

		  <div class="form-group">
		    <label for="song_id">Song ID:</label>
		    <textarea class="form-control" id="song_id" name="song_id" value="" rows="1"><?= $song_id ?></textarea>
		  </div>
		  <div class="form-group">
		    <label for="song_username">Submitted by:</label>
		    <textarea class="form-control" id="song_username" name="song_username" rows="1"><?= $song_data['song_username']; ?></textarea>
		  </div>
		  <div class="form-group">
		    <label for="song_submit_time">Submit Time:</label>
		    <textarea class="form-control" id="song_submit_time" name="song_submit_time" rows="1"><?= $song_data['song_submit_time']; ?></textarea>
		  </div>

		  <div class="form-group">
		    <label class="radio-inline">
		      <input type="radio" <?php if($song_data['song_approval_status'] == 'approved') echo( 'checked'); ?> name="song_approval_status" id="approval_status_radio" value="approved" required="required">Approved
		    </label>
		    <label class="radio-inline">
		      <input type="radio" <?php if($song_data['song_approval_status'] == 'waiting') echo( 'checked'); ?> name="song_approval_status" id="approval_status_radio" value="waiting" required="required">Waiting
		    </label>
		  </div>

		  <button type="submit" class="btn btn-default" value="click" name="song_form_update">Update</button>
		</form>       
		<form method="post" action="includes/form_submit.php">
		  <div class="form-group">
		    <textarea class="form-control" style="display: none;" name="song_id" rows="1"><?= $song_id ?></textarea>
		  </div>
		  <button type="submit" class="btn btn-default" value="click" name="song_form_delete">Delete</button>
		</form> 

	<?php
	// closing bracket for checking if user has privilage to do edit/update
	}else{
		echo '<p>Sorry, you do not have permission to access this page...</p>';
	}
}

/**
** Display the content of the admin's add new song page.
** ::param:: mysqli (mysqli object) for login
** ::param:: username (str): current username
** ::return:: none
**/
function admin_page_add($mysqli, $username){
	if( admin_check($mysqli) == true){
	?>
		<h2 class="sub-header">Song Add Form</h2>

      	<!-- Input Form for new lyric -->
        <form method="post" action="includes/form_submit.php">
          <div class="form-group">
            <label for="title">Title:</label>
            <textarea class="form-control" id="title" name="song_title" rows="1" placeholder="Write song title here" required="required"></textarea>
          </div>
          <div class="form-group">
            <label for="singer">Singer(s)/Band:</label>
            <textarea class="form-control" id="singer" name="song_singer" rows="1" placeholder="Write song singer here"></textarea>
          </div>
          <div class="form-group">
            <label for="writer">Writer(s):</label>
            <textarea class="form-control" id="writer" name="song_writer" rows="1" placeholder="Write song writer here"></textarea>
          </div>
          <div class="form-group">
            <label for="album">Album:</label>
            <textarea class="form-control" id="album" name="song_album" rows="1" placeholder="Write song album here"></textarea>
          </div>

          <!-- Base Note Option -->
          <label for="note_radio">Choose Base Note for the song:</label>
          <br>
          <label class="radio-inline">
            <input type="radio" name="song_base_note_id" id="note_radio" value="1" required="required"> C
          </label>
          <label class="radio-inline">
            <input type="radio" name="song_base_note_id" id="note_radio" value="2" required="required"> C#
          </label>
          <label class="radio-inline">
            <input type="radio" name="song_base_note_id" id="note_radio" value="3" required="required"> D
          </label>
          <label class="radio-inline">
            <input type="radio" name="song_base_note_id" id="note_radio" value="4" required="required"> D#
          </label>
          <label class="radio-inline">
            <input type="radio" name="song_base_note_id" id="note_radio" value="5" required="required"> E
          </label>
          <label class="radio-inline">
            <input type="radio" name="song_base_note_id" id="note_radio" value="6" required="required"> F
          </label>
          <br>
          <label class="radio-inline">
            <input type="radio" name="song_base_note_id" id="note_radio" value="7" required="required"> F#
          </label>
          <label class="radio-inline">
            <input type="radio" name="song_base_note_id" id="note_radio" value="8" required="required"> G
          </label>
          <label class="radio-inline">
            <input type="radio" name="song_base_note_id" id="note_radio" value="9" required="required"> G#
          </label>
          <label class="radio-inline">
            <input type="radio" name="song_base_note_id" id="note_radio" value="10" required="required"> A
          </label>
          <label class="radio-inline">
            <input type="radio" name="song_base_note_id" id="note_radio" value="11" required="required"> A#
          </label>
          <label class="radio-inline">
            <input type="radio" name="song_base_note_id" id="note_radio" value="12" required="required"> B
          </label>
          <br>
          <br>

          <div class="form-group">
            <label for="lyricForm">Lyric with Chord:</label>
            <textarea class="form-control lyric-text-area" id="lyricForm" name ="song_lyric" rows="20" placeholder="Write song lyric + chord here" required="required"></textarea>
          </div>

          <!-- Song Tag Option -->
          <label for="tag_checkbox">Choose tag(s) for the song:</label>
          <br>
          <label class="checkbox-inline">
            <input type="checkbox" id="tag_checkbox" name="song_tag_id[]" value="1"> Praise
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" id="tag_checkbox" name="song_tag_id[]" value="2"> Worship
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" id="tag_checkbox" name="song_tag_id[]" value="3"> Christmas
          </label>
           <label class="checkbox-inline">
            <input type="checkbox" id="tag_checkbox" name="song_tag_id[]" value="4"> Easter
          </label>
          <br>
          <label class="checkbox-inline">
            <input type="checkbox" id="tag_checkbox" name="song_tag_id[]" value="5"> Latin
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" id="tag_checkbox" name="song_tag_id[]" value="6"> Eucharistic
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" id="tag_checkbox" name="song_tag_id[]" value="7"> Holy Friday
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" id="tag_checkbox" name="song_tag_id[]" value="8"> Other
          </label>
          <br>
          <br>

          <div class="form-group">
            <label for="thumbs">Thumbs:</label>
            <textarea class="form-control" id="thumbs" name="song_thumbs" rows="1" placeholder="i.e: 10"></textarea>
          </div>
          <div class="form-group">
            <label for="youtube">Youtube Link:</label>
            <textarea class="form-control" id="youtube" name="song_youtube" rows="1" placeholder="Write youtube link for the song here"></textarea>
          </div>
          <div class="form-group">
            <label for="itunes">Itunes Link:</label>
            <textarea class="form-control" id="itunes" name="song_itunes" rows="1" placeholder="Write itunes link for the song here"></textarea>
          </div>
          <div class="form-group">
            <label for="spotify">Spotify Link:</label>
            <textarea class="form-control" id="spotify" name="song_spotify" rows="1" placeholder="Write spotify link for the song here"></textarea>
          </div>
          <div class="form-group">
            <label for="google_play">Google Play Link:</label>
            <textarea class="form-control" id="google_play" name="song_google_play" rows="1" placeholder="Write google play link for the song here"></textarea>
          </div>

          <div class="form-group">
            <label for="username">Submitted by:</label>
            <textarea class="form-control" id="username" name="song_username" value="<?= $username ?>" rows="1"><?= $username ?></textarea>
          </div>

          <button type="submit" class="btn btn-default" value="click" name="song_form_submit">Submit</button>
        </form>       

	<?php
	// closing bracket for checking if user has privilage to do edit/update
	}else{
		echo '<p>Sorry, you do not have permission to access this page...</p>';
	}
	
}

//=================================
// ---- Song Parsing Functions ----
// ================================

/**
** Convert all the songs in the playlist to a single text file
** ::param:: playlist_id (int): id of the target playlist
** ::param:: text_file (file stdio object): UNUSED
** ::return:: all the parsed songs (str)
**/
function getPlaylistToText($playlist_id, $text_file){
	$pl_mgr = new playlist_manager();
	echo $playlist_id;
    $pl_data = $pl_mgr->getPlaylistData($playlist_id);
	$song_mgr = new song_manager();
	$final = '';

	// iterate through all songs in the playlist
    for( $i = 0; $i < count($pl_data); $i++ ):
		$song_data = $song_mgr->getData($pl_data[$i]['playlists_songs_song_id']);
		$final .= getParsedSongToText($song_data['song_lyric'], $song_data['note_value'], $pl_data[$i]['note_value'], $song_data['song_title'], $song_data['song_singer'], $text_file);
		$final .= "\n\n";
		echo $song_data['note_value'];
	endfor;

	return $final;
}

/**
** Convert a parsed song lyric into a text file
** ::param:: myfile (str): song lyric file
** ::param:: base_note (str): base note of the song
** ::param:: target_note (str): the current song base note
** ::param:: song_title (str): title of the song
** ::param:: song_singer (str): song artist 
** ::param:: text_file (file stdio object): UNUSED
** ::return:: parsed song (str)
**/
function getParsedSongToText($myfile, $base_note, $target_note, $song_title, $song_singer, $text_file){
	$separator ="\n"; // seperator for parsing the lyric string

	$index = 0;
	$final = '';

	$myfile_array = explode($separator, $myfile);

	// write song title and singer first in the beginning
	$final .= $song_title . ' - ' . $song_singer . "\n\n";

	do{
		// get the first/chord line and put all word into array
		$chord_line = $myfile_array[$index];
		$chord_array = str_word_count($chord_line, 2, '0123456789#()+:');
		$index = $index+1;

		if(count($chord_array) == 0){ // check if empty line
		    $final .= "\n";
		}else if( count($chord_array) != 0 && strcasecmp(substr($chord_line, 0, 1), ":") == 0 ){ // check if first character the special character ':'
			
			// check if it's an intro line by checking the first word (case insensitive)
			if ( strcasecmp(substr($chord_array[0], 1, 5), 'intro') == 0 ){
				foreach($chord_array as $intro_chord){
				    if( strcasecmp($intro_chord, ':intro:') == 0 ){
				      $final .= substr($intro_chord, 1);
				    }else if(substr($intro_chord, -1) == 'x'){
				      $final .= $intro_chord;
				    }else{
				      $final .= ' ' . getChord($intro_chord, $base_note, $target_note) . ' ';
				    }
				}
			}else{
				$final .= substr($chord_line, 1);
			}
		}else{
			$start_idx = 0;
	  		$end_idx = 0;
	  		$chord = '';
	  		$lyric = '';

	  		// get lyric line
	  		$lyric_line = $myfile_array[$index];
	  		$index = $index+1;

		  	foreach ($chord_array as $key => $value) // iterate through all chords in chord line
		  	{
		  		// parse chords
		  		for($i = $start_idx; $i<$key; $i++)
		  		{
		  			$chord .= " ";
		  		}
		  		$chord .= getChord($value, $base_note, $target_note);

		  		$start_idx = strlen($chord) - 1;
		  	}

		  	$chord .= "\n";
		  	$lyric = $lyric_line;
		  	$final .= $chord . $lyric;
		}
		$final .= "\n";

	}while($index < count($myfile_array));

	return $final;
}

/**
** Transpose the chord of the song into a target base note
** ::param:: myfile (str): song lyric file
** ::param:: base_note (str): base note of the song
** ::param:: target_note (str): the current song base note
** ::return:: transposed lyric song (str)
**/
function parseSong($myfile, $base_note, $target_note){
	$separator = "\n"; // separator of the lyric file

	$index = 0;
	$final = '';

	$myfile_array = explode($separator, $myfile);

	// parse intro line at the first line of the song text
	if(count($myfile_array) <= 0){
		return 'No lyric has been found...';
	}

	do{
		// get the first/chord line and put all word into array
		$chord_line = $myfile_array[$index];
		$chord_array = str_word_count($chord_line, 2, '0123456789#()+:');
		$index = $index+1;

		if(count($chord_array) == 0){ // check if empty line
			$chord = '';
		    $lyric = '';

		    $final .= '<br>';
		    $final .= createSegment($chord, $lyric, $base_note, $target_note);
		}else if( count($chord_array) != 0 && strcasecmp(substr($chord_line, 0, 1), ":") == 0 ){ // check if first character the special character ':'
			
			// check if it's an intro line by checking the first word (case insensitive)
			if ( strcasecmp(substr($chord_array[0], 1, 5), 'intro') == 0 ){
				foreach($chord_array as $intro_chord){
				    if( strcasecmp($intro_chord, ':intro:') == 0 ){
				      $final .= '<div class="text-line">';
				      $final .= '<div class="text-seg">';
				      $final .= substr($intro_chord, 1);
				      $final .= '</div>';
				    }else if(substr($intro_chord, -1) == 'x'){
				      $final .= $intro_chord;
				      $final .= '</div>';
				    }else{
				      $final .= '<div class="chord-seg">';
				      $final .= ' ' . getChord($intro_chord, $base_note, $target_note) . ' ';
				      $final .= '</div>';
				    }
				}
			}else{
				$final .= '<div class="text-line">';
				$final .= '<div class="text-seg">';
				$final .= substr($chord_line, 1);
				$final .= '</div></div>';
			}
		}else{
			// get the second/text line and put all word into array
			$lyric_line = $myfile_array[$index];
			$lyric_array = str_word_count($lyric_line, 2, '0123456789#()+:');
			$index = $index+1;

			$i = 0;
		
			$start_idx = 0;
			$end_idx = 0;
			$chord = '';
			$lyric = '';
			$prev_value = '';

			$final .= '<div class="text-line">';

		  	foreach ($chord_array as $key => $value) { // iterate through all chords in chord line
		      $end_idx = $key;

		      if($i == 0 && $key > 0){ // check if first chord is not on the first character
		        $chord = '';
		        $lyric = substr($lyric_line, 0, $key );
		        $final .= createSegment($chord, $lyric, $base_note, $target_note);
		      }elseif($i != 0){ // process the chord on the next iteration
		        $chord = $prev_value;
		        $lyric = substr($lyric_line, $start_idx, ($end_idx - $start_idx) );
		        $final .= createSegment($chord, $lyric, $base_note, $target_note);
		      }
		      $prev_value = $value;
		      $start_idx = $key;
		      $i = $i + 1;
		      
		      if( $i == (count($chord_array)) ){ // get the remaining chord and lyric at the very last chord
		        $chord = $value;
		        $lyric = substr($lyric_line, $start_idx);

		        $final .= createSegment($chord, $lyric, $base_note, $target_note);
		      }
		  	}

		 	$final .= '</div>';
	  	}
	}while($index < count($myfile_array));

	return $final;
}

/**
** Create a lyric-chord html segments for a song
** ::param:: chord_str (str): string representation of a chord
** ::param:: lyric_str (str): song lyric
** ::param:: base_note (str): base note of the song
** ::param:: target_note (str): the current song base note
** ::return:: transposed lyric song (str)
**/
function createSegment($chord_str, $lyric_str, $base_note, $target_note){
    $segment = '';
    $chord = '';

    if(empty($chord_str) && empty($lyric_str)){
	    $segment .= '<div class="text-seg">';
	    $segment .= '<div class="chord-seg">';
	    $segment .= ' ';
	    $segment .= '</div>';
	    $segment .= '<div class="lyric-seg">';
	    $segment .= ' ';
	    $segment .= '</div>';
	    $segment .= '</div>';
    }else if(!empty($chord_str) && empty($lyric_str)){
    	$chord = getChord($chord_str, $base_note, $target_note);

	    $segment .= '<div class="text-seg">';
	    $segment .= '<div class="chord-seg">';
	    $segment .= $chord;
	    $segment .= '</div>';
	    $segment .= '<div class="lyric-seg">';
	    $segment .= ' ';
	    $segment .= '</div>';
	    $segment .= '</div>';
    }else{
	    $chord = getChord($chord_str, $base_note, $target_note);

	    $segment .= '<div class="text-seg">';
	    $segment .= '<div class="chord-seg">';
	    $segment .= $chord;
	    $segment .= '</div>';
	    $segment .= '<div class="lyric-seg">';
	    $segment .= $lyric_str;
	    $segment .= '</div>';
	    $segment .= '</div>';
	}

    return $segment;
}

/**
** Transpose a line of chords based on the base note and the target note
** ::param:: chord_str (str): string representation of a chord
** ::param:: base_note (str): base note of the song
** ::param:: target_note (str): the current song base note
** ::return:: transposed chords (str)
**/
function getChord($chord_string, $base_note, $target_note){
  $chord = '';
  $chord_addon = ''; 
  $start_idx = 0;

  if(strlen($chord_string) > 0){
    $first_letter = substr($chord_string, 0, 1);
    $chord .= $first_letter;

    $second_letter = substr($chord_string, 1, 1);
    if($second_letter == '#'){
      $chord .= $second_letter;
      $start_idx = 1;
    }
    $chord_addon = substr($chord_string, $start_idx+1);

  }
  $chord = assignChord($base_note, $target_note, $chord) . $chord_addon;

  return $chord;
}


/**
** Transpose a line of chords based on the base note and the target note
** ::param:: base_note (str): base note of the song
** ::param:: target_note (str): the current song base 
** ::param:: chord (str): string representation of a chord
** ::return:: transposed chord (str)
**/
function assignChord($base_note, $target_note, $chord){
  $result = '';
  if($base_note == '' || $target_note == '' || $chord == ''){
    return $result;
  }

  $chord_array = array(
      0 => 'C',
      1 => 'C#',
      2 => 'D',
      3 => 'D#',
      4 => 'E',
      5 => 'F',
      6 => 'F#',
      7 => 'G',
      8 => 'G#',
      9 => 'A',
      10 => 'A#',
      11 => 'B'   
    );

  // calculate distance from target to base note
  $start = array_search($base_note, $chord_array);
  $finish = array_search($target_note, $chord_array);
  $current = array_search($chord, $chord_array);
  $distance = $finish - $start;
  $target_idx = (($current+$distance)%12);

  // pseudo circular buffer indexing
  if($target_idx < 0){
  	$target_idx = 12 + $target_idx;
  }

  $result = $chord_array[$target_idx];

  return $result; 
}

?>