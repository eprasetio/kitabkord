<?php
/*
author = eprasetio

Php script to upload a profile image.
*/

require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/functions_v2.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/classes/login_manager.php';

sec_session_start();

$login_mgr = new login_manager();

$user_id = ( isset( $_POST['user_id'] ) ) ? $_POST['user_id'] : '';
$username = ( isset( $_POST['username'] ) ) ? $_POST['username'] : '';
$target_dir = "../img/profiles/";
$target_file_temp = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file_temp, PATHINFO_EXTENSION));

$file_name = $user_id . "_" . $username . "." . $imageFileType;
$target_file =  $target_dir . $file_name;

echo "Updating your profile picture...\n";

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        // echo "File is an image - " . $check["mime"] . ".";
        // file is an image, hence allow image upload
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}

// Check if file already exists
if (file_exists($target_file)) {
    // delete old file if already exist
    if (unlink($target_file)){
    	$uploadOk = 1;
    }else{
    	$uploadOk = 0;
    }
}

// Check file size (in bytes)
if ($_FILES["fileToUpload"]["size"] > 4000000) { // 4MB
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        // echo "Your profile picture has been uploaded.";
        // add the filename into the user's database
        $login_mgr->insert_profile_picture_path($username, $file_name);
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

// go back to profile page
header('Refresh: 2;url=' . $GLOBAL["ROOT_PATH"] . '/profile_page.php');
?>