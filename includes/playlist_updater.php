<?php
/*
author = eprasetio

Php script to manage a song playlist.
	The operation includes: create playlist, delete playlist, 
							insert song, delete song, and get playlists. 
*/

require_once $_SERVER["DOCUMENT_ROOT"] . '/login/includes/login_dbconnect.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/login/includes/login_functions.php';
sec_session_start();

require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/functions_v2.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/includes/classes/playlist_manager.php';

$pl_mgr = new playlist_manager();
$action = ( isset( $_POST['action'] ) ) ? $_POST['action'] : 'default';

// perform actions here
if( $action == 'create_playlist' ){	// create playlist
	$playlist_name = (isset( $_POST['playlist_name'] ) ) ? $_POST['playlist_name'] : '';
	$username = (isset( $_POST['username'] ) ) ? $_POST['username'] : '';
	
	if($playlist_name != '' && $username != ''){
		$pl_mgr->createPlaylist($playlist_name, $username);
	}

	$result = "pl_name:: " . $playlist_name . ", username:: " . $username;
	echo $result;
}else if( $action == 'delete_playlist' ){	// delete playlist
	$playlist_id = isset( $_POST['playlist_id'] ) ? $_POST['playlist_id'] : '';
	$playlist_name = isset( $_POST['playlist_name'] ) ? $_POST['playlist_name'] : '';
	$username = isset( $_POST['username'] ) ? $_POST['username'] : '';
	$sidebar_playlist_str = "";
	
	if($playlist_id != ''){
		$pl_mgr->deletePlaylist($playlist_id);
	}

	if( $username != '' ){
		$result = $pl_mgr->getPlaylists($username);

		// iterate through all playlists
		for($i=0;$i<count($result);$i++){
			// create a string for playlist sidebar as well
			$sidebar_playlist_str .= "<li><a href='playlist_page.php?playlist_id=" . $result[$i]['playlist_id'] .  "&playlist_name=" . $result[$i]['playlist_name'] .  "'>" . $result[$i]['playlist_name'] . "</a></li>";
		}

		echo $sidebar_playlist_str;
	}
}else if( $action == 'insert_song' ){	// insert song
	$song_id = isset( $_POST['song_id'] ) ? $_POST['song_id'] : '';
	$song_target_note_id = isset( $_POST['song_target_note_id'] ) ? $_POST['song_target_note_id'] : '';
	$playlist_id = isset( $_POST['playlist_id'] ) ? $_POST['playlist_id'] : '';
	$username = (isset( $_POST['username'] ) ) ? $_POST['username'] : '';

	if($song_id != '' && $playlist_id != ''){
		$pl_mgr->insertSong($song_id, $song_target_note_id, $playlist_id);
	}

	$result = NULL;
	$playlist_str = "";
	
	if( $username != '' ){

		$result = $pl_mgr->getPlaylists($username);

		// iterate through all playlists
		for($i=0;$i<count($result);$i++){
			// check if song is in playlist
			if( strcmp($pl_mgr->checkSong($song_id, $result[$i]["playlist_id"]), "true") == 0 ){
				$playlist_str .= "<p class='playlist-name'><span onclick='removeSongFromPlaylistModal(" . $song_id . ", " . 
					$result[$i]["playlist_id"] . ", \"" . $username . "\")' class='glyphicon glyphicon-check' aria-hidden='true'></span>" .
					$result[$i]["playlist_name"] . "</p>";
			}else{ 
				$playlist_str .= "<p class='playlist-name'><span onclick='addSongToPlaylistModal(" . $song_id . ", " .
					$result[$i]["playlist_id"] . ", \"" . $username . "\")' class='glyphicon glyphicon-unchecked' aria-hidden='true'></span>" .
					$result[$i]["playlist_name"] . "</p>";
			}
		}
		echo $playlist_str;

	}
}else if( $action == 'get_playlists' ){	// get all playlists
	$username = (isset( $_POST['username'] ) ) ? $_POST['username'] : '';
	$song_id = isset( $_POST['song_id'] ) ? $_POST['song_id'] : '';
	$result = NULL;
	$playlist_str = "";
	$sidebar_playlist_str = "";
	
	if( $username != '' ){
		$result = $pl_mgr->getPlaylists($username);

		// iterate through all playlists
		for($i=0;$i<count($result);$i++){
			// check if song is in playlist
			if( strcmp($pl_mgr->checkSong($song_id, $result[$i]["playlist_id"]), "true") == 0 ){
				$playlist_str .= "<p class='playlist-name'><span onclick='removeSongFromPlaylistModal(" . $song_id . ", " . 
					$result[$i]["playlist_id"] . ", \"" . $username . "\")' class='glyphicon glyphicon-check' aria-hidden='true'></span>" .
					$result[$i]["playlist_name"] . "</p>";
			}else{ 
				$playlist_str .= "<p class='playlist-name'><span onclick='addSongToPlaylistModal(" . $song_id . ", " . 
					$result[$i]["playlist_id"] . ", \"" . $username . "\")' class='glyphicon glyphicon-unchecked' aria-hidden='true'></span>" .
					$result[$i]["playlist_name"] . "</p>";
			}
			// create a string for playlist sidebar as well
			$sidebar_playlist_str .= "<li><a href='playlist_page.php?playlist_id=" . $result[$i]['playlist_id'] .  "&playlist_name=" . $result[$i]['playlist_name'] .  "'>
			<span class='sidebar-icon glyphicon glyphicon-list' aria-hidden='true'></span>" . $result[$i]['playlist_name'] . "</a></li>";
		}
		echo json_encode(array($playlist_str, $sidebar_playlist_str));

	}
}else if( $action == 'delete_song' ){	// delete a song from playlist
	$song_id = isset( $_POST['song_id'] ) ? $_POST['song_id'] : '';
	$playlist_id = isset( $_POST['playlist_id'] ) ? $_POST['playlist_id'] : '';
	$username = (isset( $_POST['username'] ) ) ? $_POST['username'] : '';

	if($song_id != '' && $playlist_id != ''){
		$pl_mgr->deleteSong($song_id, $playlist_id);
	}

	$result = NULL;
	$playlist_str = "";
	
	if( $username != '' ){
		$result = $pl_mgr->getPlaylists($username);

		// iterate through all playlists
		for($i=0;$i<count($result);$i++){
			// check if song is in playlist
			if( strcmp($pl_mgr->checkSong($song_id, $result[$i]["playlist_id"]), "true") == 0 ){
				$playlist_str .= "<p class='playlist-name'><span onclick='removeSongFromPlaylistModal(" . $song_id . ", " . 
					$result[$i]["playlist_id"] . ", \"" . $username . "\")' class='glyphicon glyphicon-check' aria-hidden='true'></span>" .
					$result[$i]["playlist_name"] . "</p>";
			}else{ 
				$playlist_str .= "<p class='playlist-name'><span onclick='addSongToPlaylistModal(" . $song_id . ", " . 
					$result[$i]["playlist_id"] . ", \"" . $username . "\")' class='glyphicon glyphicon-unchecked' aria-hidden='true'></span>" .
					$result[$i]["playlist_name"] . "</p>";
			}
		}

		echo $playlist_str;
	}

}
?>